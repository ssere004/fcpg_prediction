Epic2 manifest file is downloaded from here https://support.illumina.com/downloads/infinium-methylationepic-v2-0-product-files.html,
It has bunch of files and the EPIC-8v2-0_A1.csv file were used to find the location of fCpG files in the genome.

fCpG list file are downloaded from  https://www.nature.com/articles/s41587-021-01109-w#MOESM3 supp tables 2 and 3

Warning: there is 200 rows in the fCpG list which their ID could not be found in the Epic2 manifest file, I have no idea what is the reason
Warning: the Epic2 manifest file has some rows whith chromosome name of chr0. I have no idea what is the reason.




curl -O https://ftp.ncbi.nlm.nih.gov/geo/samples/GSM3239nnn/GSM3239873/suppl/GSM3239873%5FQKO%5F13D%5FBS%5Fseq%5Frep1%5FmCG.txt.gz
curl -O https://ftp.ncbi.nlm.nih.gov/geo/samples/GSM3239nnn/GSM3239874/suppl/GSM3239874%5FQKO%5F13D%5FBS%5Fseq%5Frep2%5FmCG.txt.gz
curl -O https://ftp.ncbi.nlm.nih.gov/geo/samples/GSM3239nnn/GSM3239875/suppl/GSM3239875%5FQKO%5FDnmt3c%5FKO%5FBS%5Fseq%5FmCG.txt.gz
curl -O https://ftp.ncbi.nlm.nih.gov/geo/samples/GSM3239nnn/GSM3239876/suppl/GSM3239876%5FQKO%5FmES%5FBS%5Fseq%5Frep1%5FmCG.txt.gz
curl -O https://ftp.ncbi.nlm.nih.gov/geo/samples/GSM3239nnn/GSM3239877/suppl/GSM3239877%5FQKO%5FmES%5FBS%5Fseq%5Frep2%5FmCG.txt.gz
curl -O https://ftp.ncbi.nlm.nih.gov/geo/samples/GSM3239nnn/GSM3239878/suppl/GSM3239878%5FRQKO%5FDnmt3b%5FOE%5FBS%5Fseq%5Frep1%5FmCG.txt.gz
curl -O https://ftp.ncbi.nlm.nih.gov/geo/samples/GSM3239nnn/GSM3239879/suppl/GSM3239879%5FRQKO%5FDnmt3b%5FOE%5FBS%5Fseq%5Frep2%5FmCG.txt.gz
curl -O https://ftp.ncbi.nlm.nih.gov/geo/samples/GSM3239nnn/GSM3239880/suppl/GSM3239880%5FRQKO%5FmES%5FBS%5Fseq%5Frep1%5FmCG.txt.gz
curl -O https://ftp.ncbi.nlm.nih.gov/geo/samples/GSM3239nnn/GSM3239881/suppl/GSM3239881%5FRQKO%5FmES%5FBS%5Fseq%5Frep2%5FmCG.txt.gz
curl -O https://ftp.ncbi.nlm.nih.gov/geo/samples/GSM3239nnn/GSM3239882/suppl/GSM3239882%5FSKO%5FDnmt1%5FOE%5FBS%5Fseq%5FmCG.txt.gz
curl -O https://ftp.ncbi.nlm.nih.gov/geo/samples/GSM3239nnn/GSM3239883/suppl/GSM3239883%5FSKO%5FDnmt3c%5FKO%5FDnmt1%5FOE%5FBS%5Fseq%5FmCG.txt.gz
curl -O https://ftp.ncbi.nlm.nih.gov/geo/samples/GSM3239nnn/GSM3239884/suppl/GSM3239884%5FSKO%5FmES%5FBS%5Fseq%5FmCG.txt.gz
curl -O https://ftp.ncbi.nlm.nih.gov/geo/samples/GSM3239nnn/GSM3239885/suppl/GSM3239885%5FTKO%5F13D%5FBS%5Fseq%5Frep1%5FmCG.txt.gz
curl -O https://ftp.ncbi.nlm.nih.gov/geo/samples/GSM3239nnn/GSM3239886/suppl/GSM3239886%5FTKO%5F13D%5FBS%5Fseq%5Frep2%5FmCG.txt.gz
curl -O https://ftp.ncbi.nlm.nih.gov/geo/samples/GSM3239nnn/GSM3239887/suppl/GSM3239887%5FTKO%5FmES%5FBS%5Fseq%5Frep1%5FmCG.txt.gz
curl -O https://ftp.ncbi.nlm.nih.gov/geo/samples/GSM3239nnn/GSM3239888/suppl/GSM3239888%5FTKO%5FmES%5FBS%5Fseq%5Frep2%5FmCG.txt.gz
curl -O https://ftp.ncbi.nlm.nih.gov/geo/samples/GSM3239nnn/GSM3239889/suppl/GSM3239889%5FWT%5F13D%5FBS%5Fseq%5Frep1%5FmCG.txt.gz
curl -O https://ftp.ncbi.nlm.nih.gov/geo/samples/GSM3239nnn/GSM3239890/suppl/GSM3239890%5FWT%5F13D%5FBS%5Fseq%5Frep2%5FmCG.txt.gz
curl -O https://ftp.ncbi.nlm.nih.gov/geo/samples/GSM3239nnn/GSM3239891/suppl/GSM3239891%5FWT%5FmES%5FBS%5Fseq%5Frep1%5FmCG.txt.gz
curl -O https://ftp.ncbi.nlm.nih.gov/geo/samples/GSM3239nnn/GSM3239892/suppl/GSM3239892%5FWT%5FmES%5FBS%5Fseq%5Frep2%5FmCG.txt.gz


Renaming 'GSM3239873%5FQKO%5F13D%5FBS%5Fseq%5Frep1%5FmCG.txt' to 'QKO_rep1.txt'
Renaming 'GSM3239874%5FQKO%5F13D%5FBS%5Fseq%5Frep2%5FmCG.txt' to 'QKO_rep2.txt'
Renaming 'GSM3239875%5FQKO%5FDnmt3c%5FKO%5FBS%5Fseq%5FmCG.txt' to 'QKO.txt'
Renaming 'GSM3239876%5FQKO%5FmES%5FBS%5Fseq%5Frep1%5FmCG.txt' to 'QKO_rep1.txt'
Renaming 'GSM3239877%5FQKO%5FmES%5FBS%5Fseq%5Frep2%5FmCG.txt' to 'QKO_rep2.txt'
Renaming 'GSM3239878%5FRQKO%5FDnmt3b%5FOE%5FBS%5Fseq%5Frep1%5FmCG.txt' to 'RQKO_rep1.txt'
Renaming 'GSM3239879%5FRQKO%5FDnmt3b%5FOE%5FBS%5Fseq%5Frep2%5FmCG.txt' to 'RQKO_rep2.txt'
Renaming 'GSM3239880%5FRQKO%5FmES%5FBS%5Fseq%5Frep1%5FmCG.txt' to 'RQKO_rep1.txt'
Renaming 'GSM3239881%5FRQKO%5FmES%5FBS%5Fseq%5Frep2%5FmCG.txt' to 'RQKO_rep2.txt'
Renaming 'GSM3239882%5FSKO%5FDnmt1%5FOE%5FBS%5Fseq%5FmCG.txt' to 'SKO.txt'
Renaming 'GSM3239883%5FSKO%5FDnmt3c%5FKO%5FDnmt1%5FOE%5FBS%5Fseq%5FmCG.txt' to 'SKO.txt'
Renaming 'GSM3239884%5FSKO%5FmES%5FBS%5Fseq%5FmCG.txt' to 'SKO.txt'
Renaming 'GSM3239885%5FTKO%5F13D%5FBS%5Fseq%5Frep1%5FmCG.txt' to 'TKO_rep1.txt'
Renaming 'GSM3239886%5FTKO%5F13D%5FBS%5Fseq%5Frep2%5FmCG.txt' to 'TKO_rep2.txt'
Renaming 'GSM3239887%5FTKO%5FmES%5FBS%5Fseq%5Frep1%5FmCG.txt' to 'TKO_rep1.txt'
Renaming 'GSM3239888%5FTKO%5FmES%5FBS%5Fseq%5Frep2%5FmCG.txt' to 'TKO_rep2.txt'
Renaming 'GSM3239889%5FWT%5F13D%5FBS%5Fseq%5Frep1%5FmCG.txt' to 'WT_rep1.txt'
Renaming 'GSM3239890%5FWT%5F13D%5FBS%5Fseq%5Frep2%5FmCG.txt' to 'WT_rep2.txt'
Renaming 'GSM3239891%5FWT%5FmES%5FBS%5Fseq%5Frep1%5FmCG.txt' to 'WT_rep1.txt'
Renaming 'GSM3239892%5FWT%5FmES%5FBS%5Fseq%5Frep2%5FmCG.txt' to 'WT_rep2.txt'




#/bin/csh
# ------- COMMANDS
export BWA=bwa
export SAM=samtools
export DATASET=hg19
export DI=/home/ssere004/projects/thyroid_hiC
export READS=/home/ssere004/projects/thyroid_hiC/reads/trimm
export REF=$DI/$DATASET/$DATASET.fa
export THREADS=10
export T=tracks.ini
# ------- CORRECT
export F=$DI/$DATASET/COHP60582_to_$DATASET
# REMEMBER TO conda activate hicexplorer
#hicCorrectMatrix correct -m $F.hic_matrix.h5 --filterThreshold -5 5 -o $F.hic_corrected.h5
echo "hicCorrectMatrix is Done"
# ------- PLOT
hicPlotMatrix -m $F.hic_corrected.h5  -o $F.hic_corrected.png --dpi 600 --log1p
echo "hicPlotMatrix is Done"
#rclone copy $F.hic_corrected.png remote:
hicPlotMatrix -m $F.hic_corrected.h5 -o $F.hic_corrected.png --dpi 600 --log1p --chromosomeOrder chr1 chr2 chr3 chr4 chr5 --perChromosome
echo "hicplotMatrix2 is Done"
#rclone copy $F.hic_corrected.png remote:
# ------- CONVERT
# hicConvertFormat --matrices $F.hic_corrected.h5 --inputFormat h5 -o ginteractions --outFileName $F.hic_corrected --outputFormat ginteractions
# ------- FIND TADs
# Minimum window length (in bp) to be considered to the left and to the right of each Hi-C bin. This number should be at least 3 times as large as the bin size of the Hi-C matrix.
# Maximum window length to be considered to the left and to the right of the cut point in bp. This number should around 6-10 times as large as the bin size of the Hi-C matrix.
export MIN=3000
export MAX=12000
export STEP=1000
export THR=0.05
export DEL=0.01
hicFindTADs -m $F.hic_corrected.h5 --outPrefix TAD_min$MIN\_max$MAX\_step$STEP\_$THR\_$DEL\_fdr --chromosomes chr1 --numberOfProcessors 32 --minDepth $MIN --maxDepth $MAX --step $STEP --thresholdComparisons $THR --delta $DEL --correctForMultipleTesting fdr
echo "hicFindTADs is Done"
hicPlotTADs --tracks $T --region chr1:1000000-9000000 -o TAD_min$MIN\_max$MAX\_step$STEP\_$THR\_$DEL\_fdr.png --dpi 600
echo "hicPlotTADs is Done"

export NEWTHR=0.01
export NEWDEL=0.01
hicFindTADs -m $F.hic_corrected.h5 --outPrefix TAD_min$MIN\_max$MAX\_step$STEP\_$NEWTHR\_$NEWDEL\_fdr --chromosomes chr1 --numberOfProcessors 32 --TAD_sep_score_prefix TAD_min$MIN\_max$MAX\_step$STEP\_$THR\_$DEL\_fdr --thresholdComparis
on
s $NEWTHR --delta $NEWDEL --correctForMultipleTesting fdr
echo "hicFindTADs2 is Done"
hicPlotTADs --tracks $T --region chr1:1000000-9000000 -o TAD_min$MIN\_max$MAX\_step$STEP\_$NEWTHR\_$NEWDEL\_fdr.png --dpi 600
echo "hicPlotTADs2 is Done"

#rclone copy TAD.png remote:

hicConvertFormat --matrices $F.hic_corrected.h5 --inputFormat h5 -o ginteractions --chromosome chr1 --outFileName $F.hic_corrected --outputFormat ginteractions


