import re
import subprocess

import sys
import os
project_root = os.path.dirname("/home/ssere004/fCpG_prediction/fcpg_prediction/")
sys.path.append(project_root)
import numpy as np
import argparse

KO = 'DNMT3'
species = 'homo'


parser = argparse.ArgumentParser()
parser.add_argument('-KO', '--knockout', help='TET or DNMT3, QKO, PKO, mouse_3aKO, mouse_3bKO, mouse_SI_TET23_KO', required=True)
parser.add_argument('-sp', '--species', help='species: homo, mouse, mouse_si', required=False, default='BERT')

args = parser.parse_args()

KO = args.knockout
species = args.species

eval_threshold = 0.05


def find_eval(line):
    pattern = r'E= ([0-9eE.+-]+)'
    match = re.search(pattern, line)
    if match:
        return float(match.group(1))


def find_motif_num(input_file, mtf):
    with open(input_file, 'r') as infile:
            for line in infile:
                if "MOTIF {} MEME".format(mtf) in line:
                    match = re.search(r'\b(MEME-\d+)\b', line)
                    if match:
                        return match.group(1)


def get_motif_evals(input_file):
    Evals = []
    mtfs = []
    try:
        with open(input_file, 'r') as infile:
            for line in infile:
                if 'position-specific probability matrix' in line:
                    mtfs.append(re.search('Motif (.*?) MEME', line).group(1))
                if line.strip().startswith("letter-probability matrix"):
                    Evals.append(find_eval(line))
        Evals = list(zip(Evals, mtfs))
        return {mtf: evl for evl, mtf in Evals}
    except Exception as e:
        print(f"An error occurred: {e}")

def write_selected_motifs(in_file, out_file, motifs, motifs_pvals):
    output = ""
    dashline=0
    for i in range(len(motifs)):
        with open(in_file, 'r') as infile:
            write_lines = False
            for line in infile:
                if 'position-specific probability matrix' in line:
                    pattern = r"Motif (.+?) MEME-\d{1,2} position-specific probability matrix"
                    match = re.search(pattern, line)
                    if match and match.group(1) == motifs[i]:
                        output = output + 'p-value based on hypergeometric test: ' + str(motifs[i]) + ' ' + str(motifs_pvals[i])
                        write_lines = True
                        dashline = 0
                elif line.strip().startswith("-----"):
                    if dashline == 0:
                        dashline = 1
                    else:
                        write_lines = False
                        dashline = 0
                if write_lines:
                    output = output + line
    print(output)
    with open(out_file, 'w') as outputfile:
        outputfile.write(output)
    pass

root = '/home/ssere004/fCpG_prediction/fcpg_prediction/dump_files/motifs/high_atten_regs'
root_ = '/home/ssere004/fCpG_prediction/fcpg_prediction/dump_files/motifs'

#meme finds frequent motifs in the high attention regions of positively predicted sequences.
# To make the fasta file for positive high attention regions you should run the finding high attention regions module from motif_utils module.

print('START .....')
print('MEME is running .....')
subprocess.run([root + "/meme_run/meme", root + "/{}-{}_pos.fasta".format(species, KO), "-minw", "6", "-maxw", "12", "-minsites", "20", "-dna", "-nmotifs", "100", "-oc", root + "/{}-{}_MEME/".format(species, KO), "-p", "10"])
subprocess.run([root + "/meme_run/streme", "--p", root + "/{}-{}_pos.fasta".format(species, KO), "--n",  root + "/{}-{}_neg.fasta".format(species, KO), "--minw", "6", "--maxw", "12", "--dna", "--nmotifs", "100", "--oc", root_ + "/streme/{}-{}_STREME/".format(species, KO)])
print('MEME DONE!')


evals = get_motif_evals(root + '/{}-{}_MEME/meme.txt'.format(species, KO))
print('{} total number of motifs'.format(len(evals)))
print(evals)

selected_motifs = [mtf for mtf in evals if evals[mtf] < eval_threshold]
print('{} total number of motifs after filtering evals by threshold {}'.format(len(selected_motifs), eval_threshold))

#subprocess.run(["fimo --oc ./{}-{}_MEME/ ./motif_finding_feed/homo_DNMT3_pos_seq.fasta"])
subprocess.run([root + "/meme_run/fimo", "--thresh", "0.00001", "--oc", root + "/{}-{}_MEME/".format(species, KO), root + "/{}-{}_MEME/meme.txt".format(species, KO), root + "/motif_finding_feed/{}_{}_pos_seq.fasta". format(species, KO)])

import pandas as pd
df1 = pd.read_csv(root + '/{}-{}_MEME/fimo.tsv'.format(species, KO), sep='\t', comment='#')
res_pos = df1.motif_id.value_counts().reset_index().rename(columns={'motif_id': 'motif', 'count': 'pos_num'})

subprocess.run([root + "/meme_run/fimo", "--thresh", "0.00001", "--oc", root + "/{}-{}_MEME/".format(species, KO), root + "/{}-{}_MEME/meme.txt".format(species, KO), root + "/motif_finding_feed/{}_{}_neg_seq.fasta". format(species, KO)])
df2 = pd.read_csv(root + '/{}-{}_MEME/fimo.tsv'.format(species, KO), sep='\t', comment='#')
res_neg = df2.motif_id.value_counts().reset_index().rename(columns={'motif_id': 'motif', 'count': 'neg_num'})

instance_c_df = res_pos.merge(res_neg, on='motif')
instance_c_df.to_csv(root + '/{}-{}_MEME/mtf_instances.csv'.format(species, KO), sep='\t')

from scipy.stats import hypergeom
import preprocess.data_reader as data_reader
pvals = {}
pos_seq_size = len(data_reader.readfasta(root + "/motif_finding_feed/{}_{}_pos_seq.fasta".format(species, KO)))
neg_seq_size = len(data_reader.readfasta(root + "/motif_finding_feed/{}_{}_neg_seq.fasta".format(species, KO)))
N = pos_seq_size + neg_seq_size
K = pos_seq_size
for i, row in instance_c_df.iterrows():
    n = int(row['pos_num']) + int(row['neg_num'])
    x = int(row['pos_num'])
    pval = hypergeom.sf(x-1, N, K, n)
    pvals[row['motif']] = pval

print('pvalues of selected motifs is calculated, number of motifs: {}'.format(len(pvals)))

pvals = {key: pvals[key] for key in pvals if key in selected_motifs}

final_motifs = sorted(pvals, key=pvals.get, reverse=True)[:3]
final_motif_pvals = [pvals[mtf] for mtf in final_motifs]
print('final_motifs', final_motifs)
print('final_motifs_pvals', final_motif_pvals)

write_selected_motifs(root + '/{}-{}_MEME/meme.txt'.format(species, KO),
                      root + '/{}-{}_MEME/meme_best.txt'.format(species, KO),
                      final_motifs, final_motif_pvals)


for i in range(10):
    print('________________-------------------__________________DONE_____________-_____________--------------_____________DONE_______----------DONE {}, {}'.format(species, KO))





