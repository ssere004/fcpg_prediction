# # import configs
# # import preprocess.data_reader as data_reader
# # import preprocess.preprocess as preprocess
# # import process.process as process
# import torch.optim as optim
# import os
# import motif_util
# from transformers import AutoModel, AutoTokenizer
# from tqdm import tqdm
# import pandas as pd
# import torch
# from torch.utils.data import Dataset, DataLoader
# import torch.nn as nn
# from sklearn.model_selection import train_test_split
# import numpy as np
# import argparse
# from preprocess import BERT_preprocess as bertprep
# import DNABERT_finetune as dbft
# import torch.nn.functional as F
#
# window_size = 200
# min_len = 5
# pval_cutoff = 0.005
# min_n_motif = 3
# align_all_ties = False
# save_file_dir = './dump_files/pretrained_models/'
# return_idx = True
# verbose = True
#
#
# X = pd.read_csv('X.csv', header=None)
# Y = pd.read_csv('Y.csv', header=None)
# X = X[1][1:]
# Y = Y[1]
# Y = Y[1:]
#
# # X = X[:1000]
# # Y = Y[:1000]
#
# Y = np.asarray(pd.cut(Y, bins=2, labels=[0, 1], right=False))
# b = np.zeros((Y.size, Y.max()+1))
# b[np.arange(Y.size), Y] = 1
# Y = b
# X__ = X
# Y__ = Y
#
#
# labels = torch.tensor(Y, dtype=torch.float32)
# dataset = dbft.DNADataset(X.tolist(), labels, tokenizer, max_sequence_length)
# data_loader_test = DataLoader(dataset, batch_size=batch_size, shuffle=True)
#
#
# clf_model.
#
# atten_scores = np.load('test.npy')
#
# pred = np.load(os.path.join(args.predict_dir, "pred_results.npy"))
# dev = pd.read_csv(os.path.join(args.data_dir, "dev.tsv"), sep='\t', header=0)
# dev.columns = ['sequence', 'label']
# dev['seq'] = dev['sequence'].apply(motif_util.kmer2seq)
# dev_pos = dev[dev['label'] == 1]
# dev_neg = dev[dev['label'] == 0]
# pos_atten_scores = atten_scores[dev_pos.index.values]
# neg_atten_scores = atten_scores[dev_neg.index.values]
# assert len(dev_pos) == len(pos_atten_scores)
#
# # run motif analysis
# merged_motif_seqs = motif_util.motif_analysis(dev_pos['seq'],
#                                     dev_neg['seq'],
#                                     pos_atten_scores,
#                                     window_size = window_size,
#                                     min_len = min_len,
#                                     pval_cutoff = pval_cutoff,
#                                     min_n_motif = min_n_motif,
#                                     align_all_ties = align_all_ties,
#                                     save_file_dir = save_file_dir,
#                                     verbose = verbose,
#                                     return_idx  = return_idx
#                                 )
#
#
#
# class DNASequenceClassifier(nn.Module):
#     def __init__(self, bert_model, fc_ad, num_classes, input_ids, attention_mask):
#         super(DNASequenceClassifier, self).__init__()
#         self.bert = bert_model
#         self.maxpool1 = nn.MaxPool2d(kernel_size=(8, 2), stride=(1, 3))
#         self.conv2 = nn.Conv2d(32, 64, kernel_size=(8, 2), padding=(4, 1), bias=False)
#         self.maxpool2 = nn.MaxPool2d(kernel_size=(10, 2), stride=(1, 3))#[32, 128, 106, 1]
#         self.flatten = nn.Flatten()
#         self.set_flatten_size(input_ids, attention_mask)
#         self.fc = nn.Sequential(
#             nn.Linear(self.flatten_size, 64),
#             nn.Dropout(0.5),
#             nn.ReLU(),
#             nn.Linear(64, num_classes),
#             nn.Softmax()
#         )
#         if fc_ad != None:
#             self.fc.load_state_dict(torch.load(fc_ad))
#     def forward(self, input_ids, attention_mask):
#         torch_outs = self.bert(
#             input_ids,
#             attention_mask=attention_mask,
#             encoder_attention_mask=attention_mask,
#             output_hidden_states=True
#         )
#         embeddings = torch_outs['hidden_states'][-1].cpu().detach().numpy()
#         at_cpu = attention_mask.cpu()
#         at_cpu_reshaped = at_cpu.unsqueeze(-1)  # Shape becomes [32, 200, 1]
#         weighted_embeddings = at_cpu_reshaped * embeddings  # Shape will be [32, 200, 1280]
#         x = self.maxpool1(weighted_embeddings.float().to('cuda'))
#         x = self.conv2(x)
#         x = F.relu(x)
#         x = self.maxpool2(x)
#         x = self.flatten(x)
#         #mean_sequence_embeddings = torch.sum(weighted_embeddings, axis=1) / torch.sum(at_cpu, axis=-1).unsqueeze(-1)
#         logits = self.fc(x)
#         return logits, weighted_embeddings
#     def set_flatten_size(self, input_ids, attention_mask):
#         torch_outs = self.bert(
#             input_ids,
#             attention_mask=attention_mask,
#             encoder_attention_mask=attention_mask,
#             output_hidden_states=True
#         )
#         embeddings = torch_outs['hidden_states'][-1].cpu().detach().numpy()
#         at_cpu = attention_mask.cpu()
#         at_cpu_reshaped = at_cpu.unsqueeze(-1)  # Shape becomes [32, 200, 1]
#         weighted_embeddings = (at_cpu_reshaped * embeddings).float()  # Shape will be [32, 200, 1280]
#         x = self.maxpool1(weighted_embeddings)
#         x = self.conv2(x)
#         x = F.relu(x)
#         x = self.maxpool2(x)
#         x = self.flatten(x)
#         self.flatten_size = x.shape[1]
#
#
# def save(mdl, file_name):
#     data_reader.save_dic(file_name+'_bert.pkl', mdl.bert)
#     torch.save(mdl.fc.state_dict(), file_name+'_torchnn.pth')
#
# def load_clf_model(file_name):
#     return DNASequenceClassifier(data_reader.load_dic(file_name+'_bert.pkl'), file_name+'_torchnn.pth', 2)
#
# class DNADataset(Dataset):
#     def __init__(self, sequences, labels, tokenizer, max_length):
#         self.sequences = sequences
#         self.labels = labels
#         self.tokenizer = tokenizer
#         self.max_length = max_length
#     def __len__(self):
#         return len(self.sequences)
#     def __getitem__(self, idx):
#         sequence = self.sequences[idx]
#         label = self.labels[idx]
#         # Tokenize sequence
#         encoded_sequence = self.tokenizer(
#             bertprep.seq2kmer(sequence, int(kmer)),
#             max_length=self.max_length,
#             padding='max_length',
#             truncation=True,
#             return_tensors='pt'
#         )
#         input_ids = encoded_sequence["input_ids"].squeeze()
#         attention_mask = encoded_sequence["attention_mask"].squeeze()
#         return {
#             "input_ids": input_ids,
#             "attention_mask": attention_mask,
#             "label": label,
#             "sequence": sequence
#         }
#
#
#
# X = pd.read_csv('X.csv', header=None)
# Y = pd.read_csv('Y.csv', header=None)
# X = X[1][1:]
# Y = Y[1]
# Y = Y[1:]
#
# # X = X[:1000]
# # Y = Y[:1000]
#
# Y = np.asarray(pd.cut(Y, bins=2, labels=[0, 1], right=False))
# b = np.zeros((Y.size, Y.max()+1))
# b[np.arange(Y.size), Y] = 1
# Y = b
# X__ = X
# Y__ = Y
#
# x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=0.1, random_state=None)
#
# # Set batch size and max sequence length
# batch_size = 32
# max_sequence_length = 200
#
# labels = torch.tensor(y_train, dtype=torch.float32)
# dataset = DNADataset(x_train.tolist(), labels, tokenizer, max_sequence_length)
# data_loader_train = DataLoader(dataset, batch_size=batch_size, shuffle=True)
#
# labels = torch.tensor(y_test, dtype=torch.float32)
# dataset = DNADataset(x_test.tolist(), labels, tokenizer, max_sequence_length)
# data_loader_test = DataLoader(dataset, batch_size=batch_size, shuffle=True)
#
#
#
# from transformers import AutoTokenizer, AutoModelForMaskedLM
# import torch
#
# # Import the tokenizer and the model
# tokenizer = AutoTokenizer.from_pretrained("InstaDeepAI/nucleotide-transformer-500m-human-ref")
# model = AutoModelForMaskedLM.from_pretrained("InstaDeepAI/nucleotide-transformer-500m-human-ref")
#
# batch = next(iter(data_loader_train))
# num_classes = 2  # Binary classification
# clf_model = DNASequenceClassifier(model, None, num_classes, batch['input_ids'], batch['attention_mask'])
#
#
#
# torch_outs = clf_model.bert(input_ids, attention_mask=attention_mask, encoder_attention_mask=attention_mask, output_hidden_states=True)
# logits = clf_model.fc(mean_sequence_embeddings.to('cuda'))
