import sys
import os

project_root = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(project_root)


# import configs
import preprocess.data_reader as data_reader
# import preprocess.preprocess as preprocess
# import process.process as process
import torch.optim as optim
from tqdm import tqdm
import pandas as pd
import torch
from torch.utils.data import DataLoader
import torch.nn as nn
from sklearn.model_selection import train_test_split
import numpy as np
import argparse
from transformers import BertTokenizer, BertModel
import configs
from utils.model_manager import DNASequenceClassifier
from utils.data_manager import DNADataset
from utils import data_manager as dmngr
from transformers import AutoModel, AutoTokenizer, AutoModelForMaskedLM
from preprocess import preprocess
import torch
import torch.nn as nn
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn.metrics import precision_score, recall_score, f1_score, confusion_matrix
#from other_methods.hyeanaDNA import HyenaDNAPreTrainedModel, CharacterTokenizer

#python es_prediction/DNABERT_finetune.py -kmer 6 -KO TET -ws 512 -bs 16 -m BERT -sp homo -im True

kmer = 6
KO = 'TET'
window_size = 512
model_type = 'BERT'
batch_size = 32
species = 'homo'
include_methylation = 'none'
mc_num = 40
d_size = 100000

parser = argparse.ArgumentParser()
parser.add_argument('-kmer', '--kmer', help='kmer', required=True)
parser.add_argument('-ws', '--window_size', help='window_size', required=True)
parser.add_argument('-KO', '--knockout', help='TET, DNMT3, QKO, PKO, mouse_3aKO, mouse_3bKO, mouse_SI_TET23_KO', required=True)
parser.add_argument('-im', '--include_methylation', help='include methylation, none, wt, ko, wt-ko', required=False, default='none')
parser.add_argument('-bs', '--batch_size', help='batch size', required=False, default=32)
parser.add_argument('-sp', '--species', help='homo, mouse, mouse_si', required=False, default='homo')
parser.add_argument('-ds', '--data_size', help='data size', required=False, default=100000)
parser.add_argument('-s', '--save', help='Save the predicted labels and classifier model?', required=False, default=False)
args = parser.parse_args()

kmer = int(args.kmer)
KO = args.knockout
window_size = int(args.window_size)
batch_size = int(args.batch_size)
species = args.species
include_methylation = args.include_methylation
d_size = int(args.data_size)
do_save = bool(args.save)
do_save = True


class DNASequenceFeatureEncoder(nn.Module):
    def __init__(self, bert_model, meth_window_size):
        super(DNASequenceFeatureEncoder, self).__init__()
        self.bert = bert_model
        self.meth_window_size = meth_window_size
    def forward(self, input_ids, attention_mask, methylations):
        outputs = self.bert(input_ids=input_ids, attention_mask=attention_mask)
        pooled_output = outputs[1]  # Pooled embeddings from BERT
        # Concatenate with methylation features
        pooled_output = torch.cat((pooled_output, methylations), dim=1).to(torch.float32)
        return pooled_output  # Return feature embeddings instead of logits


model_name = "zhihan1996/DNA_bert_"+str(kmer)
model_name_alias = 'brt'
model = BertModel.from_pretrained(model_name, num_labels=2, finetuning_task="dnaprom", cache_dir=None)
tokenizer = BertTokenizer.from_pretrained(model_name, trust_remote_code=True)
meth_ws = {'none': 0, 'wt': window_size, 'ko': window_size, 'wt-ko': 2*window_size}
clf_model = DNASequenceFeatureEncoder(model, window_size)

cnfg_dic = {'homo': configs.hg19, 'mouse': configs.mouse, 'mouse_si': configs.mouse_si}
cnfg = cnfg_dic[species]
assembly = data_reader.readfasta(cnfg['assembly'])
dmrs, not_dmrs = dmngr.load_df(KO)
meth_seq_lst = None





train_sample_size = int(min(d_size * 0.9, len(dmrs) * 2, len(not_dmrs) * 2))
train_dataset = pd.concat([dmngr.make_input_dataset(assembly, dmrs.sample(int(train_sample_size // 2)), 1, window_size=window_size, meth_seq_lst=meth_seq_lst, mc_num=20),
                     dmngr.make_input_dataset(assembly, not_dmrs.sample(int(train_sample_size // 2)), 0, window_size=window_size, meth_seq_lst=meth_seq_lst, mc_num=20)])
#dataset_ = pd.read_csv('./dump_files/sample_dataset.csv', header=None, index_col=None, names=['seq', 'meth', 'label'])
train_dataset = train_dataset.sample(frac=1).reset_index(drop=True)
train_dataset = train_dataset[train_dataset['seq'].str.len() == window_size]

train_include_dic = dmngr.make_train_included_dic(assembly, train_dataset, window_size)
test_dmrs, test_not_dmrs = dmngr.make_test_coordinate_df(train_include_dic, dmrs), dmngr.make_test_coordinate_df(train_include_dic, not_dmrs)
test_sample_size = int(min(d_size * 0.1, len(test_dmrs) * 2, len(test_not_dmrs) * 2))

test_dataset = pd.concat([dmngr.make_input_dataset(assembly, test_dmrs.sample(int(test_sample_size // 2)), 1, window_size=window_size, meth_seq_lst=meth_seq_lst, mc_num=20),
                     dmngr.make_input_dataset(assembly, test_not_dmrs.sample(int(test_sample_size // 2)), 0, window_size=window_size, meth_seq_lst=meth_seq_lst, mc_num=20)])
test_dataset = test_dataset.sample(frac=1).reset_index(drop=True)
test_dataset = test_dataset[test_dataset['seq'].str.len() == window_size]

print("Non overlapping test set is created!!")

assert preprocess.find_closest_distances(train_dataset, test_dataset)['closest_distance'].min() >= window_size//2

x_train, y_train = train_dataset[[col for col in train_dataset.columns if col not in ['label', 'chr', 'position']]], dmngr.prepare_y(train_dataset['label'])
x_test, y_test = test_dataset[[col for col in test_dataset.columns if col not in ['label', 'chr', 'position']]], dmngr.prepare_y(test_dataset['label'])


# Set batch size and max sequence length
labels = torch.tensor(y_train, dtype=torch.float32)
dataset = DNADataset(x_train, labels, tokenizer, window_size, kmer)
data_loader_train = DataLoader(dataset, batch_size=batch_size, shuffle=True)

labels = torch.tensor(y_test, dtype=torch.float32)
dataset = DNADataset(x_test, labels, tokenizer, window_size, kmer)
data_loader_test = DataLoader(dataset, batch_size=batch_size, shuffle=True)

optimizer = optim.Adam(clf_model.parameters(), lr=1e-5)
criterion = nn.BCELoss()
clf_model.to('cuda')
# Set the number of epochs
num_epochs = 5
total_samples = len(data_loader_train.dataset)  # Total number of samples
batch_size = data_loader_train.batch_size  # Batch size
batch_size_test = data_loader_test.batch_size
total_samples_test = len(data_loader_test.dataset)

clf_model.eval()

features = np.zeros([train_sample_size, 768])
trgts = np.zeros([train_sample_size, 2])
batch_idx = 0
with torch.no_grad():
    total_loss = 0
    total_correct = 0
    total_processed = 0  # To keep track of total processed samples
    progress_bar = tqdm(data_loader_train, desc=f"-----", leave=False)
    for batch in progress_bar:
        #break
        input_ids = batch["input_ids"].to('cuda')
        attention_mask = batch["attention_mask"].to('cuda')
        target_labels = batch["label"].to('cuda').to(torch.float32)
        methylations = batch["methylations"].to('cuda')
        embeddings = clf_model(input_ids, attention_mask, methylations)
        batch_sz = embeddings.shape[0]
        features[batch_idx:batch_idx+batch_sz] = embeddings.to('cpu').numpy()
        trgts[batch_idx:batch_idx+batch_sz] = target_labels.to('cpu').numpy()
        batch_idx += batch_sz

y_train = trgts[:, 0]
X_train = features

clf_rf = RandomForestClassifier(n_estimators=100, random_state=42, n_jobs=-1)
clf_rf.fit(X_train, y_train)

clf_svc = SVC(kernel='rbf', C=1.0, random_state=42)
clf_svc.fit(X_train, y_train)


features = np.zeros([test_sample_size, 768])
trgts = np.zeros([test_sample_size, 2])
batch_idx = 0
with torch.no_grad():
    total_loss = 0
    total_correct = 0
    total_processed = 0  # To keep track of total processed samples
    progress_bar = tqdm(data_loader_test, desc=f"-----", leave=False)
    for batch in progress_bar:
        #break
        input_ids = batch["input_ids"].to('cuda')
        attention_mask = batch["attention_mask"].to('cuda')
        target_labels = batch["label"].to('cuda').to(torch.float32)
        methylations = batch["methylations"].to('cuda')
        embeddings = clf_model(input_ids, attention_mask, methylations)
        batch_sz = embeddings.shape[0]
        features[batch_idx:batch_idx+batch_sz] = embeddings.to('cpu').numpy()
        trgts[batch_idx:batch_idx+batch_sz] = target_labels.to('cpu').numpy()
        batch_idx += batch_sz

y_test = trgts[:, 0]
X_test = features


def acc_calc(a, b):
    predicted_labels = b
    true_labels = a
    accuracy = np.mean(predicted_labels == true_labels)
    precision = precision_score(true_labels, predicted_labels)
    sensitivity = recall_score(true_labels, predicted_labels)  # Sensitivity is the same as recall
    f1 = f1_score(true_labels, predicted_labels)
    tn, fp, fn, tp = confusion_matrix(true_labels, predicted_labels).ravel()
    specificity = tn / (tn + fp)
    return accuracy, precision, sensitivity, specificity, f1


y_pred = clf_rf.predict(X_test)
accuracy, precision, sensitivity, specificity, f1 = acc_calc(y_test, y_pred)
with open("BERT_RF_nodata_leakage.txt", "a") as file_object:
    file_object.write(f'{str(kmer)}\t{str(window_size)}\t{str(KO)}\t{str(accuracy)}\t{str(precision)}\t{str(sensitivity)}\t{str(specificity)}\t{f1}\trf')
    file_object.write("\n")

y_pred = clf_svc.predict(X_test)
accuracy, precision, sensitivity, specificity, f1 = acc_calc(y_test, y_pred)
with open("BERT_RF_nodata_leakage.txt", "a") as file_object:
    file_object.write(f'{str(kmer)}\t{str(window_size)}\t{str(KO)}\t{str(accuracy)}\t{str(precision)}\t{str(sensitivity)}\t{str(specificity)}\t{f1}\tsvc')
    file_object.write("\n")
