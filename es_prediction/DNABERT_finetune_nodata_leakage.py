import sys
import os

project_root = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(project_root)


# import configs
import preprocess.data_reader as data_reader
# import preprocess.preprocess as preprocess
# import process.process as process
import torch.optim as optim
from tqdm import tqdm
import pandas as pd
import torch
from torch.utils.data import DataLoader
import torch.nn as nn
from sklearn.model_selection import train_test_split
import numpy as np
import argparse
from transformers import BertTokenizer, BertModel
import configs
from utils.model_manager import DNASequenceClassifier
from utils.data_manager import DNADataset
from utils import data_manager as dmngr
from transformers import AutoModel, AutoTokenizer, AutoModelForMaskedLM
from preprocess import preprocess
#from other_methods.hyeanaDNA import HyenaDNAPreTrainedModel, CharacterTokenizer

#python es_prediction/DNABERT_finetune.py -kmer 6 -KO TET -ws 512 -bs 16 -m BERT -sp homo -im True

kmer = 6
KO = 'TET'
window_size = 512
model_type = 'BERT'
batch_size = 32
species = 'homo'
include_methylation = 'none'
mc_num = 40
d_size = 100000

parser = argparse.ArgumentParser()
parser.add_argument('-kmer', '--kmer', help='kmer', required=True)
parser.add_argument('-ws', '--window_size', help='window_size', required=True)
parser.add_argument('-KO', '--knockout', help='TET, DNMT3, QKO, PKO, mouse_3aKO, mouse_3bKO, mouse_SI_TET23_KO', required=True)
parser.add_argument('-m', '--model', help='model, BERT, BigBird, nucleotide-transformer', required=False, default='BERT')
parser.add_argument('-im', '--include_methylation', help='include methylation, none, wt, ko, wt-ko', required=False, default='none')
parser.add_argument('-bs', '--batch_size', help='batch size', required=False, default=32)
parser.add_argument('-sp', '--species', help='homo, mouse, mouse_si', required=False, default='homo')
parser.add_argument('-ds', '--data_size', help='data size', required=False, default=100000)
parser.add_argument('-s', '--save', help='Save the predicted labels and classifier model?', required=False, default=False)
args = parser.parse_args()

kmer = int(args.kmer)
KO = args.knockout
window_size = int(args.window_size)
model_type = args.model
batch_size = int(args.batch_size)
species = args.species
include_methylation = args.include_methylation
d_size = int(args.data_size)
do_save = bool(args.save)
do_save = True


num_classes = 2  # Binary classification

if model_type == 'BigBird':
    from transformers import BigBirdModel, BigBirdTokenizer
    model_name = "google/bigbird-roberta-base"
    model_name_alias = 'bigbird'
    tokenizer = BigBirdTokenizer.from_pretrained(model_name)
    model = BigBirdModel.from_pretrained(model_name, num_labels=2)
    meth_ws = {'none': 0, 'wt': window_size, 'ko': window_size, 'wt-ko': 2*window_size}
    clf_model = DNASequenceClassifier(model, None, num_classes, meth_window_size=meth_ws[include_methylation])
elif model_type == 'BERT2':
    from transformers import BertConfig, AutoModel, AutoTokenizer
    model_name = 'zhihan1996/DNABERT-2-117M'
    model_name_alias = 'brt2'
    tokenizer = AutoTokenizer.from_pretrained("zhihan1996/DNABERT-2-117M", trust_remote_code=True)
    config = BertConfig.from_pretrained("zhihan1996/DNABERT-2-117M")
    model = AutoModel.from_config(config)
    meth_ws = {'none': 0, 'wt': window_size, 'ko': window_size, 'wt-ko': 2*window_size}
    clf_model = DNASequenceClassifier(model, None, num_classes, meth_window_size=meth_ws[include_methylation])
elif model_type == 'nucleotide-transformer':
    if species == 'homo':
        model_name = "InstaDeepAI/nucleotide-transformer-500m-human-ref"
    elif species == 'mouse':
        model_name = 'InstaDeepAI/nucleotide-transformer-v2-50m-multi-species'
    model_name_alias = 'nt'
    tokenizer = AutoTokenizer.from_pretrained(model_name)
    model = AutoModel.from_pretrained(model_name)
    meth_ws = {'none': 0, 'wt': window_size, 'ko': window_size, 'wt-ko': 2*window_size}
    clf_model = DNASequenceClassifier(model, None, num_classes, meth_window_size=meth_ws[include_methylation])
# elif model_type == 'hyena': # After adding the methylaiton option the DNASequence classifier and DNADATAset class of hyeana_finetune is not updadated. Go and use othermethods.hyeana_finetune.py
#     from other_methods.hyeana_finetune import DNASequenceClassifier
#     from other_methods.hyeana_finetune import DNADataset
#     pretrained_model_name = 'hyenadna-medium-450k-seqlen'
#     model_name_alias = 'hyna'
#     max_length = 512
#     model = HyenaDNAPreTrainedModel.from_pretrained('./checkpoints', pretrained_model_name,)
#     # create tokenizer, no training involved :)
#     tokenizer = CharacterTokenizer(characters=['A', 'C', 'G', 'T', 'N'], model_max_length=max_length,)
#     clf_model = DNASequenceClassifier(model, None, num_classes)
elif model_type == 'BERT':
    model_name = "zhihan1996/DNA_bert_"+str(kmer)
    model_name_alias = 'brt'
    model = BertModel.from_pretrained(model_name, num_labels=2, finetuning_task="dnaprom", cache_dir=None)
    tokenizer = BertTokenizer.from_pretrained(model_name, trust_remote_code=True)
    meth_ws = {'none': 0, 'wt': window_size, 'ko': window_size, 'wt-ko': 2*window_size}
    clf_model = DNASequenceClassifier(model, None, num_classes, meth_window_size=meth_ws[include_methylation])

cnfg_dic = {'homo': configs.hg19, 'mouse': configs.mouse, 'mouse_si': configs.mouse_si}
cnfg = cnfg_dic[species]
assembly = data_reader.readfasta(cnfg['assembly'])
dmrs, not_dmrs = dmngr.load_df(KO)

if include_methylation != 'none':
    meth_seq_lst = []
    if 'wt' in include_methylation:
        methylations = pd.read_csv(cnfg_dic[species]['WT_methylation'], header=None, sep='\t', names=['chr', 'position', 'end', 'meth', 'coverage']) #10sec
        meth_seq_lst.append(preprocess.make_methseq_dic(cnfg_dic[species]['og'], methylations, assembly, configs.rc['ct']))
        print('methylation loaded for wild type')
    if 'ko' in include_methylation:
        methylations = pd.read_csv(cnfg_dic[species]['KO_methylations'][KO], header=None, sep='\t', names=['chr', 'position', 'end', 'meth', 'coverage']) #10sec
        meth_seq_lst.append(preprocess.make_methseq_dic(cnfg_dic[species]['og'], methylations, assembly, configs.rc['ct']))
        print('methylation loaded for the knock out {}'.format(KO))
else:
    meth_seq_lst = None
    print('no methylation included')

print('The data loaded for {}. dmrs size {}, not_dmrs size {}'.format(KO, str(len(dmrs)), str(len(not_dmrs))))

train_sample_size = int(min(d_size * 0.9, len(dmrs) * 2, len(not_dmrs) * 2))
train_dataset = pd.concat([dmngr.make_input_dataset(assembly, dmrs.sample(int(train_sample_size // 2)), 1, window_size=window_size, meth_seq_lst=meth_seq_lst, mc_num=20),
                     dmngr.make_input_dataset(assembly, not_dmrs.sample(int(train_sample_size // 2)), 0, window_size=window_size, meth_seq_lst=meth_seq_lst, mc_num=20)])
#dataset_ = pd.read_csv('./dump_files/sample_dataset.csv', header=None, index_col=None, names=['seq', 'meth', 'label'])
train_dataset = train_dataset.sample(frac=1).reset_index(drop=True)
train_dataset = train_dataset[train_dataset['seq'].str.len() == window_size]

train_include_dic = dmngr.make_train_included_dic(assembly, train_dataset, window_size)
test_dmrs, test_not_dmrs = dmngr.make_test_coordinate_df(train_include_dic, dmrs), dmngr.make_test_coordinate_df(train_include_dic, not_dmrs)
test_sample_size = int(min(d_size * 0.1, len(test_dmrs) * 2, len(test_not_dmrs) * 2))

test_dataset = pd.concat([dmngr.make_input_dataset(assembly, test_dmrs.sample(int(test_sample_size // 2)), 1, window_size=window_size, meth_seq_lst=meth_seq_lst, mc_num=20),
                     dmngr.make_input_dataset(assembly, test_not_dmrs.sample(int(test_sample_size // 2)), 0, window_size=window_size, meth_seq_lst=meth_seq_lst, mc_num=20)])
test_dataset = test_dataset.sample(frac=1).reset_index(drop=True)
test_dataset = test_dataset[test_dataset['seq'].str.len() == window_size]

print("Non overlapping test set is created!!")

assert preprocess.find_closest_distances(train_dataset, test_dataset)['closest_distance'].min() >= window_size//2

x_train, y_train = train_dataset[[col for col in train_dataset.columns if col not in ['label', 'chr', 'position']]], dmngr.prepare_y(train_dataset['label'])
x_test, y_test = test_dataset[[col for col in test_dataset.columns if col not in ['label', 'chr', 'position']]], dmngr.prepare_y(test_dataset['label'])


# Set batch size and max sequence length
labels = torch.tensor(y_train, dtype=torch.float32)
dataset = DNADataset(x_train, labels, tokenizer, window_size, kmer)
data_loader_train = DataLoader(dataset, batch_size=batch_size, shuffle=True)

labels = torch.tensor(y_test, dtype=torch.float32)
dataset = DNADataset(x_test, labels, tokenizer, window_size, kmer)
data_loader_test = DataLoader(dataset, batch_size=batch_size, shuffle=True)

optimizer = optim.Adam(clf_model.parameters(), lr=1e-5)
criterion = nn.BCELoss()
clf_model.to('cuda')
# Set the number of epochs
num_epochs = 5
total_samples = len(data_loader_train.dataset)  # Total number of samples
batch_size = data_loader_train.batch_size  # Batch size
batch_size_test = data_loader_test.batch_size
total_samples_test = len(data_loader_test.dataset)
# Training loop
for epoch in range(num_epochs):
    clf_model.train()
    total_loss = 0
    total_correct = 0
    total_processed = 0  # To keep track of total processed samples
    progress_bar = tqdm(data_loader_train, desc=f"Epoch {epoch+1}/{num_epochs}", leave=False)
    for batch in progress_bar:
        input_ids = batch["input_ids"].to('cuda')
        attention_mask = batch["attention_mask"].to('cuda')
        target_labels = batch["label"].to('cuda').to(torch.float32)
        methylations = batch["methylations"].to('cuda')
        optimizer.zero_grad()
        logits, _ = clf_model(input_ids, attention_mask, methylations=methylations)
        loss = criterion(logits, target_labels)
        loss.backward()
        optimizer.step()
        predictions = logits.argmax(dim=1)
        total_correct += (predictions == target_labels.argmax(dim=1)).sum().item()
        total_processed += batch_size
        if total_processed % (10*batch_size) == 0:
            b_acc = total_correct / total_processed
            progress_percentage = (total_processed / total_samples) * 100
            progress_bar.set_postfix(loss=loss.item(), batch_acc=b_acc, progress=f"{progress_percentage:.2f}%")
        total_loss += loss.item()
    epoch_accuracy = total_correct / total_samples
    average_loss = total_loss / len(data_loader_train)
    print(f"Epoch [{epoch+1}/{num_epochs}] - Average Loss: {average_loss:.4f}, Epoch Accuracy: {epoch_accuracy:.4f}")
    # Evaluate the model on test data
clf_model.eval()
test_correct = 0
#keep track of the predicted and labels to draw the ROC curve
predicted_test, labels_test = np.zeros((total_samples_test, 2)) - 1, np.zeros((total_samples_test, 2)) - 1
idx = 0
with torch.no_grad():
    for batch in data_loader_test:
        input_ids = batch["input_ids"].to('cuda')
        attention_mask = batch["attention_mask"].to('cuda')
        target_labels = batch["label"].to('cuda')
        methylations = batch["methylations"].to('cuda')
        logits, _ = clf_model(input_ids, attention_mask, methylations)
        predicted_labels = logits.argmax(dim=1)
        test_correct += (predicted_labels == target_labels.argmax(dim=1)).sum().item()
        predicted_test[idx: idx + len(logits)] = logits.cpu().numpy()
        labels_test[idx: idx + len(target_labels)] = target_labels.cpu().numpy()
        idx += len(logits)

assert -1 not in predicted_test and -1 not in labels_test
if not os.path.exists("./dump_files/pred_results/nodata_leakage/"): os.makedirs("./dump_files/pred_results/nodata_leakage/")
import random
KO = str(KO) + str(random.randint(0, 10000))
if do_save:
    np.save("./dump_files/pred_results/nodata_leakage/predicted_{}_{}_{}_{}_{}_include_meth_{}.npy".format(str(cnfg['og']), str(kmer), str(window_size), str(KO), model_name_alias, str(include_methylation)), predicted_test)
    np.save("./dump_files/pred_results/nodata_leakage/labels_{}_{}_{}_{}_{}_include_meth_{}.npy".format(str(cnfg['og']), str(kmer), str(window_size), str(KO), model_name_alias, str(include_methylation)), labels_test)

test_accuracy = test_correct / total_samples_test
with open("kmer_test_BERT_classification_results_nodata_leakage.txt", "a") as file_object:
    file_object.write(f'{str(kmer)}\t{str(window_size)}\t{str(KO)}\t{str(test_accuracy)}\t{str(model_type)}\t{str(include_methylation)}\t{str(d_size)}\t{len(x_train)}\t{len(x_test)}')
    file_object.write("\n")

if not os.path.exists("./dump_files/pretrained_models/nodata_leakage/"): os.makedirs("./dump_files/pretrained_models/nodata_leakage/")
#if do_save:
    #clf_model.save("./dump_files/pretrained_models/nodata_leakage/" + "kmer_"+str(kmer) + "_ws_" + str(window_size) + "_KO_" + str(KO) + "_model_" + str(model_type) + '_methyl' + str(include_methylation))
