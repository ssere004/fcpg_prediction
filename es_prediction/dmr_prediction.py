import sys
import os

project_root = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(project_root)


import configs
import pandas as pd
import tensorflow as tf
import preprocess.data_reader as data_reader
import preprocess.preprocess as preprocess
import process.process as process
from sklearn.model_selection import train_test_split
from tensorflow.keras.layers import Activation,Dense
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Dropout, Flatten, Reshape
from sklearn.metrics import accuracy_score, classification_report, confusion_matrix, f1_score, precision_score, recall_score
from tensorflow.keras.constraints import max_norm
from tensorflow.keras.losses import binary_crossentropy
import numpy as np
import random
import argparse
from sklearn.ensemble import RandomForestClassifier
import argparse
import utils.data_manager as dmngr


parser = argparse.ArgumentParser()
parser.add_argument('-ws', '--window_size', help='window_size', required=False, default=512)
parser.add_argument('-sp', '--species', help='homo, mouse', required=False, default='homo')
parser.add_argument('-KO', '--knockout', help='TET, DNMT3, QKO, PKO, mouse_3aKO, mouse_3bKO, mouse_SI_TET23_KO', required=False, default='TET')
args = parser.parse_args()

KO = args.knockout
window_size = int(args.window_size)
species = args.species


if species == 'homo':
    cnfg = configs.hg19
elif species == 'mouse':
    cnfg = configs.mouse
assembly = data_reader.readfasta(cnfg['assembly'])
#assembly = data_reader.readfasta('/home/csgrads/ssere004/Organisms/homo_sapiens/hg19/hg19.fa')
dmrs, not_dmrs = dmngr.load_df(KO)
rc = configs.rc
sequences_df = preprocess.convert_assembely_to_onehot(cnfg['og'], data_reader.readfasta(cnfg['assembly']), from_file=False)
#sequences_df = data_reader.load_dic('seq_dic.pkl')

sequences_df['chrX'] = sequences_df.pop('chrx')
sequences_df['chrY'] = sequences_df.pop('chry')

annot_seq = preprocess.make_annotseq_dic(data_reader.read_annot(cnfg['gene_annotation']), ['gene'], sequences_df, strand_spec=False)
methylations = pd.read_csv(cnfg['WT_methylation'], header=None, sep='\t', names=['chr', 'position', 'end', 'meth', 'coverage']) #10sec
meth_seq = preprocess.make_methseq_dic(cnfg['og'], methylations, sequences_df, rc['ct'])
#meth_seq = data_reader.load_dic('meth_seq_dic.pkl')
meth_seq['chrX'] = meth_seq.pop('chrx')
meth_seq['chrY'] = meth_seq.pop('chry')

X, Y = process.input_maker([sequences_df, meth_seq], [dmrs.sample(n=min(50000, min(len(dmrs), len(not_dmrs)))), not_dmrs.sample(n=min(50000, min(len(dmrs), len(not_dmrs))))], window_size=window_size)

Y = np.asarray(pd.cut(Y, bins=2, labels=[0, 1], right=False))
b = np.zeros((Y.size, Y.max()+1))
b[np.arange(Y.size), Y] = 1
Y = b

x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=0.1, random_state=None)
x_val, x_test, y_val, y_test = train_test_split(x_test, y_test, test_size=0.5, random_state=None)

# clf = RandomForestClassifier(random_state=0, n_estimators=50, warm_start=True, n_jobs=-1)
#
# xx = x_train
# nsamples, nx, ny, nz = xx.shape
# xx = xx.reshape((nsamples, nx*ny))
# yy = y_train
# clf.fit(xx, yy)
# clf.n_estimators += 100
# nsamples, nx, ny, nz = x_test.shape
# x_test = x_test.reshape((nsamples, nx*ny))
# y_pred=clf.predict(x_test)


W_maxnorm = 3
model = Sequential()
model.add(Conv2D(256, kernel_size=(10, 4), activation='relu', input_shape=(X.shape[1], X.shape[2], 1), padding='same', kernel_constraint=max_norm(W_maxnorm)))
model.add(MaxPooling2D(pool_size=(8, 2), strides=(1, 3)))
model.add(Conv2D(128, kernel_size=(8, 2), activation='relu', padding='same', kernel_constraint=max_norm(W_maxnorm)))
model.add(MaxPooling2D(pool_size=(10, 2), strides=(1, 3)))
model.add(Flatten())
model.add(Dense(64, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(128, activation='relu'))
model.add(Dropout(0.6))
model.add(Dense(2))
model.add(Activation('softmax'))
myoptimizer = tf.keras.optimizers.Adam(learning_rate=1e-5)
model.compile(loss=binary_crossentropy, optimizer=myoptimizer, metrics=['accuracy'])
with tf.device('/device:GPU:0'):
    #print('model fitting started for %s and %s    %s --> %s' % (cnfg_ref['og'], cnfg_snp['og'], control_bp, test_bp))
    model.fit(x_train,  y_train, batch_size=32, epochs=10, verbose=1, validation_data=(x_val, y_val))
    y_pred = model.predict(x_test)
acc = accuracy_score(y_test, y_pred.round())

with open("seq_meth_pred_results.txt", "a") as file_object:
    file_object.write('%s\t%s\t%s\t%s' %(str(window_size), str(len(X)), str(acc), str(KO)))
    file_object.write("\n")



def convert_y(Y):
    Y = np.asarray(pd.cut(Y, bins=2, labels=[0, 1], right=False))
    b = np.zeros((Y.size, Y.max()+1))
    b[np.arange(Y.size), Y] = 1
    return b











