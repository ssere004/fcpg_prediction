import sys
import os

project_root = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(project_root)

import torch
import torch.nn as nn
import torch.optim as optim
import pandas as pd
from preprocess import data_reader as data_reader
import preprocess.preprocess as preprocess
from torch.utils.data import Dataset, DataLoader
import configs as configs
from tqdm import tqdm
import torch.nn.functional as F
import argparse
import utils.data_manager as dmngr

W_maxnorm = 3

class ConvNet(nn.Module):
    def __init__(self, x):
        super(ConvNet, self).__init__()
        self.conv1 = nn.Conv2d(1, 256, kernel_size=(10, 4), padding=(5, 2), bias=False) #[32, 256, 121, 5]
        self.maxpool1 = nn.MaxPool2d(kernel_size=(8, 2), stride=(1, 3)) #[32, 256, 114, 2]
        self.conv2 = nn.Conv2d(256, 128, kernel_size=(8, 2), padding=(4, 1), bias=False)
        self.maxpool2 = nn.MaxPool2d(kernel_size=(10, 2), stride=(1, 3))#[32, 128, 106, 1]
        self.flatten = nn.Flatten()
        self.set_flatten_size(x)
        self.fc1 = nn.Linear(self.flatten_size, 64)
        self.dropout1 = nn.Dropout(0.5)
        self.fc2 = nn.Linear(64, 128)
        self.dropout2 = nn.Dropout(0.6)
        self.fc3 = nn.Linear(128, 2)
        self.softmax = nn.Softmax(dim=1)
    def forward(self, x):
        x = self.conv1(x)
        x = F.relu(x)
        x = self.maxpool1(x)
        x = self.conv2(x)
        x = F.relu(x)
        x = self.maxpool2(x)
        x = self.flatten(x)
        x = self.fc1(x)
        x = F.relu(x)
        x = self.dropout1(x)
        x = self.fc2(x)
        x = F.relu(x)
        x = self.dropout2(x)
        x = self.fc3(x)
        x = self.softmax(x)
        return x
    def set_flatten_size(self, x):
        x = self.conv1(x)
        x = F.relu(x)
        x = self.maxpool1(x)
        x = self.conv2(x)
        x = F.relu(x)
        x = self.maxpool2(x)
        x = self.flatten(x)
        self.flatten_size = x.shape[1]


class DNADataset(Dataset):
    def __init__(self, dataset_df):
        super(DNADataset, self).__init__()
        self.dataset_df = dataset_df
        self.label_map = {'A': torch.Tensor([1, 0, 0, 0]), 'C': torch.Tensor([0, 1, 0, 0]), 'G': torch.Tensor([0, 0, 1, 0]), 'T': torch.Tensor([0, 0, 0, 1])}
        self.other_char = torch.Tensor([0, 0, 0, 0])
    def __len__(self):
        return len(self.dataset_df)
    def __getitem__(self, idx):
        label = self.dataset_df.loc[idx, 'label']
        seq = self.dataset_df.loc[idx, 'seq']
        extra_letters = {'M', 'B', 'S', 'W', 'R', 'Y', 'N', 'K'}
        for ll in extra_letters:
            seq = seq.replace(ll, 'N')
        one_hot_seq = torch.zeros(len(seq), 4)
        for i, char in enumerate(seq):
            if char in self.label_map:
                one_hot_seq[i] = self.label_map[char]
            else:
                one_hot_seq[i] = self.other_char
        if 'meth' in self.dataset_df.columns: one_hot_seq = torch.cat((one_hot_seq, torch.Tensor(self.dataset_df.loc[idx, 'meth']).view(-1, 1)), dim=1)
        return torch.tensor([0.0, 1.0]) if label == 1 else torch.tensor([1.0, 0.0]), one_hot_seq.unsqueeze(0)

parser = argparse.ArgumentParser()
parser.add_argument('-ws', '--window_size', help='window_size', required=False, default=512)
parser.add_argument('-sp', '--species', help='homo, mouse', required=False, default='homo')
parser.add_argument('-im', '--include_methylation', help='include methylation, wt, ko, none', required=False, default='none')
parser.add_argument('-KO', '--knockout', help='TET, DNMT3, QKO, PKO, mouse_3aKO, mouse_3bKO, mouse_SI_TET23_KO', required=False, default='TET')
args = parser.parse_args()

kmer = 6
KO = 'mouse_3bKO'
window_size = 512
batch_size = 32
species = 'mouse'
include_methylation = 'none'
d_size = 100000

KO = args.knockout
window_size = int(args.window_size)
species = args.species
include_methylation = args.include_methylation

# def tmp_func(idx):
#     label = dataset_df.loc[idx, 'label']
#     seq = dataset_df.loc[idx, 'seq']
#     extra_letters = {'M', 'B', 'S', 'W', 'R', 'Y', 'N', 'K'}
#     for ll in extra_letters:
#         seq = seq.replace(ll, 'N')
#     one_hot_seq = torch.zeros(len(seq), 5)
#     for i, char in enumerate(seq):
#         if char in label_map:
#             one_hot_seq[i] = label_map[char]
#         else:
#             one_hot_seq[i] = other_char
#     return label, one_hot_seq

def make_input_dataset(assembly, coordinate_df, label, meth_seq=None, window_size=512):
    res_df = pd.DataFrame()
    res_df['seq'] = coordinate_df.apply(lambda row: str(assembly[row['chr'].lower()][int(row['position']) - int(window_size//2): int(row['position']) + int(window_size//2)]), axis=1)
    if meth_seq != None: res_df['meth'] = coordinate_df.apply(lambda row: meth_seq[row['chr'].lower()][int(row['position']) - int(window_size//2): int(row['position']) + int(window_size//2)][:,0], axis=1)
    res_df['label'] = label
    return res_df

def test(model, test_loader):
    model.eval()
    test_correct = 0
    with torch.no_grad():
        for batch_idx, (target, data) in enumerate(test_loader):
            data, target = data.cuda(), target.cuda()
            outputs = model(data)
            predicted_labels = outputs.argmax(dim=1)
            test_correct += (predicted_labels == target.argmax(dim=1)).sum().item()
    test_accuracy = test_correct / total_samples_test
    return test_accuracy

cnfg_dic = {'homo': configs.hg19, 'mouse': configs.mouse, 'mouse_si': configs.mouse_si}
assembly = data_reader.readfasta(cnfg_dic[species]['assembly'])
#assembly = data_reader.readfasta('/home/csgrads/ssere004/Organisms/homo_sapiens/hg19/hg19.fa')
dmrs, not_dmrs = dmngr.load_df(KO)

if include_methylation != 'none':
    meth_seq_lst = []
    if 'wt' in include_methylation:
        methylations = pd.read_csv(cnfg_dic[species]['WT_methylation'], header=None, sep='\t', names=['chr', 'position', 'end', 'meth', 'coverage']) #10sec
        meth_seq_lst.append(preprocess.make_methseq_dic(cnfg_dic[species]['og'], methylations, assembly, configs.rc['ct']))
        print('methylation loaded for wild type')
    if 'ko' in include_methylation:
        methylations = pd.read_csv(cnfg_dic[species]['KO_methylations'][KO], header=None, sep='\t', names=['chr', 'position', 'end', 'meth', 'coverage']) #10sec
        meth_seq_lst.append(preprocess.make_methseq_dic(cnfg_dic[species]['og'], methylations, assembly, configs.rc['ct']))
        print('methylation loaded for the knock out {}'.format(KO))
else:
    meth_seq_lst = None
    print('no methylation included')

#meth_seq = data_reader.load_dic('meth_seq_dic.pkl')
# meth_seq['chrX'] = meth_seq.pop('chrx')
# meth_seq['chrY'] = meth_seq.pop('chry')
train_sample_size = int(min(d_size * 0.9, len(dmrs) * 2, len(not_dmrs) * 2))
train_dataset = pd.concat([dmngr.make_input_dataset(assembly, dmrs.sample(int(train_sample_size // 2)), 1, window_size=window_size, meth_seq_lst=meth_seq_lst, mc_num=20),
                     dmngr.make_input_dataset(assembly, not_dmrs.sample(int(train_sample_size // 2)), 0, window_size=window_size, meth_seq_lst=meth_seq_lst, mc_num=20)])
#dataset_ = pd.read_csv('./dump_files/sample_dataset.csv', header=None, index_col=None, names=['seq', 'meth', 'label'])
train_dataset = train_dataset.sample(frac=1).reset_index(drop=True)
train_dataset = train_dataset[train_dataset['seq'].str.len() == window_size]

train_include_dic = dmngr.make_train_included_dic(assembly, train_dataset, window_size)
test_dmrs, test_not_dmrs = dmngr.make_test_coordinate_df(train_include_dic, dmrs), dmngr.make_test_coordinate_df(train_include_dic, not_dmrs)
test_sample_size = int(min(d_size * 0.1, len(test_dmrs) * 2, len(test_not_dmrs) * 2))

test_dataset = pd.concat([dmngr.make_input_dataset(assembly, test_dmrs.sample(int(test_sample_size // 2)), 1, window_size=window_size, meth_seq_lst=meth_seq_lst, mc_num=20),
                     dmngr.make_input_dataset(assembly, test_not_dmrs.sample(int(test_sample_size // 2)), 0, window_size=window_size, meth_seq_lst=meth_seq_lst, mc_num=20)])
test_dataset = test_dataset.sample(frac=1).reset_index(drop=True)
test_dataset = test_dataset[test_dataset['seq'].str.len() == window_size]

#dataset = pd.read_csv('sample_dataset.csv')
batch_size = 32
train_ratio = 0.9
train_loader = DataLoader(DNADataset(train_dataset), batch_size=batch_size, shuffle=True)
test_loader = DataLoader(DNADataset(test_dataset), batch_size=batch_size, shuffle=True)

model = ConvNet(next(iter(train_loader))[1])
optimizer = optim.Adam(model.parameters(), lr=1e-5)
loss_fn = nn.BCELoss()

num_epochs = 10
total_samples_train = len(train_dataset)
total_samples_test = len(test_dataset)

# for batch_idx, (target, data) in enumerate(train_loader):
#     break

with torch.cuda.device('cuda:0'):
    model.cuda()
    loss_fn.cuda()
    for epoch in range(num_epochs):
        model.train()
        total_loss, total_correct, total_processed = 0, 0, 0
        progress_bar = tqdm(train_loader, desc=f"Epoch {epoch+1}/{num_epochs}", leave=False)
        for batch_idx, (target, data) in enumerate(train_loader):
            data, target = data.cuda(), target.cuda()
            optimizer.zero_grad()
            output = model(data)
            loss = loss_fn(output, target)
            loss.backward()
            optimizer.step()
            predictions = output.argmax(dim=1)
            total_correct += (predictions == target.argmax(dim=1)).sum().item()
            total_processed += batch_size
            if total_processed%(10*batch_size) == 0:
                b_acc = total_correct / total_processed
                progress_percentage = (total_processed / total_samples_train) * 100
                progress_bar.set_postfix(loss=loss.item(), batch_acc=b_acc, progress=f"{progress_percentage:.2f}%")
            total_loss += loss.item()
        print('Epoch: {} Loss: {:.4f}'.format(epoch, loss.item()))
        acc = test(model, test_loader)
        print('Test accuracy: {}'.format(acc))

with open("CO_results.txt", "a") as file_object:
    file_object.write('%s\t%s\t%s\t%s\t%s' %(str(window_size), str(len(train_dataset)), str(test(model, test_loader)), str(KO), str(include_methylation)))
    file_object.write("\n")
