import sys
import os
project_root = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(project_root)
from torch.utils.data import DataLoader
import numpy as np
import torch
import pandas as pd
import es_prediction.motif_util as mu
from transformers import BertTokenizer
from utils.data_manager import DNADataset
import utils.data_manager as dmngr
from utils.model_manager import DNASequenceClassifier
import preprocess.data_reader as data_reader
import configs
import utils.model_manager as mdlmngr
import argparse


def get_avg_att_scores(attention_scores, kmer):
    scores = np.zeros([attention_scores.shape[0], attention_scores.shape[-1]])
    for index, attention_score in enumerate(attention_scores):
        attn_score = []
        for i in range(1, attention_score.shape[-1]-kmer+2):
            attn_score.append(float(attention_score[:,0,i].sum()))
        for i in range(len(attn_score)-1):
            if attn_score[i+1] == 0:
                attn_score[i] = 0
                break
        # attn_score[0] = 0
        counts = np.zeros([len(attn_score)+kmer-1])
        real_scores = np.zeros([len(attn_score)+kmer-1])
        for i, score in enumerate(attn_score):
            for j in range(kmer):
                counts[i+j] += 1.0
                real_scores[i + j] += score
        real_scores = real_scores / counts
        real_scores = real_scores / np.linalg.norm(real_scores)
        scores[index] = real_scores
    return scores


def model_eval(X, Y, model, tokenizer, window_size, kmer):
    batch_size = 2
    labels = torch.tensor(Y, dtype=torch.float32)
    dataset = DNADataset(X, labels, tokenizer, window_size, kmer)
    data_loader = DataLoader(dataset, batch_size=batch_size, shuffle=True)
    model.to('cuda')
    model.eval()
    sequences = np.empty(len(dataset), dtype=object)
    labels = np.zeros(len(dataset))
    scores = np.zeros((len(dataset), window_size))
    #attention_scores = np.zeros([len(dataset), 12, window_size, window_size])
    idx = 0
    with torch.no_grad():
        for batch in data_loader:
            input_ids = batch["input_ids"].to('cuda')
            attention_mask = batch["attention_mask"].to('cuda')
            o = model(input_ids=input_ids, attention_mask=attention_mask, output_attentions=True)
            sequences[idx:idx+len(input_ids)] = batch["sequence"]
            labels[idx:idx+len(input_ids)] = batch["label"].argmax(dim=1).numpy()
            #attention_scores[idx:idx+len(input_ids)] = o[-1][-1].cpu().numpy()
            scores[idx:idx+len(input_ids)] = get_avg_att_scores(o[-1][-1].cpu().numpy(), kmer)
            idx += len(input_ids)
    return sequences, labels.astype(int), scores



kmer = 6
KO = 'DNMT3'
window_size = 512
species = 'homo'


parser = argparse.ArgumentParser()
parser.add_argument('-kmer', '--kmer', help='kmer', required=True)
parser.add_argument('-ws', '--window_size', help='window_size', required=True)
parser.add_argument('-KO', '--knockout', help='TET or DNMT3, QKO, PKO, mouse_3aKO, mouse_3bKO, mouse_SI_TET23_KO', required=True)
parser.add_argument('-sp', '--species', help='species: homo, mouse, mouse_si', required=False, default='BERT')
args = parser.parse_args()


kmer = int(args.kmer)
KO = args.knockout
window_size = int(args.window_size)
species = args.species

cnfg_dic = {'homo': configs.hg19, 'mouse': configs.mouse, 'mouse_si': configs.mouse_si}
cnfg = cnfg_dic[species]


model_add = "./dump_files/pretrained_models/" + "kmer_"+str(kmer) + "_ws_" + str(window_size) + "_KO_" + str(KO) + "_model_" + 'BERT'+ '_methyl' + str('none')
model_name = "zhihan1996/DNA_bert_"+str(kmer)
tokenizer = BertTokenizer.from_pretrained(model_name, trust_remote_code=True)
clf_model = mdlmngr.load_clf_model(model_add)
#clf_model = mdlmngr.load_clf_model("./dump_files/pretrained_models_/" + "kmer_"+str(kmer) + "_ws_" + str(window_size) + "_KO_" + str(KO))
print('model loaded from '+ model_add)

dmrs, not_dmrs = dmngr.load_df(KO)
assembly = data_reader.readfasta(cnfg['assembly'])
sample_size = min(50000, len(dmrs)*2, len(not_dmrs)*2)
dataset = pd.concat([dmngr.make_input_dataset(assembly, dmrs.sample(int(sample_size//2)), 1, window_size=window_size),
                     dmngr.make_input_dataset(assembly, not_dmrs.sample(int(sample_size//2)), 0, window_size=window_size)])
dataset = dataset.sample(frac=1).reset_index(drop=True)
X = dataset.drop(columns=['label'])
Y = np.asarray(pd.cut(dataset['label'], bins=2, labels=[0, 1], right=False))
b = np.zeros((Y.size, Y.max()+1))
b[np.arange(Y.size), Y] = 1
Y = b
X__ = X
Y__ = Y


sequences, labels, scores = model_eval(X, Y, clf_model.bert, tokenizer, window_size, kmer)

np.save('./dump_files/motif_finding_feed/{}_{}_sequences.npy'.format(species, KO), sequences)
np.save('./dump_files/motif_finding_feed/{}_{}_labels.npy'.format(species, KO), labels)
np.save('./dump_files/motif_finding_feed/{}_{}_scores.npy'.format(species, KO), scores)
