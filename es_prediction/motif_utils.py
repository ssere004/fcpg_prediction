import sys
import os
project_root = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(project_root)
from torch.utils.data import DataLoader
import numpy as np
import torch
import pandas as pd
import es_prediction.motif_util as mu
from transformers import BertTokenizer
from utils.data_manager import DNADataset
import utils.data_manager as dmngr
from utils.model_manager import DNASequenceClassifier
import preprocess.data_reader as data_reader
import configs
import utils.model_manager as mdlmngr
import argparse



def model_eval(X, Y, model, tokenizer, window_size, kmer):
    batch_size = 2
    labels = torch.tensor(Y, dtype=torch.float32)
    dataset = DNADataset(X, labels, tokenizer, window_size, kmer)
    data_loader = DataLoader(dataset, batch_size=batch_size, shuffle=True)
    model.to('cuda')
    model.eval()
    sequences = np.empty(len(dataset), dtype=object)
    labels = np.zeros(len(dataset))
    scores = np.zeros((len(dataset), window_size))
    #attention_scores = np.zeros([len(dataset), 12, window_size, window_size])
    idx = 0
    with torch.no_grad():
        for batch in data_loader:
            input_ids = batch["input_ids"].to('cuda')
            attention_mask = batch["attention_mask"].to('cuda')
            o = model(input_ids=input_ids, attention_mask=attention_mask, output_attentions=True)
            sequences[idx:idx+len(input_ids)] = batch["sequence"]
            labels[idx:idx+len(input_ids)] = batch["label"].argmax(dim=1).numpy()
            #attention_scores[idx:idx+len(input_ids)] = o[-1][-1].cpu().numpy()
            scores[idx:idx+len(input_ids)] = get_avg_att_scores(o[-1][-1].cpu().numpy(), kmer)
            idx += len(input_ids)
    return sequences, labels.astype(int), scores

def get_avg_att_scores(attention_scores, kmer):
    scores = np.zeros([attention_scores.shape[0], attention_scores.shape[-1]])
    for index, attention_score in enumerate(attention_scores):
        attn_score = []
        for i in range(1, attention_score.shape[-1]-kmer+2):
            attn_score.append(float(attention_score[:, 0, i].sum()))
        for i in range(len(attn_score)-1):
            if attn_score[i+1] == 0:
                attn_score[i] = 0
                break
        # attn_score[0] = 0
        counts = np.zeros([len(attn_score)+kmer-1])
        real_scores = np.zeros([len(attn_score)+kmer-1])
        for i, score in enumerate(attn_score):
            for j in range(kmer):
                counts[i+j] += 1.0
                real_scores[i + j] += score
        real_scores = real_scores / counts
        real_scores = real_scores / np.linalg.norm(real_scores)
        # print(index)
        # print(real_scores)
        # print(len(real_scores))
        scores[index] = real_scores
    return scores


kmer = 6
KO = 'DNMT3'
window_size = 512
species = 'homo'
m_ws = 10
m_ml = 8

parser = argparse.ArgumentParser()
parser.add_argument('-kmer', '--kmer', help='kmer', required=True)
parser.add_argument('-ws', '--window_size', help='window_size', required=True)
parser.add_argument('-KO', '--knockout', help='TET or DNMT3, QKO, PKO, mouse_3aKO, mouse_3bKO, mouse_SI_TET23_KO', required=True)
parser.add_argument('-sp', '--species', help='species: homo, mouse, mouse_si', required=False, default='BERT')
parser.add_argument('-m_ws', '--motif_ws', help='Motif window size', required=False, default=10)
parser.add_argument('-m_ml', '--motif_min_len', help='Motif min len', required=False, default=8)


args = parser.parse_args()


kmer = int(args.kmer)
KO = args.knockout
window_size = int(args.window_size)
species = args.species
m_ws = args.motif_ws
m_ml = args.motif_min_len
#
# cnfg_dic = {'homo': configs.hg19, 'mouse': configs.mouse, 'mouse_si': configs.mouse_si}
# cnfg = cnfg_dic[species]
#
#
# model_add = "./dump_files/pretrained_models/" + "kmer_"+str(kmer) + "_ws_" + str(window_size) + "_KO_" + str(KO) + "_model_" + 'BERT'+ '_methyl' + str('none')
# model_name = "zhihan1996/DNA_bert_"+str(kmer)
# tokenizer = BertTokenizer.from_pretrained(model_name, trust_remote_code=True)
# clf_model = mdlmngr.load_clf_model(model_add)
# #clf_model = mdlmngr.load_clf_model("./dump_files/pretrained_models_/" + "kmer_"+str(kmer) + "_ws_" + str(window_size) + "_KO_" + str(KO))
# print('model loaded from '+ model_add)
#
# dmrs, not_dmrs = dmngr.load_df(KO)
# assembly = data_reader.readfasta(cnfg['assembly'])
# sample_size = min(10000, len(dmrs)*2, len(not_dmrs)*2)
# dataset = pd.concat([dmngr.make_input_dataset(assembly, dmrs.sample(int(sample_size//2)), 1, window_size=window_size),
#                      dmngr.make_input_dataset(assembly, not_dmrs.sample(int(sample_size//2)), 0, window_size=window_size)])
# dataset = dataset.sample(frac=1).reset_index(drop=True)
# X = dataset.drop(columns=['label'])
# Y = np.asarray(pd.cut(dataset['label'], bins=2, labels=[0, 1], right=False))
# b = np.zeros((Y.size, Y.max()+1))
# b[np.arange(Y.size), Y] = 1
# Y = b
# X__ = X
# Y__ = Y
#
#
# sequences, labels, scores = model_eval(X, Y, clf_model.bert, tokenizer, window_size, kmer)
#
# np.save('./dump_files/motif_finding_feed/{}_{}_sequences.npy'.format(species, KO), sequences)
# np.save('./dump_files/motif_finding_feed/{}_{}_labels.npy'.format(species, KO), labels)
# np.save('./dump_files/motif_finding_feed/{}_{}_scores.npy'.format(species, KO), scores)


sequences, labels, scores = np.load('./dump_files/motif_finding_feed/{}_{}_sequences.npy'.format(species, KO), allow_pickle=True), \
                            np.load('./dump_files/motif_finding_feed/{}_{}_labels.npy'.format(species, KO), allow_pickle=True), \
                            np.load('./dump_files/motif_finding_feed/{}_{}_scores.npy'.format(species, KO), allow_pickle=True)

#scores = np.concatenate(scores_lst, axis=0)
dev = pd.DataFrame()
dev['seq'] = sequences
dev['label'] = labels
dev_pos = dev[dev['label'] == 1]
dev_neg = dev[dev['label'] == 0]
pos_atten_scores = scores[dev_pos.index.values]
neg_atten_scores = scores[dev_neg.index.values]


# merged_motif_seqs = mu.motif_analysis(dev_pos['seq'],
#                                         dev_neg['seq'],
#                                         pos_atten_scores,
#                                         window_size = int(m_ws),
#                                         min_len = int(m_ml),
#                                         pval_cutoff = 0.01,
#                                         min_n_motif = 50,
#                                         save_file_dir='./dump_files/motifs/ws_'+str(m_ws)+'/'+species+'-'+KO,
#                                         verbose = True,
#                                         return_idx = False
#                                     )

motif_seqs = mu.find_high_att_regions(neg_atten_scores, list(dev_pos['seq']), min_len=6, kwargs={})
import preprocess.data_reader as data_reader
print('high attention regions found for {} sequences'.format(len(motif_seqs)))
data_reader.write_fasta(motif_seqs.keys(), './dump_files/motifs/high_atten_regs/'+species+'-'+KO+'_neg.fasta')


motif_seqs = mu.find_high_att_regions(pos_atten_scores, list(dev_pos['seq']), min_len=6, kwargs={})
import preprocess.data_reader as data_reader
print('high attention regions found for {} sequences'.format(len(motif_seqs)))
data_reader.write_fasta(motif_seqs.keys(), './dump_files/motifs/high_atten_regs/'+species+'-'+KO+'_pos.fasta')


# window_size = 10
# min_len = 6
# pval_cutoff = 0.01
# min_n_motif = 10
# save_file_dir = './dump_files/motifs/'+species+' '+KO
# verbose = True
# return_idx = False
# kwargs = {}
# align_all_ties=True
# pos_seqs = dev_pos['seq']
# neg_seqs = dev_neg['seq']



#
#
# motifs = {'DNMT3': 'motif_CAGGAG_820_weblogo.png',
#           'TET': 'motif_CAGGGG_581_weblogo.png',
#           'QKO': 'motif_GCGCTG_144_weblogo.png',
#           'PKO': 'motif_GCGGGG_98_weblogo.png',
#           'mouse_3a': 'motif_CAGGGT_16_weblogo.png',
#           'mouse_3b': 'motif_GGTGGG_23_weblogo.png',
#           'mouse_si_TET23': 'motif_CTCCCT_790_weblogo.png'
#           }
#
#
# p = dr.load_dic('./motif_founded/motifs/homo DNMT3/pwms_dic.pkl')
# d = p['CAGGAG']
# for bp in d.keys():
#     print(bp, d[bp])
#
#
# 'DNMT3': 'MA1976.1'
#
# A 0.19390243902439025, 0.13658536585365855, 0.025609756097560974, 0.8292682926829268, 0.0012195121951219512, 0.024390243902439025, 0.7914634146341464, 0.1378048780487805, 0.11341463414634147, 0.18902439024390244
# C 0.37317073170731707, 0.40853658536585363, 0.9121951219512195, 0.012195121951219513, 0.0, 0.042682926829268296, 0.003658536585365854, 0.0475609756097561, 0.17804878048780487, 0.3207317073170732
# G 0.22195121951219512, 0.32195121951219513, 0.06219512195121951, 0.00853658536585366, 0.998780487804878, 0.9329268292682927, 0.1926829268292683, 0.8073170731707318, 0.6390243902439025, 0.275609756097561
# T 0.21097560975609755, 0.1329268292682927, 0.0, 0.15, 0.0, 0.0, 0.012195121951219513, 0.007317073170731708, 0.06951219512195123, 0.2146341463414634
#
# 'TET': 'MA1723.1'
#
# A 0.2719449225473322, 0.13253012048192772, 0.16351118760757316, 0.9845094664371773, 0.03614457831325301, 0.04991394148020654, 0.0, 0.13253012048192772, 0.1962134251290878, 0.07917383820998279
# C 0.1686746987951807, 0.2289156626506024, 0.7263339070567987, 0.0, 0.0, 0.04130808950086059, 0.09122203098106713, 0.07401032702237521, 0.3098106712564544, 0.37521514629948366
# G 0.4423407917383821, 0.5542168674698795, 0.10154905335628227, 0.01549053356282272, 0.963855421686747, 0.9087779690189329, 0.891566265060241, 0.7796901893287436, 0.3098106712564544, 0.37005163511187605
# T 0.11703958691910499, 0.08433734939759036, 0.008605851979345954, 0.0, 0.0, 0.0, 0.01721170395869191, 0.013769363166953529, 0.18416523235800344, 0.17555938037865748
#
# 'QKO': 'MA1973.1'
#
# A 0.1597222222222222, 0.2013888888888889, 0.0763888888888889, 0.0, 0.0, 0.0, 0.09722222222222222, 0.0, 0.1527777777777778, 0.11805555555555555
# C 0.2916666666666667, 0.2013888888888889, 0.1111111111111111, 1.0, 0.013888888888888888, 0.9722222222222222, 0.1527777777777778, 0.09027777777777778, 0.2361111111111111, 0.4305555555555556
# G 0.4027777777777778, 0.4513888888888889, 0.7986111111111112, 0.0, 0.9861111111111112, 0.027777777777777776, 0.20833333333333334, 0.8819444444444444, 0.4861111111111111, 0.3611111111111111
# T 0.14583333333333334, 0.14583333333333334, 0.013888888888888888, 0.0, 0.0, 0.0, 0.5416666666666666, 0.027777777777777776, 0.125, 0.09027777777777778
#
# 'PKO': 'MA1929.1'
#
# A 0.20408163265306123, 0.24489795918367346, 0.0, 0.01020408163265306, 0.05102040816326531, 0.0, 0.0, 0.1326530612244898, 0.15306122448979592, 0.16326530612244897
# C 0.23469387755102042, 0.25510204081632654, 0.05102040816326531, 0.9897959183673469, 0.0, 0.09183673469387756, 0.24489795918367346, 0.030612244897959183, 0.3469387755102041, 0.29591836734693877
# G 0.3979591836734694, 0.2755102040816326, 0.9285714285714286, 0.0, 0.9489795918367347, 0.9081632653061225, 0.7551020408163265, 0.8367346938775511, 0.2857142857142857, 0.3163265306122449
# T 0.16326530612244897, 0.22448979591836735, 0.02040816326530612, 0.0, 0.0, 0.0, 0.0, 0.0, 0.21428571428571427, 0.22448979591836735
#
#
# 'mouse_3a': 'MA1987.1'
#
# A 0.125, 0.0625, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0625, 0.1875
# C 0.125, 0.1875, 1.0, 0.0, 0.0, 0.0, 0.0, 0.1875, 0.125, 0.125
# G 0.5, 0.25, 0.0, 0.0, 1.0, 1.0, 1.0, 0.0, 0.5625, 0.5
# T 0.25, 0.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.8125, 0.25, 0.1875
#
# 'mouse_3b': 'MA0073.1'
# A 0.17391304347826086, 0.2608695652173913, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.13043478260869565, 0.13043478260869565
# C 0.13043478260869565, 0.043478260869565216, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.21739130434782608, 0.2608695652173913
# G 0.391304347826087, 0.34782608695652173, 0.9565217391304348, 1.0, 0.43478260869565216, 1.0, 1.0, 1.0, 0.5217391304347826, 0.34782608695652173
# T 0.30434782608695654, 0.34782608695652173, 0.043478260869565216, 0.0, 0.5652173913043478, 0.0, 0.0, 0.0, 0.13043478260869565, 0.2608695652173913
#
#
# 'mouse_si tet23': 'MA1976.1'
#
# A 0.2468354430379747, 0.4, 0.0, 0.19367088607594937, 0.0, 0.0, 0.0, 0.056962025316455694, 0.10253164556962026, 0.18860759493670887
# C 0.3278481012658228, 0.330379746835443, 0.9924050632911392, 0.0, 0.4620253164556962, 1.0, 1.0, 0.020253164556962026, 0.3379746835443038, 0.31772151898734174
# G 0.2, 0.1911392405063291, 0.0037974683544303796, 0.0, 0.049367088607594936, 0.0, 0.0, 0.0037974683544303796, 0.3468354430379747, 0.21012658227848102
# T 0.22531645569620254, 0.07848101265822785, 0.0037974683544303796, 0.8063291139240506, 0.48860759493670886, 0.0, 0.0, 0.9189873417721519, 0.21265822784810126, 0.28354430379746837



# Set environment variables
export BWA=bwa
export THREADS=40
export SAM=samtools
export DATASET=cowpea
export OUTPUT_DIR=/home/ssere004/projects/cowpea_variant_calling
export READS=/home/stelo/cowpea/cyprus/reads

# Create a symbolic link to the reference genome in the writable directory
ln -s /data/home/stelo/cowpea/cyprus/GCF_004118075.2_ASM411807v2_genomic.fna $OUTPUT_DIR/GCF_004118075.2_ASM411807v2_genomic.fna
export REF=$OUTPUT_DIR/GCF_004118075.2_ASM411807v2_genomic

# Set read file locations
export A1=$READS/2010/E100078229_L01_PLAchspR510675-664_1.fq.gz
export A2=$READS/2010/E100078229_L01_PLAchspR510675-664_2.fq.gz
export A=2010_to_$DATASET

export B1=$READS/2011/E100078229_L01_PLAchspR510676-404_1.fq.gz
export B2=$READS/2011/E100078229_L01_PLAchspR510676-404_2.fq.gz
export B=2011_to_$DATASET

export C1=$READS/2016/E100078229_L01_PLAchspR510677-741_1.fq.gz
export C2=$READS/2016/E100078229_L01_PLAchspR510677-741_2.fq.gz
export C=2016_to_$DATASET
export THREADS=40


gatk HaplotypeCaller -R $REF.fna -I $OUTPUT_DIR/$A.recal.bam -O $OUTPUT_DIR/$A.g.vcf -ERC GVCF
gatk HaplotypeCaller -R $REF.fna -I $OUTPUT_DIR/$B.recal.bam -O $OUTPUT_DIR/$B.g.vcf -ERC GVCF
gatk HaplotypeCaller -R $REF.fna -I $OUTPUT_DIR/$C.recal.bam -O $OUTPUT_DIR/$C.g.vcf -ERC GVCF



# Index the reference genome in the writable directory
cd $OUTPUT_DIR
# $BWA index $REF.fna
# $SAM faidx $REF.fna
# gatk CreateSequenceDictionary -R $REF.fna -O ${REF%.fna}.dict
#
# # Align reads to reference genome and convert to BAM
# $BWA mem -M -R '@RG\tID:BD\tSM:BD\tLB:BD\tPL:ILLUMINA' -t $THREADS $REF.fna $A1 $A2 | $SAM view -b -o $OUTPUT_DIR/$A.bam
# $SAM sort -m10G -@ 20 -T /tmp/aln.sorted -o $OUTPUT_DIR/$A.sorted.bam $OUTPUT_DIR/$A.bam
#
# $BWA mem -M -R '@RG\tID:BD\tSM:BD\tLB:BD\tPL:ILLUMINA' -t $THREADS $REF.fna $B1 $B2 | $SAM view -b -o $OUTPUT_DIR/$B.bam
# $SAM sort -m10G -@ 20 -T /tmp/aln.sorted -o $OUTPUT_DIR/$B.sorted.bam $OUTPUT_DIR/$B.bam
#
# $BWA mem -M -R '@RG\tID:BD\tSM:BD\tLB:BD\tPL:ILLUMINA' -t $THREADS $REF.fna $C1 $C2 | $SAM view -b -o $OUTPUT_DIR/$C.bam
# $SAM sort -m10G -@ 20 -T /tmp/aln.sorted -o $OUTPUT_DIR/$C.sorted.bam $OUTPUT_DIR/$C.bam

# (Commented Out) Post-Processing with Picard and GATK
java -jar /home/qliang/0.soft/GATK/picard.jar MarkDuplicates I=$OUTPUT_DIR/$A.sorted.bam O=$OUTPUT_DIR/$A.dedup.bam METRICS_FILE=$OUTPUT_DIR/$A.txt
java -jar /home/qliang/0.soft/GATK/picard.jar BuildBamIndex INPUT=$OUTPUT_DIR/$A.dedup.bam
java -jar /home/qliang/0.soft/GATK/picard.jar CreateSequenceDictionary R=$REF.fna O=$OUTPUT_DIR/$REF.dict



/home/qliang/0.soft/jdk1.8.0_221/bin/java -jar /home/qliang/0.soft/GATK/GenomeAnalysisTK.jar -T RealignerTargetCreator -R $REF.fna -I $OUTPUT_DIR/$A.dedup.bam -o $OUTPUT_DIR/realignment_targets.$A.list
/home/qliang/0.soft/jdk1.8.0_221/bin/java -jar /home/qliang/0.soft/GATK/GenomeAnalysisTK.jar -T IndelRealigner -R $REF.fna -I $OUTPUT_DIR/$A.dedup.bam -targetIntervals $OUTPUT_DIR/realignment_targets.$A.list -o $OUTPUT_DIR/$A.realigned.bam
/home/qliang/0.soft/jdk1.8.0_221/bin/java -jar /home/qliang/0.soft/GATK/GenomeAnalysisTK.jar -T HaplotypeCaller -nct 20 -R $REF.fna -I $OUTPUT_DIR/$A.realigned.bam -ERC GVCF -o $OUTPUT_DIR/$A.variant.g.vcf
/home/qliang/0.soft/GATK/gatk/gatk GenotypeGVCFs --allow-old-rms-mapping-quality-annotation-data -R $REF.fna  -V $OUTPUT_DIR/$A.variant.g.vcf -O $OUTPUT_DIR/combined_genotype.g.vcf
/home/qliang/0.soft/jdk1.8.0_221/bin/java -jar /home/qliang/0.soft/GATK/GenomeAnalysisTK.jar -T SelectVariants -R $REF.fna -V $OUTPUT_DIR/$A.variant.g.vcf -selectType SNP -o $OUTPUT_DIR/$A.snps.g.vcf
/home/qliang/0.soft/GATK/gatk/gatk VariantFiltration -V $OUTPUT_DIR/$A.variant.g.vcf -filter "QD < 2.0" --filter-name "QD2" -filter "QUAL < 30.0" --filter-name "QUAL30" -filter "SOR > 3.0" --filter-name "SOR3" -filter "FS > 60.0" --filter-name "FS60" -filter "MQRankSum < -12.5" --filter-name "MQRankSum-12.5" -filter "ReadPosRankSum < -8.0" --filter-name "ReadPosRankSum-8" -O $OUTPUT_DIR/$A.filtered.g.vcf



gatk MarkDuplicatesSpark -I $OUTPUT_DIR/$A.sorted.bam -O $OUTPUT_DIR/$A.dedup.bam -M $OUTPUT_DIR/$A.metrics --tmp-dir $OUTPUT_DIR/tmp --spark-master local[8]
gatk MarkDuplicatesSpark -I $OUTPUT_DIR/$B.sorted.bam -O $OUTPUT_DIR/$B.dedup.bam -M $OUTPUT_DIR/$B.metrics --tmp-dir $OUTPUT_DIR/tmp --spark-master local[8]
gatk MarkDuplicatesSpark -I $OUTPUT_DIR/$C.sorted.bam -O $OUTPUT_DIR/$C.dedup.bam -M $OUTPUT_DIR/$C.metrics --tmp-dir $OUTPUT_DIR/tmp --spark-master local[8]





# Base quality score recalibration (BQSR)
gatk BaseRecalibrator -I $OUTPUT_DIR/$A.dedup.bam -R $REF.fna --known-sites known_sites.vcf -O $OUTPUT_DIR/$A.recal_data.table
gatk ApplyBQSR -R $REF.fna -I $OUTPUT_DIR/$A.dedup.bam --bqsr-recal-file $OUTPUT_DIR/$A.recal_data.table -O $OUTPUT_DIR/$A.recal.bam

gatk BaseRecalibrator -I $OUTPUT_DIR/$B.dedup.bam -R $REF.fna --known-sites known_sites.vcf -O $OUTPUT_DIR/$B.recal_data.table
gatk ApplyBQSR -R $REF.fna -I $OUTPUT_DIR/$B.dedup.bam --bqsr-recal-file $OUTPUT_DIR/$B.recal_data.table -O $OUTPUT_DIR/$B.recal.bam

gatk BaseRecalibrator -I $OUTPUT_DIR/$C.dedup.bam -R $REF.fna --known-sites known_sites.vcf -O $OUTPUT_DIR/$C.recal_data.table
gatk ApplyBQSR -R $REF.fna -I $OUTPUT_DIR/$C.dedup.bam --bqsr-recal-file $OUTPUT_DIR/$C.recal_data.table -O $OUTPUT_DIR/$C.recal.bam

# Call variants using HaplotypeCaller
gatk HaplotypeCaller -R $REF.fna -I $OUTPUT_DIR/$A.recal.bam -O $OUTPUT_DIR/$A.g.vcf -ERC GVCF
gatk HaplotypeCaller -R $REF.fna -I $OUTPUT_DIR/$B.recal.bam -O $OUTPUT_DIR/$B.g.vcf -ERC GVCF
gatk HaplotypeCaller -R $REF.fna -I $OUTPUT_DIR/$C.recal.bam -O $OUTPUT_DIR/$C.g.vcf -ERC GVCF

# Combine GVCFs for joint genotyping
gatk CombineGVCFs -R $REF.fna -V $OUTPUT_DIR/$A.g.vcf -V $OUTPUT_DIR/$B.g.vcf -V $OUTPUT_DIR/$C.g.vcf -O $OUTPUT_DIR/combined.g.vcf

# Perform joint genotyping
gatk GenotypeGVCFs -R $REF.fna -V $OUTPUT_DIR/combined.g.vcf -O $OUTPUT_DIR/raw_variants.vcf

# Select SNPs and INDELs
gatk SelectVariants -R $REF.fna -V $OUTPUT_DIR/raw_variants.vcf -select-type SNP -O $OUTPUT_DIR/raw_snps.vcf
gatk SelectVariants -R $REF.fna -V $OUTPUT_DIR/raw_variants.vcf -select-type INDEL -O $OUTPUT_DIR/raw_indels.vcf

# Apply hard filters to SNPs and INDELs
gatk VariantFiltration -R $REF.fna -V $OUTPUT_DIR/raw_snps.vcf --filter-expression "QD < 2.0" --filter-name "QD2" --filter-expression "QUAL < 30.0" --filter-name "QUAL30" --filter-expression "SOR > 3.0" --filter-name "SOR3" --filter-expression "FS > 60.0" --filter-name "FS60" --filter-expression "MQRankSum < -12.5" --filter-name "MQRankSum12.5" --filter-expression "ReadPosRankSum < -8.0" --filter-name "ReadPosRankSum8" -O $OUTPUT_DIR/filtered_snps.vcf

gatk VariantFiltration -R $REF.fna -V $OUTPUT_DIR/raw_indels.vcf --filter-expression "QD < 2.0" --filter-name "QD2" --filter-expression "QUAL < 30.0" --filter-name "QUAL30" --filter-expression "FS > 200.0" --filter-name "FS200" --filter-expression "ReadPosRankSum < -20.0" --filter-name "ReadPosRankSum20" -O $OUTPUT_DIR/filtered_indels.vcf



