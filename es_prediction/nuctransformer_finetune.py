import sys
import os

project_root = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(project_root)


# import configs
import preprocess.data_reader as data_reader
# import preprocess.preprocess as preprocess
# import process.process as process
import torch.optim as optim
from tqdm import tqdm
import pandas as pd
import torch
from torch.utils.data import DataLoader
import torch.nn as nn
from sklearn.model_selection import train_test_split
import numpy as np
import argparse
from transformers import BertTokenizer, BertModel
import configs
from utils.model_manager import DNASequenceClassifier
from utils.data_manager import DNADataset
from utils import data_manager as dmngr
from transformers import AutoModel, AutoTokenizer, AutoModelForMaskedLM

kmer = 6
KO = 'TET'
window_size = 512
model_type = 'nucleotide-transformer'
batch_size = 8

# parser = argparse.ArgumentParser()
# parser.add_argument('-kmer', '--kmer', help='kmer', required=True)
# parser.add_argument('-ws', '--window_size', help='window_size', required=True)
# parser.add_argument('-KO', '--knockout', help='TET, DNMT3, QKO, PKO, mouse_3aKO, mouse_3bKO, mouse_SI_TET23_KO', required=True)
# parser.add_argument('-m', '--model', help='model, BERT, BigBird, nucleotide-transformer', required=False, default='BERT')
# parser.add_argument('-bs', '--batch_size', help='batch size', required=False, default=32)
# args = parser.parse_args()
#
# kmer = int(args.kmer)
# KO = args.knockout
# window_size = int(args.window_size)
# model_type = args.model
# batch_size = int(args.batch_size)


num_classes = 2  # Binary classification

if model_type == 'BigBird':
    from transformers import BigBirdModel, BigBirdTokenizer
    model_name = "google/bigbird-roberta-base"
    model_name_alias = 'bigbird'
    tokenizer = BigBirdTokenizer.from_pretrained(model_name)
    model = BigBirdModel.from_pretrained(model_name, num_labels=2)
    clf_model = DNASequenceClassifier(model, None, num_classes)
elif model_type == 'BERT2':
    from transformers import BertConfig, AutoModel, AutoTokenizer
    model_name = 'zhihan1996/DNABERT-2-117M'
    model_name_alias = 'brt2'
    tokenizer = AutoTokenizer.from_pretrained("zhihan1996/DNABERT-2-117M", trust_remote_code=True)
    config = BertConfig.from_pretrained("zhihan1996/DNABERT-2-117M")
    model = AutoModel.from_config(config)
elif model_type == 'nucleotide-transformer':
    model_name = "InstaDeepAI/nucleotide-transformer-500m-human-ref"
    model_name_alias = 'nt'
    tokenizer = AutoTokenizer.from_pretrained(model_name)
    model = AutoModel.from_pretrained(model_name)
    clf_model = DNASequenceClassifier(model, None, num_classes)
elif model_type == 'BERT':
    model_name = "zhihan1996/DNA_bert_"+str(kmer)
    model_name_alias = 'brt'
    model = BertModel.from_pretrained(model_name, num_labels=2, finetuning_task="dnaprom", cache_dir=None)
    tokenizer = BertTokenizer.from_pretrained(model_name, trust_remote_code=True)
    clf_model = DNASequenceClassifier(model, None, num_classes)


cnfg = configs.hg19
assembly = data_reader.readfasta(cnfg['assembly'])
#assembly = data_reader.readfasta('/home/csgrads/ssere004/Organisms/homo_sapiens/hg19/hg19.fa')
dmrs, not_dmrs = dmngr.load_df(KO)

print('The data loaded for {}. dmrs size {}, not_dmrs size {}'.format(KO, str(len(dmrs)), str(len(not_dmrs))))

sample_size = min(100000, len(dmrs)*2, len(not_dmrs)*2)
dataset = pd.concat([dmngr.make_input_dataset(assembly, dmrs.sample(int(sample_size//2)), 1, window_size=window_size),
                     dmngr.make_input_dataset(assembly, not_dmrs.sample(int(sample_size//2)), 0, window_size=window_size)])
dataset = dataset.sample(frac=1).reset_index(drop=True)

X = dataset['seq']
Y = np.asarray(pd.cut(dataset['label'], bins=2, labels=[0, 1], right=False))
b = np.zeros((Y.size, Y.max()+1))
b[np.arange(Y.size), Y] = 1
Y = b

x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=0.1, random_state=None)

# Set batch size and max sequence length
labels = torch.tensor(y_train, dtype=torch.float32)
dataset = DNADataset(x_train.tolist(), labels, tokenizer, window_size, kmer)
data_loader_train = DataLoader(dataset, batch_size=batch_size, shuffle=True)

labels = torch.tensor(y_test, dtype=torch.float32)
dataset = DNADataset(x_test.tolist(), labels, tokenizer, window_size, kmer)
data_loader_test = DataLoader(dataset, batch_size=batch_size, shuffle=True)

optimizer = optim.Adam(clf_model.parameters(), lr=1e-5)
criterion = nn.BCELoss()
clf_model.to('cuda')
# Set the number of epochs
num_epochs = 5
total_samples = len(data_loader_train.dataset)  # Total number of samples
batch_size = data_loader_train.batch_size  # Batch size
batch_size_test = data_loader_test.batch_size
total_samples_test = len(data_loader_test.dataset)
# Training loop
for epoch in range(num_epochs):
    clf_model.train()
    total_loss = 0
    total_correct = 0
    total_processed = 0  # To keep track of total processed samples
    progress_bar = tqdm(data_loader_train, desc=f"Epoch {epoch+1}/{num_epochs}", leave=False)
    for batch in progress_bar:
        input_ids = batch["input_ids"].to('cuda')
        attention_mask = batch["attention_mask"].to('cuda')
        target_labels = batch["label"].to('cuda')
        optimizer.zero_grad()
        logits, _ = clf_model(input_ids, attention_mask)
        loss = criterion(logits, target_labels)
        loss.backward()
        optimizer.step()
        predictions = logits.argmax(dim=1)
        total_correct += (predictions == target_labels.argmax(dim=1)).sum().item()
        total_processed += batch_size
        if total_processed % (10*batch_size) == 0:
            b_acc = total_correct / total_processed
            progress_percentage = (total_processed / total_samples) * 100
            progress_bar.set_postfix(loss=loss.item(), batch_acc=b_acc, progress=f"{progress_percentage:.2f}%")
        total_loss += loss.item()
    epoch_accuracy = total_correct / total_samples
    average_loss = total_loss / len(data_loader_train)
    print(f"Epoch [{epoch+1}/{num_epochs}] - Average Loss: {average_loss:.4f}, Epoch Accuracy: {epoch_accuracy:.4f}")
    # Evaluate the model on test data
clf_model.eval()
test_correct = 0
#keep track of the predicted and labels to draw the ROC curve
predicted_test, labels_test = np.zeros((total_samples_test, 2)) - 1, np.zeros((total_samples_test, 2)) - 1
idx = 0
with torch.no_grad():
    for batch in data_loader_test:
        input_ids = batch["input_ids"].to('cuda')
        attention_mask = batch["attention_mask"].to('cuda')
        target_labels = batch["label"].to('cuda')
        logits, _ = clf_model(input_ids, attention_mask)
        predicted_labels = logits.argmax(dim=1)
        test_correct += (predicted_labels == target_labels.argmax(dim=1)).sum().item()
        predicted_test[idx: idx + len(logits)] = logits.cpu().numpy()
        labels_test[idx: idx + len(target_labels)] = target_labels.cpu().numpy()
        idx += len(logits)

assert -1 not in predicted_test and -1 not in labels_test
if not os.path.exists("./dump_files/pred_results/"): os.makedirs("./dump_files/pred_results/")

np.save("./dump_files/pred_results/predicted_{}_{}_{}_{}_{}.npy".format(str(cnfg['og']), str(kmer), str(window_size), str(KO), model_name_alias), predicted_test)
np.save("./dump_files/pred_results/labels_{}_{}_{}_{}_{}.npy".format(str(cnfg['og']), str(kmer), str(window_size), str(KO), model_name_alias), labels_test)

test_accuracy = test_correct / total_samples_test
with open("kmer_test_BERT_classification_results.txt", "a") as file_object:
    file_object.write('%s\t%s\t%s\t%s' %(str(kmer), str(window_size), str(KO), str(test_accuracy)))
    file_object.write("\n")

clf_model.save("./dump_files/pretrained_models/" + "kmer_"+str(kmer) + "_ws_" + str(window_size) + "_KO_" + str(KO))

