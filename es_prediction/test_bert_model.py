import sys
import os
project_root = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(project_root)
# import configs
import preprocess.data_reader as data_reader
# import preprocess.preprocess as preprocess
# import process.process as process
import pandas as pd
import torch
from torch.utils.data import DataLoader
import numpy as np
import argparse
from transformers import BertTokenizer, BertModel
import configs
from utils.model_manager import DNASequenceClassifier
from utils.data_manager import DNADataset
import utils.data_manager as dmngr

kmer = 6
KO = 'TET'
window_size = 512

parser = argparse.ArgumentParser()
parser.add_argument('-kmer', '--kmer', help='kmer', required=True)
parser.add_argument('-ws', '--window_size', help='window_size', required=True)
parser.add_argument('-KO', '--knockout', help='TET or DNMT3', required=True)
args = parser.parse_args()

kmer = int(args.kmer)
KO = args.knockout
window_size = int(args.window_size)


def load_clf_model(file_name):
    return DNASequenceClassifier(BertModel.from_pretrained(file_name+'_bert'), file_name+'_torchnn.pth', 2)

model_name = "zhihan1996/DNA_bert_"+str(kmer)
tokenizer = BertTokenizer.from_pretrained(model_name, trust_remote_code=True)
fn = "./dump_files/pretrained_models/" + "kmer_"+str(kmer) + "_ws_" + str(window_size) + "_KO_" + str(KO)
clf_model = load_clf_model(fn)


window_size = 512

dmrs, not_dmrs = dmngr.load_df('fCpG')

assembly = data_reader.readfasta(configs.hg19['assembly'])

sample_size = min(20000, len(dmrs)*2, len(not_dmrs)*2)
dataset = pd.concat([dmngr.make_input_dataset(assembly, dmrs.sample(int(sample_size//2)), 1, window_size=window_size),
                     dmngr.make_input_dataset(assembly, not_dmrs.sample(int(sample_size//2)), 0, window_size=window_size)])
dataset = dataset.sample(frac=1).reset_index(drop=True)

X = dataset['seq']
Y = np.asarray(pd.cut(dataset['label'], bins=2, labels=[0, 1], right=False))
b = np.zeros((Y.size, Y.max()+1))
b[np.arange(Y.size), Y] = 1
Y = b
X__ = X
Y__ = Y

batch_size = 32
labels = torch.tensor(Y, dtype=torch.float32)
dataset = DNADataset(X.tolist(), labels, tokenizer, window_size, kmer)
data_loader_test = DataLoader(dataset, batch_size=batch_size, shuffle=True)

clf_model.eval()
test_correct = 0
with torch.no_grad():
    for batch in data_loader_test:
        input_ids = batch["input_ids"].to('cuda')
        attention_mask = batch["attention_mask"].to('cuda')
        target_labels = batch["label"].to('cuda')
        logits, _ = clf_model(input_ids, attention_mask)
        predicted_labels = logits.argmax(dim=1)
        test_correct += (predicted_labels == target_labels.argmax(dim=1)).sum().item()

test_accuracy = test_correct / len(data_loader_test.dataset)
