import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

# Data
ex_name = 'mouse_cross_ko'
data = {
    'MESC-DNMT3aKO': [0.65, 0.5305, 0.56815],
    'MESC-DNMT3bKO': [0.55505, 0.73, 0.52635],
    'MISC-TET2/3KO': [0.5815, 0.53955, 0.73]
}
index = ['MESC-DNMT3aKO', 'MESC-DNMT3bKO', 'MISC-TET2/3KO']

ex_name = 'homo_cross_ko'
data = {
    'HESC-DNMT3KO': [0.76, 0.5518, 0.51235, 0.51785],
    'HESC-TETKO': [0.5709, 0.79, 0.80145, 0.7602],
    'HESC-QKO': [0.4891, 0.63525, 0.67, 0.7106],
    'HESC-PKO': [0.51595, 0.56005, 0.6531, 0.66]
}
index = ['HESC-DNMT3KO', 'HESC-TETKO', 'HESC-QKO', 'HESC-PKO']


ex_name = 'dnmt3_ko'
data = {
    'HESC-DNMT3KO': [0.76, 0.5606,0.5738],
    'MESC_DNMT3aKO': [0.64025,	0.65,	0.5305],
    'MESC_DNMT3bKO': [0.6396,	0.55505,	0.73],
}
index = ['HESC-DNMT3KO', 'MESC_DNMT3aKO', 'MESC_DNMT3bKO']


ex_name = 'tet_ko'
data = {
    'HESC-TETKO': [0.79,	0.48785],
    'MISC_TET23KO': [0.5065, 0.73],
}
index = ['HESC-TETKO', 'MISC_TET23KO']



# Creating a DataFrame
df = pd.DataFrame(data, index=index)

# Creating the heatmap
plt.figure(figsize=(10,7))
ax = sns.heatmap(df, annot=True, cmap="YlGnBu", linewidths=0.5, vmin=0.5, linecolor="black", vmax=1.0, annot_kws={"size": 22})

ax.set_xticklabels(ax.get_xticklabels(), fontsize=12)
ax.set_yticklabels(ax.get_yticklabels(), fontsize=12)
ax.tick_params(axis='both', which='both', pad=10)
cbar = ax.collections[0].colorbar
cbar.ax.tick_params(labelsize=16)


# plt.show()
plt.savefig('/Users/salehsereshki/Documents/fCpG/Fig2/{}.pdf'.format(ex_name), format='pdf', dpi=300)
