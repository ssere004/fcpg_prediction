import pandas as pd


HES_mpd_ad = '/home/ssere004/Organisms/ES/WGBS/hues8/wt_mapped/SRR8611939_1_bismark_bt2_pe.CpG_report.txt'
HES_gsm_ad = '/home/ssere004/Organisms/ES/WGBS/hues8/GSM3618718_HUES8_WT_WGBS.bed'

#MES_mpd_ad = '/data/saleh/Species/mouse/fCpG/wt_mapped/SRR6894127_1_bismark_bt2_pe.bismark.cov'
MES_mpd_ad = '/data/saleh/Species/mouse/fCpG/wt_mapped/merged_sorted.CX_report.txt'
MES_gsm_ad = '/data/saleh/Species/mouse/fCpG/WT_WGBS_processed.bed'

MsiES_mpd_ad = '/data/saleh/Species/mouse/fCpG/intestine/wt_mapped/SRR18645747_1_bismark_bt2_pe_.babismark.cov'
MsiES_gsm_ad = '/data/saleh/Species/mouse/fCpG/intestine/mouse_SI_WT.bed'

def f_reader(fn):
    if fn[-3:] == 'bed':
        column_types = {'chr': str, 'start': float, 'end': float, 'meth':float, 'coverage': float}
        df = pd.read_csv(fn, sep='\t', names = ['chr', 'start', 'end', 'meth', 'coverage'], dtype=column_types)
    if fn[-3:] == 'cov':
        column_types = {'chr': str, 'start': float, 'end': float, 'meth':float, 'm': float, 'u': float}
        df = pd.read_csv(fn, sep='\t', names = ['chr', 'start', 'end', 'meth', 'm', 'u'], dtype=column_types)
    if fn[-3:] == 'txt':
        column_types = {'chr': str, 'start': int, 'strand': str, 'm': int, 'u': int, 'context': str, 'thri': str }
        df = pd.read_csv(fn, sep='\t', names=['chr', 'start', 'strand', 'm', 'u', 'context', 'thri'], dtype=column_types)
        df['meth'] = df['m'] / (df['m'] + df['u'])
        df = df.dropna().reset_index(drop=True)
    if df['meth'].max > 1:
        df['meth'] = df['meth'] / df['meth'].max()
    return df[['chr', 'start', 'meth']]

def df_merger(df1, df2):
    df1 = df1.rename(columns={'meth': 'meth1'})
    df1['start'] = df1['start'] + 1
    df2 = df2.rename(columns={'meth': 'meth2'})
    merged_df = pd.merge(df1, df2, on=['chr', 'start'], how='outer').dropna()
    return merged_df

df1, df2 = f_reader(MsiES_gsm_ad), f_reader(MsiES_mpd_ad)
merged_df = df_merger(df1, df2)
print('GSM version number of Cs: {}, Mapped verion number of Cs: {}'.format(len(df1), len(df2)))
print('Merged version number of Cs: {}'.format(len(merged_df)))
print(merged_df['meth1'].corr(merged_df['meth2']), 'Correlation')
difference = (merged_df['meth1'] - merged_df['meth2']) ** 2
print(difference.mean(), 'Mean of square of differences')
print((merged_df['meth1'] - merged_df['meth2']).abs().mean(), 'mean of abs differences')

