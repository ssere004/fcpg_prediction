import sys
import os
project_root = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(project_root)

import preprocess.data_reader as dr
import pandas as pd

KOs = ['homo-DNMT3', 'homo-PKO', 'homo-QKO', 'homo-TET', 'mouse-mouse_3aKO', 'mouse-mouse_3bKO', 'mouse_si-mouse_SI_TET23_KO']
ws_sizes = ['ws_8', 'ws_10', 'ws_12']
for KO in KOs:
    dfs = []
    for ws in ws_sizes:
        fn = 'motif_pvals_{}_{}.pkl'.format(ws, KO)
        root = '../motif_founded/pvals/' + fn
        data = dr.load_dic(root)
        df = pd.DataFrame(list(data.items()), columns=["Key", "Value"])
        df['ws'] = ws
        dfs.append(df)
    res = pd.concat(dfs, axis=0).sort_values(by="Value")
    print(KO)
    print(res.head(3))

