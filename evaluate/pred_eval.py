import numpy as np
from sklearn.metrics import roc_curve, auc, accuracy_score
import matplotlib.pyplot as plt

# The predicted and real lablels should be provided to this module to plot the ROC curve.
# If you don't have it just run the DNABERT_finetune.py

og = 'human'
# og = 'mouse'

plt.rcParams.update({'font.size': 18}) # Adjusts the default font size
plt.rcParams['axes.labelsize'] = 22 # For x and y labels
plt.rcParams['axes.titlesize'] = 26 # For the title
plt.rcParams['xtick.labelsize'] = 16 # For x tick labels
plt.rcParams['ytick.labelsize'] = 16 # For y tick labels
plt.rcParams['legend.fontsize'] = 18 # For legend

if og == 'mouse':
    KOs = ['mouse_3aKO', 'mouse_3bKO', 'mouse_SI_TET23_KO']
    colors = ['dodgerblue', 'seagreen', 'saddlebrown']
    line_labels = ['DNMT3aKO', 'DNMT3bKO', 'TET2/3KO']
else:
    KOs = ['TET', 'DNMT3', 'QKO', 'PKO']
    colors = ['violet', 'cornflowerblue', 'lightslategray', 'darkorange']
    line_labels = ['TET', 'DNMT3', 'QKO', 'PKO']

root = ''

root = '/Users/salehsereshki/Desktop/ServerDownloadTemp/fCpG/pred_results/'

root = '/Users/salehsereshki/Desktop/Data/pyprojs/fCpG_prediction/dump/nodata_leakage_pred_results/'

plt.figure(figsize=(10, 8))
for i, KO in enumerate(KOs):
    if og == 'mouse':
        if KO == 'mouse_SI_TET23_KO':
            labels = np.load(root+'labels_mouse_si_6_512_{}_brt_include_meth_none.npy'.format(KO))
            preds = np.load(root+'predicted_mouse_si_6_512_{}_brt_include_meth_none.npy'.format(KO))
        else:
            labels = np.load(root+'labels_mouse_6_512_{}_brt_include_meth_none.npy'.format(KO))
            preds = np.load(root+'predicted_mouse_6_512_{}_brt_include_meth_none.npy'.format(KO))
    else:
        labels = np.load(root+'labels_hg19_6_512_{}_brt_include_meth_none.npy'.format(KO))
        preds = np.load(root+'predicted_hg19_6_512_{}_brt_include_meth_none.npy'.format(KO))
    y_true = np.argmax(labels, axis=1)
    y_pred = preds[:, 1]
    fpr, tpr, _ = roc_curve(y_true, y_pred)
    roc_auc = auc(fpr, tpr)
    accuracy = accuracy_score(y_true, np.argmax(preds, axis=1))
    plt.plot(fpr, tpr, color=colors[i], lw=3, label=f"{line_labels[i]} (AUC={roc_auc:.2f}, ACC={accuracy:.2f})")


plt.plot([0, 1], [0, 1], color='gray', lw=1, linestyle='--')

plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate', fontsize=20)
plt.ylabel('True Positive Rate', fontsize=20)


if og == 'mouse':
    plt.title('Mouse', fontsize=22)
else:
    plt.title('Human', fontsize=22)

# Add a legend with a larger font size
plt.legend(loc="lower right", fontsize=16)

# Add grid lines and set the background color
plt.grid(True)
plt.gca().set_facecolor('white')

# Annotate accuracy and AUC with a larger font size, different position, and a border
bbox_props = dict(boxstyle="square,pad=0.3", ec="black", lw=0.5, fc="white")
# plt.annotate(
#     f'Accuracy: {accuracy * 100:.2f}%\nAUC: {roc_auc:.2f}',
#     xy=(0.02, 0.97),
#     xycoords='axes fraction',
#     fontsize=12,
#     verticalalignment='top',
#     bbox=bbox_props
# )

# Display the plot
plt.savefig('/Users/salehsereshki/Documents/fCpG/Fig1/{}_nodataleakage.pdf'.format(og), format='pdf', dpi=300)







##############################################################

from sklearn.metrics import precision_score, recall_score, f1_score, confusion_matrix


def acc_calc(a, b):
    predicted_labels = np.argmax(b, axis=1)
    true_labels = np.argmax(a, axis=1)
    accuracy = np.mean(predicted_labels == true_labels)
    precision = precision_score(true_labels, predicted_labels)
    sensitivity = recall_score(true_labels, predicted_labels)  # Sensitivity is the same as recall
    f1 = f1_score(true_labels, predicted_labels)
    tn, fp, fn, tp = confusion_matrix(true_labels, predicted_labels).ravel()
    specificity = tn / (tn + fp)
    return accuracy, precision, sensitivity, specificity, f1


root = '/Users/salehsereshki/Desktop/ServerDownloadTemp/fCpG/pred_results/'

root = '/Users/salehsereshki/Desktop/Data/pyprojs/fCpG_prediction/dump/nodata_leakage_pred_results/'

spe_Ko_dic = {'human': ['DNMT3', 'TET', 'QKO', 'PKO'], 'mouse': ['mouse_3aKO', 'mouse_3bKO', 'mouse_SI_TET23_KO']}

for og in spe_Ko_dic:
    for KO in spe_Ko_dic[og]:
        if og == 'mouse':
            if KO == 'mouse_SI_TET23_KO':
                labels = np.load(root+'labels_mouse_si_6_512_{}_brt_include_meth_none.npy'.format(KO))
                preds = np.load(root+'predicted_mouse_si_6_512_{}_brt_include_meth_none.npy'.format(KO))
            else:
                labels = np.load(root+'labels_mouse_6_512_{}_brt_include_meth_none.npy'.format(KO))
                preds = np.load(root+'predicted_mouse_6_512_{}_brt_include_meth_none.npy'.format(KO))
        else:
            labels = np.load(root+'labels_hg19_6_512_{}_brt_include_meth_none.npy'.format(KO))
            preds = np.load(root+'predicted_hg19_6_512_{}_brt_include_meth_none.npy'.format(KO))
        accuracy, precision, sensitivity, specificity, f1 = acc_calc(preds, labels)
        print(f'{og} {KO} {accuracy} {precision} {sensitivity} {specificity} {f1}')
