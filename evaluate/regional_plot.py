import matplotlib.pyplot as plt
import pandas as pd
from matplotlib.ticker import MultipleLocator, StrMethodFormatter

# Assuming dataset_ is your DataFrame

dataset_ = pd.read_csv('./regional_preds_df_nodataleakage.csv', sep='\t')

#chr = 'chromosome 19'

fig, axs = plt.subplots(3, 1, figsize=(12, 5), constrained_layout=True)

# Enhance style
plt.style.use('seaborn-darkgrid')  # Use seaborn style for a professional look
plt.rcParams.update({'font.size': 12})  # Update font size for readability
min_position, max_postion = dataset_['position'].min() - 500, dataset_['position'].max() + 500

# First subplot
axs[0].scatter(dataset_['position'], dataset_['meth1'], label='WT methylation', alpha=0.9, s=15, edgecolors='w')
axs[0].scatter(dataset_['position'], dataset_['meth2'], label='TET KO methylation', alpha=0.9, s=15, edgecolors='w')
axs[0].set_xlabel('Position', fontsize=14)
axs[0].set_ylabel('Value', fontsize=14)
axs[0].set_title('methylation levels', fontsize=18)
axs[0].grid(True)
# axs[0].set_xlim(left=min_position, right=max_postion)
axs[0].legend(loc='upper left', bbox_to_anchor=(1, 1), fontsize=16, markerscale=1.5)

diff = abs(dataset_['meth2'] - dataset_['meth1'])
axs[1].scatter(dataset_['position'][diff > 0.6], diff[diff > 0.6], label='DMCs', alpha=0.9, s=15, edgecolors='w', color='red')
axs[1].scatter(dataset_['position'][diff < 0.6], diff[diff < 0.6], label='not DMCs', alpha=0.9, s=15, edgecolors='w', color='blue')
axs[1].set_xlabel('Position', fontsize=14)
axs[1].set_ylabel('methylation difference', fontsize=14)
axs[1].set_title('Difference in methylation levels', fontsize=18)
# axs[1].set_xlim(left=min_position, right=max_postion)
axs[1].grid(True)
axs[1].legend(loc='upper left', bbox_to_anchor=(1, 1), fontsize=16, markerscale=1.5)



# Second subplot
axs[2].scatter(dataset_['position'][dataset_['pred'] > 0.5], dataset_['pred'][dataset_['pred'] > 0.5], label='predicted DMCs', alpha=0.9, s=15, edgecolors='w', color='red')
axs[2].scatter(dataset_['position'][dataset_['pred'] <= 0.5], dataset_['pred'][dataset_['pred'] <= 0.5], label='predicted not DMCs', alpha=0.9, s=15, edgecolors='w', color='blue')
#axs[2].scatter(dataset_['position'], dataset_['pred'], label='pred', color='green', alpha=0.9, s=15, edgecolors='w')
axs[2].set_xlabel('Position', fontsize=14)
axs[2].set_ylabel('Pred', fontsize=14)
axs[2].set_title('prediction probability', fontsize=18)
# axs[2].set_xlim(left=min_position, right=max_postion)
axs[2].grid(True)
axs[2].legend(loc='upper left', bbox_to_anchor=(1, 1), fontsize=16, markerscale=1.5)

for ax in axs:
    ax.xaxis.set_major_locator(MultipleLocator(500))  # Set major ticks to multiples of 500
    ax.xaxis.set_major_formatter(StrMethodFormatter("{x:0.0f}"))

plt.savefig('/Users/salehsereshki/Documents/fCpG/Fig1/regional_nodataleakage.pdf', format='pdf', dpi=300)
