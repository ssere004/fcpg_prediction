import sys
import os

project_root = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(project_root)


# import configs
import preprocess.data_reader as data_reader
import preprocess.preprocess as preprocess
# import preprocess.preprocess as preprocess
# import process.process as process
import torch.optim as optim
from tqdm import tqdm
import pandas as pd
import torch
from torch.utils.data import DataLoader
import torch.nn as nn
from sklearn.model_selection import train_test_split
import numpy as np
import argparse
from transformers import BertTokenizer, BertModel
import configs
from utils import data_manager as dmngr
from transformers import AutoModel, AutoTokenizer, AutoModelForMaskedLM
from other_methods.hyeanaDNA import HyenaDNAPreTrainedModel, CharacterTokenizer
from torch.utils.data import Dataset

#conda activate hyena-dna

class DNASequenceClassifier(nn.Module):
    def __init__(self, bert_model, fc_ad, num_classes):
        super(DNASequenceClassifier, self).__init__()
        self.bert = bert_model
        self.conv1 = nn.Conv1d(in_channels=256, out_channels=128, kernel_size=5, stride=1, padding=2)
        self.relu = nn.ReLU()
        self.dropout = nn.Dropout(0.5)
        self.fc = nn.Sequential(
            nn.Linear(128, 64),
            nn.Dropout(0.5),
            nn.ReLU(),
            nn.Linear(64, 16),
            nn.Dropout(0.5),
            nn.ReLU(),
            nn.Linear(16, num_classes),
            nn.Softmax()
        )
        if fc_ad != None:
            self.fc.load_state_dict(torch.load(fc_ad))
    def forward(self, input_ids):
        #output_attentions=True
        x = self.bert(input_ids)
        x = x.permute(0, 2, 1)
        x = self.conv1(x)
        x = self.relu(x)
        x, _ = x.max(dim=2)
        logits = clf_model.fc(x)
        return logits
    def save(self, file_name):
        self.bert.save_pretrained(file_name+'_bert')
        torch.save(self.fc.state_dict(), file_name+'_torchnn.pth')

class DNADataset(Dataset):
    def __init__(self, sequences, labels, tokenizer, max_length, kmer):
        self.sequences = sequences
        self.labels = labels
        self.tokenizer = tokenizer
        self.max_length = max_length
        self.kmer = kmer
    def __len__(self):
        return len(self.sequences)
    def __getitem__(self, idx):
        sequence = self.sequences[idx]
        label = self.labels[idx]
        # Tokenize sequence
        tok_seq = self.tokenizer(sequence)["input_ids"]
        tok_seq = torch.LongTensor(tok_seq)
        return {
            "input_ids": tok_seq,
            "label": label,
            "sequence": sequence
        }

kmer = 6
KO = 'TET'
window_size = 512
model_type = 'hynea'
batch_size = 32
d_size = 100000
meth_seq_lst = None

num_classes = 2  # Binary classification

pretrained_model_name = 'hyenadna-medium-450k-seqlen'
model_name_alias = 'hyna'
max_length = 512
model = HyenaDNAPreTrainedModel.from_pretrained('./checkpoints', pretrained_model_name,)
# create tokenizer, no training involved :)
tokenizer = CharacterTokenizer(characters=['A', 'C', 'G', 'T', 'N'], model_max_length=max_length,)
clf_model = DNASequenceClassifier(model, None, num_classes)

cnfg = configs.hg19
assembly = data_reader.readfasta(cnfg['assembly'])
#assembly = data_reader.readfasta('/home/csgrads/ssere004/Organisms/homo_sapiens/hg19/hg19.fa')
dmrs, not_dmrs = dmngr.load_df(KO)
train_sample_size = int(min(d_size * 0.9, len(dmrs) * 2, len(not_dmrs) * 2))
train_dataset = pd.concat([dmngr.make_input_dataset(assembly, dmrs.sample(int(train_sample_size // 2)), 1, window_size=window_size, meth_seq_lst=meth_seq_lst, mc_num=20),
                     dmngr.make_input_dataset(assembly, not_dmrs.sample(int(train_sample_size // 2)), 0, window_size=window_size, meth_seq_lst=meth_seq_lst, mc_num=20)])
#dataset_ = pd.read_csv('./dump_files/sample_dataset.csv', header=None, index_col=None, names=['seq', 'meth', 'label'])
train_dataset = train_dataset.sample(frac=1).reset_index(drop=True)
train_dataset = train_dataset[train_dataset['seq'].str.len() == window_size]

train_include_dic = dmngr.make_train_included_dic(assembly, train_dataset, window_size)
test_dmrs, test_not_dmrs = dmngr.make_test_coordinate_df(train_include_dic, dmrs), dmngr.make_test_coordinate_df(train_include_dic, not_dmrs)
test_sample_size = int(min(d_size * 0.1, len(test_dmrs) * 2, len(test_not_dmrs) * 2))

test_dataset = pd.concat([dmngr.make_input_dataset(assembly, test_dmrs.sample(int(test_sample_size // 2)), 1, window_size=window_size, meth_seq_lst=meth_seq_lst, mc_num=20),
                     dmngr.make_input_dataset(assembly, test_not_dmrs.sample(int(test_sample_size // 2)), 0, window_size=window_size, meth_seq_lst=meth_seq_lst, mc_num=20)])
test_dataset = test_dataset.sample(frac=1).reset_index(drop=True)
test_dataset = test_dataset[test_dataset['seq'].str.len() == window_size]

print("Non overlapping test set is created!!")

assert preprocess.find_closest_distances(train_dataset, test_dataset)['closest_distance'].min() >= window_size//2

x_train, y_train = train_dataset[[col for col in train_dataset.columns if col not in ['label', 'chr', 'position']]], dmngr.prepare_y(train_dataset['label'])
x_test, y_test = test_dataset[[col for col in test_dataset.columns if col not in ['label', 'chr', 'position']]], dmngr.prepare_y(test_dataset['label'])

# Set batch size and max sequence length
labels = torch.tensor(y_train, dtype=torch.float32)
dataset = DNADataset(x_train['seq'].tolist(), labels, tokenizer, window_size, kmer)
data_loader_train = DataLoader(dataset, batch_size=batch_size, shuffle=True)

labels = torch.tensor(y_test, dtype=torch.float32)
dataset = DNADataset(x_test['seq'].tolist(), labels, tokenizer, window_size, kmer)
data_loader_test = DataLoader(dataset, batch_size=batch_size, shuffle=True)

optimizer = optim.Adam(clf_model.parameters(), lr=1e-5)
criterion = nn.BCELoss()
clf_model.to('cuda')
# Set the number of epochs
num_epochs = 5
total_samples = len(data_loader_train.dataset)  # Total number of samples
batch_size = data_loader_train.batch_size  # Batch size
batch_size_test = data_loader_test.batch_size
total_samples_test = len(data_loader_test.dataset)
# Training loop
for epoch in range(num_epochs):
    clf_model.train()
    total_loss = 0
    total_correct = 0
    total_processed = 0  # To keep track of total processed samples
    progress_bar = tqdm(data_loader_train, desc=f"Epoch {epoch+1}/{num_epochs}", leave=False)
    for batch in progress_bar:
        input_ids = batch["input_ids"].to('cuda')
        target_labels = batch["label"].to('cuda')
        optimizer.zero_grad()
        logits = clf_model(input_ids)
        loss = criterion(logits, target_labels)
        loss.backward()
        optimizer.step()
        predictions = logits.argmax(dim=1)
        total_correct += (predictions == target_labels.argmax(dim=1)).sum().item()
        total_processed += batch_size
        if total_processed % (10*batch_size) == 0:
            b_acc = total_correct / total_processed
            progress_percentage = (total_processed / total_samples) * 100
            progress_bar.set_postfix(loss=loss.item(), batch_acc=b_acc, progress=f"{progress_percentage:.2f}%")
        total_loss += loss.item()
    epoch_accuracy = total_correct / total_samples
    average_loss = total_loss / len(data_loader_train)
    print(f"Epoch [{epoch+1}/{num_epochs}] - Average Loss: {average_loss:.4f}, Epoch Accuracy: {epoch_accuracy:.4f}")
    # Evaluate the model on test data
clf_model.eval()
test_correct = 0
#keep track of the predicted and labels to draw the ROC curve
predicted_test, labels_test = np.zeros((total_samples_test, 2)) - 1, np.zeros((total_samples_test, 2)) - 1
idx = 0
with torch.no_grad():
    for batch in data_loader_test:
        input_ids = batch["input_ids"].to('cuda')
        target_labels = batch["label"].to('cuda')
        optimizer.zero_grad()
        logits = clf_model(input_ids)
        predicted_labels = logits.argmax(dim=1)
        test_correct += (predicted_labels == target_labels.argmax(dim=1)).sum().item()
        predicted_test[idx: idx + len(logits)] = logits.cpu().numpy()
        labels_test[idx: idx + len(target_labels)] = target_labels.cpu().numpy()
        idx += len(logits)

assert -1 not in predicted_test and -1 not in labels_test
if not os.path.exists("./dump_files/pred_results/"): os.makedirs("./dump_files/pred_results/")

np.save("./dump_files/pred_results/predicted_{}_{}_{}_{}_{}.npy".format(str(cnfg['og']), str(kmer), str(window_size), str(KO), model_name_alias), predicted_test)
np.save("./dump_files/pred_results/labels_{}_{}_{}_{}_{}.npy".format(str(cnfg['og']), str(kmer), str(window_size), str(KO), model_name_alias), labels_test)

test_accuracy = test_correct / total_samples_test
with open("kmer_test_BERT_classification_results.txt", "a") as file_object:
    file_object.write('%s\t%s\t%s\t%s\t%s' %(str(kmer), str(window_size), str(KO), str(test_accuracy), str(model_type)))
    file_object.write("\n")

clf_model.save("./dump_files/pretrained_models/" + "kmer_"+str(kmer) + "_ws_" + str(window_size) + "_KO_" + str(KO) + "_model_" + str(model_type))

