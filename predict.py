import configs
import tensorflow as tf
import preprocess.data_reader as data_reader
import preprocess.preprocess as preprocess
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
from process import process

window_size = 512

fcpgs = data_reader.read_fcpgs(configs.fCpGs_address1)
epic2 = data_reader.read_epic2_conf(configs.epic_file)
#epic2 = read_epic2_conf('/home/ssere004/Organisms/homo_sapiens/meth_epic2_config_files/EPIC-8v2-0_A1.csv')

positive_set = preprocess.make_fcpg_loc_df(fcpgs, epic2)
negative_set = epic2[~epic2['Name'].isin(fcpgs['ID'])][['Name', 'chr', 'position']].sample(n=len(positive_set))
cnfg = configs.hg19
sequences_df = preprocess.convert_assembely_to_onehot(cnfg['og'], data_reader.readfasta(cnfg['assembly']), from_file=False)
#following line is not tested!!!
annot_seq = preprocess.make_annotseq_dic(data_reader.read_annot(cnfg['gene_annotation']), ['gene'], sequences_df, strand_spec=False)

input_list = [sequences_df, annot_seq]
sample_list = [positive_set, negative_set]
for smple in sample_list:
    smple['position'] = smple['position'].astype(int)

X, Y = process.input_maker(input_list, sample_list, window_size=window_size)

x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=0.1, random_state=None)

clf = RandomForestClassifier(random_state=0, n_estimators=50, warm_start=True, n_jobs=-1)

xx = x_train
nsamples, nx, ny, nz = xx.shape
xx = xx.reshape((nsamples, nx*ny))
yy = y_train
clf.fit(xx, yy)
clf.n_estimators += 100
nsamples, nx, ny, nz = x_test.shape
x_test = x_test.reshape((nsamples, nx*ny))
y_pred=clf.predict(x_test)

accuracy_score(y_test, y_pred.round())


##gff-version 3
#!gff-spec-version 1.21
#!processor NCBI annotwriter
#!genome-build GRCh38.p14
#!genome-build-accession NCBI_Assembly:GCF_000001405.40

#!annotation-date 03/15/2023
#!annotation-source NCBI RefSeq GCF_000001405.40-RS_2023_03
##sequence-region NC_000001.11 1 248956422
##species https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id=9606
