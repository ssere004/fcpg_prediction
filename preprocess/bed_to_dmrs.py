from process import process
import data_reader as dr

fn1 = 'GSM3618718_HUES8_WT_WGBS.bed'
fn2 = 'GSM3618720_HUES8_TKO_WGBS.bed'

dmrs, not_dmrs = find_DMRs(read_wgbs_bed(fn1), read_wgbs_bed(fn2), 10, 0.6)

