import random
import pandas as pd

# Number of samples
num_samples = 500000

# Length of DNA sequences
sequence_length = 200

# Generate X (list of DNA sequences)
X = []
for _ in range(num_samples):
    # Generate random DNA sequence of length 200
    dna_sequence = ''.join(random.choice(['A', 'C', 'G', 'T']) for _ in range(sequence_length))
    X.append(dna_sequence)

# Generate Y (binary labels)
half_size = num_samples // 2
Y = [0] * half_size + [1] * half_size

# For the half where Y is 1, impute a specific sequence of length 20 randomly
specific_sequence = "AAGCTCGAGCTAGCTAGCTA"  # Example specific sequence of length 20
for i in range(half_size, num_samples):
    # Choose a random index to insert the specific sequence
    insert_index = random.randint(0, sequence_length - len(specific_sequence))
    # Impute the specific sequence at the chosen index
    modified_sequence = X[i][:insert_index] + specific_sequence + X[i][insert_index + len(specific_sequence):]
    X[i] = modified_sequence

# Shuffle X and Y together
combined = list(zip(X, Y))
random.shuffle(combined)
X, Y = zip(*combined)

X = pd.Series(X)
Y = pd.Series(Y)
X.to_csv('X_dummy.csv')
Y.to_csv('Y_dummy.csv')
