import sys
import os
project_root = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(project_root)
import numpy as np
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-KO', '--knockout', help='TET or DNMT3, QKO, PKO, mouse_3aKO, mouse_3bKO, mouse_SI_TET23_KO', required=True)
parser.add_argument('-sp', '--species', help='species: homo, mouse, mouse_si', required=False, default='BERT')

args = parser.parse_args()

KO = args.knockout
species = args.species

sequences, labels = np.load('./dump_files/motif_finding_feed/{}_{}_sequences.npy'.format(species, KO), allow_pickle=True), \
                            np.load('./dump_files/motif_finding_feed/{}_{}_labels.npy'.format(species, KO), allow_pickle=True)


pos_seqs = sequences[labels == 1]
neg_seqs = sequences[labels == 0]

import preprocess.data_reader as data_reader
data_reader.write_fasta(pos_seqs, './dump_files/motif_finding_feed/{}_{}_pos_seq.fasta'.format(species, KO))
data_reader.write_fasta(neg_seqs, './dump_files/motif_finding_feed/{}_{}_neg_seq.fasta'.format(species, KO))
