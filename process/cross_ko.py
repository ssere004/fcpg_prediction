import sys
import os
project_root = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(project_root)
import preprocess.data_reader as data_reader
import pandas as pd
import torch
from torch.utils.data import Dataset, DataLoader
import numpy as np
import argparse
from transformers import BertTokenizer, BertModel
import configs
import utils.model_manager as mdlmngr
from utils.data_manager import DNADataset
import utils.data_manager as dmngr


parser = argparse.ArgumentParser()
parser.add_argument('-kmer', '--kmer', help='kmer', required=True)
parser.add_argument('-ws', '--window_size', help='window_size', required=True)
parser.add_argument('-test_KO', '--test_knockout', help='TET or DNMT3, QKO, PKO, mouse_3aKO, mouse_3bKO, mouse_SI_TET23_KO', required=True)
parser.add_argument('-train_KO', '--train_knockout', help='TET or DNMT3, QKO, PKO, mouse_3aKO, mouse_3bKO, mouse_SI_TET23_KO', required=True)
parser.add_argument('-m', '--model', help='model, BERT, BigBird, nucleotide-transformer', required=True)
parser.add_argument('-tsp', '--test_species', help='test species: homo, mouse, mouse_si', required=False, default='BERT')
args = parser.parse_args()

# kmer = 6
# train_KO = 'TET'
# test_KO = 'TET'
# window_size = 512
# model_type = 'BERT'
# species = 'homo'


# kmer = 6
# train_KO = 'DNMT3'
# test_KO = 'TET'
# window_size = 512

kmer = int(args.kmer)
test_KO = args.test_knockout
train_KO = args.train_knockout
window_size = int(args.window_size)
model_type = args.model
species = args.test_species

model_name = "./dump_files/pretrained_models/" + "kmer_"+str(kmer) + "_ws_" + str(window_size) + "_KO_" + str(train_KO) + "_model_" + str(model_type)+ '_methyl' + str('none')
clf_model = mdlmngr.load_clf_model(model_name)
#clf_model = mdlmngr.load_clf_model("./dump_files/pretrained_models_/" + "kmer_"+str(kmer) + "_ws_" + str(window_size) + "_KO_" + str(train_KO))
tokenizer = BertTokenizer.from_pretrained("zhihan1996/DNA_bert_"+str(kmer), trust_remote_code=True)
print('model is loaded....', model_name)
cnfg_dic = {'homo': configs.hg19, 'mouse': configs.mouse, 'mouse_si': configs.mouse_si}
cnfg = cnfg_dic[species]
assembly = data_reader.readfasta(cnfg['assembly'])
#assembly = data_reader.readfasta('/home/csgrads/ssere004/Organisms/homo_sapiens/hg19/hg19.fa')
dmrs, not_dmrs = dmngr.load_df(test_KO)
print('data is loaded... ')

sample_size = min(20000, len(dmrs)*2, len(not_dmrs)*2)
dataset = pd.concat([dmngr.make_input_dataset(assembly, dmrs.sample(int(sample_size//2)), 1, window_size=window_size),
                     dmngr.make_input_dataset(assembly, not_dmrs.sample(int(sample_size//2)), 0, window_size=window_size)])
dataset = dataset.sample(frac=1).reset_index(drop=True)

X = dataset.drop(columns=['label'])
Y = np.asarray(pd.cut(dataset['label'], bins=2, labels=[0, 1], right=False))
b = np.zeros((Y.size, Y.max()+1))
b[np.arange(Y.size), Y] = 1
Y = b

batch_size = 32
labels = torch.tensor(Y, dtype=torch.float32)
dataset = DNADataset(X, labels, tokenizer, window_size, kmer)
data_loader_test = DataLoader(dataset, batch_size=batch_size, shuffle=True)

total_samples_test = len(data_loader_test.dataset)

clf_model.to('cuda')
clf_model.eval()
test_correct = 0
#keep track of the predicted and labels to draw the ROC curve
predicted_test, labels_test = np.zeros((total_samples_test, 2)) - 1, np.zeros((total_samples_test, 2)) - 1
idx = 0

with torch.no_grad():
    for batch in data_loader_test:
        input_ids = batch["input_ids"].to('cuda')
        attention_mask = batch["attention_mask"].to('cuda')
        target_labels = batch["label"].to('cuda')
        methylations = batch["methylations"].to('cuda')
        input_ids.requires_grad = False
        attention_mask.requires_grad = False
        target_labels.requires_grad = False
        logits, _ = clf_model(input_ids, attention_mask, methylations=methylations)
        predicted_labels = logits.argmax(dim=1)
        test_correct += (predicted_labels == target_labels.argmax(dim=1)).sum().item()
        predicted_test[idx: idx + len(logits)] = logits.cpu().numpy()
        labels_test[idx: idx + len(target_labels)] = target_labels.cpu().numpy()
        idx += len(logits)

assert -1 not in predicted_test and -1 not in labels_test

# np.save("./dump_files/pred_results/cross_ko/predicted_{}_{}_{}_{}.npy".format(str(kmer), str(window_size), str(args.test_knockout), str(args.train_knockout)), predicted_test)
# np.save("./dump_files/pred_results/cross_ko/labels_{}_{}_{}_{}.npy".format(str(kmer), str(window_size), str(args.test_knockout), str(args.train_knockout)), labels_test)

test_accuracy = test_correct / total_samples_test
with open("cross_ko_results.txt", "a") as file_object:
    file_object.write('%s\t%s\t%s\t%s\t%s\t' %(str(kmer), str(window_size), str(args.test_knockout), str(args.train_knockout), str(test_accuracy)))
    file_object.write("\n")
