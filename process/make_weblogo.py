from logomaker.src.Logo import Logo
import logomaker
from matplotlib import pyplot as plt
import re
import pandas as pd

import sys
import os
project_root = os.path.dirname("/home/ssere004/fCpG_prediction/fcpg_prediction/")
sys.path.append(project_root)
import numpy as np
import argparse

KO = 'TET'
species = 'homo'


parser = argparse.ArgumentParser()
parser.add_argument('-KO', '--knockout', help='TET or DNMT3, QKO, PKO, mouse_3aKO, mouse_3bKO, mouse_SI_TET23_KO', required=True)
parser.add_argument('-sp', '--species', help='species: homo, mouse, mouse_si', required=False, default='BERT')

args = parser.parse_args()

KO = args.knockout
species = args.species

root = '/home/ssere004/fCpG_prediction/fcpg_prediction/dump_files/motifs/streme/{}-{}_STREME'.format(species, KO)

def parse_matrices_from_file(filename):
    with open(filename, 'r') as file:
        text = file.read()
    def parse_matrices(text):
        sections = text.strip().split('\n\n')
        matrices = {}
        for section in sections:
            lines = section.split('\n')
            header = lines[0]
            name = re.search(r'^\d+-(\w+)', header).group(1)
            e_val = re.search(r'e_val = (\S+)', header).group(1)
            matrix_data = [list(map(float, line.split())) for line in lines[1:]]
            matrices[name] = {
                'e_val': e_val,
                'matrix': matrix_data
            }
        return matrices
    return parse_matrices(text)

res = parse_matrices_from_file(root+ '/streme_selected_20.txt')

for mtf in res.keys():
    matrix = res[mtf]['matrix']
    pwm_df = pd.DataFrame({'A': [row[0] for row in matrix], 'C': [row[1] for row in matrix],
                           'G': [row[2] for row in matrix], 'T': [row[3] for row in matrix]})
    logo = logomaker.Logo(pwm_df, figsize=(10, 3))
    plt.savefig(root+'/{}_logo_20.png'.format(mtf), dpi=300, bbox_inches='tight')
    plt.close()
