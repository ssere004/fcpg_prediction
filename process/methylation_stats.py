import sys
import os

project_root = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(project_root)

import pandas as pd
import numpy as np
#
hues8_root = '/home/ssere004/Organisms/ES/WGBS/hues8/'
mouse_root = '/data/saleh/Species/mouse/fCpG/'
mouse_si_root = '/data/saleh/Species/mouse/fCpG/intestine/'
#
# # ['chr', 'start', 'stop', 'meth', 'coverage']
bed_files = {'hu_WT': hues8_root+'GSM3618718_HUES8_WT_WGBS.bed',
             'hu_DKO': hues8_root+'DKO_methylations.bed',
             'hu_TKO': hues8_root+'GSM3618720_HUES8_TKO_WGBS.bed',
             'hu_QKO': hues8_root+'GSM3618719_HUES8_QKO_WGBS.bed',
             'hu_PKO': hues8_root+'GSM3618721_HUES8_PKO_WGBS.bed',
             'mou_3aKO': mouse_root+'3aKO_WGBS_processed.bed',
             'mou_3bKO': mouse_root+'3bKO_WGBS_processed.bed',
             'mou_WT': mouse_root+'WT_WGBS_processed.bed',
             'mou_si_tet23KO_': mouse_si_root + 'mouse_SI_KO.bed',
             'mou_si_WT': mouse_si_root + 'mouse_SI_WT.bed'
             }

#
# report = {'accession': [], 'cov_avg': [], 'meth_avg': [], 'enough_covered': []}
#
# for key in bed_files.keys():
#     print('in progerss for ' + key)
#     df = pd.read_csv(bed_files[key], sep='\t', names=['chr', 'start', 'stop', 'meth', 'coverage'])
#     print('dataframe is read ')
#     report['accession'].append(key), report['cov_avg'].append(np.mean(df['coverage']))
#     report['meth_avg'].append(np.mean(df['meth'])), report['enough_covered'].append(len(df[df.coverage > 10]))
#
# report = pd.DataFrame(report)
# report.to_csv('./dump_files/report/methylation_stats.csv', sep='\t', index=False)

# import utils.data_manager as dmngr
# res = {'KO': [], 'dmCs': [], 'not_dmCs': []}
# KOs = ['DNMT3', 'TET', 'QKO', 'PKO', 'mouse_3aKO', 'mouse_3bKO', 'mouse_SI_TET23_KO']
# for KO in KOs:
#     dmrs, not_dmrs = dmngr.load_df(KO)
#     res['KO'].append(KO), res['dmCs'].append(len(dmrs)), res['not_dmCs'].append(len(not_dmrs))
#
# report = pd.DataFrame(res)
# report.to_csv('./dump_files/report/dmCs_stats.csv', sep='\t', index=False)


KO_WT_pairs = [('hu_DKO', 'hu_WT'), ('hu_TKO', 'hu_WT'), ('hu_QKO', 'hu_WT'), ('hu_PKO', 'hu_WT'),
               ('mou_3aKO', 'mou_WT'), ('mou_3bKO', 'mou_WT'),
               ('mou_si_tet23KO_', 'mou_si_WT')]


report = {'accession': [], 'higher_in_wt': [], 'lower_in_wt': []}
for ko, wt in KO_WT_pairs:
    fn1, fn2 = bed_files[wt], bed_files[ko]
    df1, df2 = pd.read_csv(fn1, sep='\t', names = ['chr', 'start', 'end', 'meth', 'coverage']), pd.read_csv(fn2, sep='\t', names = ['chr', 'start', 'end', 'meth', 'coverage'])
    coverage_threshold, min_diff, decrease = 10, 0.6, True
    df1, df2 = df1[df1.coverage > coverage_threshold], df2[df2.coverage > coverage_threshold]
    df1, df2 = df1.drop('end', axis=1), df2.drop('end', axis=1)
    merged_df = pd.merge(df1, df2, on=['chr', 'start']) #20 sec run
    dmrs_higher_in_wt = merged_df[merged_df.meth_x - merged_df.meth_y > min_diff]
    dmrs_lower_in_wt = merged_df[-merged_df.meth_x + merged_df.meth_y > min_diff]
    report['accession'].append(ko), report['higher_in_wt'].append(len(dmrs_higher_in_wt)), report['lower_in_wt'].append(len(dmrs_lower_in_wt))
report = pd.DataFrame(report)
report.to_csv('./dump_files/report/positive_vs_negative_dmCs.csv', sep='\t', index=False)

