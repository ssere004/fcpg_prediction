import numpy as np
from sklearn.utils import shuffle
import pandas as pd
from scipy.stats import fisher_exact
from pandarallel import pandarallel

def input_maker(input_list, sample_list, window_size=128):
    if len(input_list) == 0:
        raise ValueError('no input in the input list')
    for inp in input_list:
        if len(inp.keys()) == 0:
            raise ValueError('sequences dataframe does not contain sequences')
    chrs = list(input_list[0].keys())
    chr_tmp = chrs[0]
    for inp in input_list:
        if len(inp[chr_tmp].shape) != 2:
            raise ValueError('all inputs must have the same number of dimension 2')
    pad_size = 0
    if window_size < 20:
        pad_size = 20 - window_size
    x_col_size = 0
    for inp in input_list:
        x_col_size += inp[chr_tmp].shape[1]
    X = np.zeros((sum([len(smple) for smple in sample_list]), window_size + pad_size, x_col_size))
    Y = np.zeros(sum([len(smple) for smple in sample_list]))
    profiles = {}
    for chr in chrs:
        chr_inps = []
        len_ref_chr = len(input_list[0][chr])
        same_size_chrs = True
        for inp in input_list:
            chr_inps.append(inp[chr])
            if len(inp[chr]) != len_ref_chr:
                same_size_chrs = False
        if same_size_chrs:
            profiles[chr] = np.concatenate(chr_inps, axis=1)
    label = 0
    last_i = 0
    for sample_n in sample_list:
        for i in range(last_i, last_i + len(sample_n)):
            row = sample_n.iloc[i - last_i]
            try:
                X[i][:window_size] = profiles[row['chr']][int(row['position'] - window_size/2): int(row['position'] + window_size/2)]
                # The bp in the center by itself can be detected by the classifier and then be used to predict the context
                X[i][:int(window_size/2)] = 0
                X[i][window_size: pad_size + window_size] = -1
            except ValueError:
                print(i - last_i)
            Y[i] = label
        last_i = i
        label += 1
    if len(Y[Y == 1]) > len(Y[Y==0]):
        X = X[:len(Y) - (len(Y[Y==1]) - len(Y[Y==0]))]
        Y = Y[:len(Y) - (len(Y[Y==1]) - len(Y[Y==0]))]
    elif len(Y[Y==1]) < len(Y[Y==0]):
        X = X[(len(Y[Y==0]) - len(Y[Y==1])):]
        Y = Y[(len(Y[Y==0]) - len(Y[Y==1])):]
    print('Each sample ratio is %f' %float(len(Y[Y==1]) / float(len(Y))))
    X, Y = shuffle(X, Y, random_state=0)
    return np.expand_dims(np.asarray(X), 3), np.asarray(Y)

def BERT_input_maker(assembly, sample_list, window_size):
    res_x, res_y = [], []
    lable = 0
    for smpl in sample_list:
        res_x.append(smpl.apply(lambda row: assembly[row['chr'].lower()][int(row['position'] - window_size/2): int(row['position'] + window_size/2)], axis=1))
        res_y.append(pd.Series([lable] * len(smpl)))
        lable+=1
    X = pd.concat(res_x, axis=0)
    Y = pd.concat(res_y)
    X, Y = shuffle(X, Y, random_state=0)
    return X, Y

def calculate_p_value(row):
    contingency_table = [
        [round(row['meth_x'] * row['coverage_x']), round((1 - row['meth_x']) * row['coverage_x'])],
        [round(row['meth_y'] * row['coverage_y']), round((1 - row['meth_y']) * row['coverage_y'])]
    ]
    _, p_value = fisher_exact(contingency_table)
    return p_value

#input: two dataframes where they have columns for [chr, start, end, meth, coverage]
#output: a merged dataframe containing the shared cytosines with at least the minimum coverage, \\
# and at least the min_diff between the samples methylation level and their DMR p_value

wt_ko_pairs = [['/data/saleh/fCpG/mouse/WT_WGBS_processed.bed', '/data/saleh/fCpG/mouse/3aKO_WGBS_processed.bed', 'mouse_3aKO_'],
 ['/data/saleh/fCpG/mouse/WT_WGBS_processed.bed', '/data/saleh/fCpG/mouse/3bKO_WGBS_processed.bed', 'mouse_3bKO_']]



def read_case_study_dfs(fn):
    df = pd.read_csv(fn, sep='\t', header=None)
    df.columns = ['chr', 'start', 'end', 'ratio', 'meth_on_1000', 'strand']
    df = df[df.strand == '+']
    df[['meth_reads', 'coverage']] = df['ratio'].str.strip("'").str.split('/', expand=True)
    df['meth'] = df['meth_reads'].astype(int) / df['coverage'].astype(int)
    df['coverage'] = df['coverage'].astype(int)
    return df[['chr', 'start', 'end', 'meth', 'coverage']]

def read_case_study2_dfs(fn):
    df = pd.read_csv(fn, sep='\t')
    df = df[["#chrom", "start", "end", "ratio", "totalC", "strand"]]
    df.columns = ['chr', 'start', 'end', 'meth', 'coverage', "strand"]
    #df = df[df.strand == '+']
    df = df.dropna(subset=['coverage'])
    df['coverage'] = df['coverage'].astype(int)
    df['start'] = df['start'].astype(int)
    df['meth'] = df['meth'].astype(float)
    return df[['chr', 'start', 'end', 'meth', 'coverage']]


def read_case_study3_dfs(fn):
    df = pd.read_csv(fn, sep='\t', header=None)
    df.columns = ['chr', 'start', 'end', 'meth']
    df['meth'] = df['meth']/100
    df['coverage'] = 100
    df['start'] = df['start'].astype(int)
    df['meth'] = df['meth'].astype(float)
    return df


def read_case_study4_dfs(fn):
    df = pd.read_csv(fn, sep='\t')
    df.columns = ['chr', 'pos', 'mCwatson', 'Cwatson', 'mCcrick', 'Ccrick']
    df['coverage'] = (df['mCwatson'] + df['Cwatson']) + (df['mCcrick'] + df['Ccrick'])
    df['meth'] = (df['mCwatson'] + df['mCcrick']) / df['coverage']
    df = df[['chr', 'pos', 'meth', 'coverage']]
    df.columns = ['chr', 'start', 'meth', 'coverage']
    df['start'] = df['start'] - 1
    df['end'] = df['start'] + 2
    return df[['chr', 'start', 'end', 'meth', 'coverage']]



def find_DMRs(df1, df2, coverage_threshold, min_diff, decrease=False):
    #df1, df2, = pd.read_csv(fn1, sep = '\t', names=range(17))[[0,1,2,15,16]], pd.read_csv(fn2, sep = '\t', names=range(17))[[0,1,2,15,16]]
    #fn1, fn2 = 'mouse_SI_WT.bed', 'mouse_SI_KO.bed'
    #df1, df2 = pd.read_csv(fn1, sep='\t', names = ['chr', 'start', 'end', 'meth', 'coverage']), pd.read_csv(fn2, sep='\t', names = ['chr', 'start', 'end', 'meth', 'coverage'])
    #coverage_threshold, min_diff, decrease = 10, 0.6, True
    df1, df2 = df1[df1.coverage > coverage_threshold], df2[df2.coverage > coverage_threshold]
    df1, df2 = df1.drop('end', axis=1), df2.drop('end', axis=1)
    merged_df = pd.merge(df1, df2, on=['chr', 'start']) #20 sec run
    #merged_df = merged_df[merged_df.meth_x > merged_df.meth_y]
    dmrs = merged_df[abs(merged_df.meth_x - merged_df.meth_y) > min_diff]
    pandarallel.initialize(progress_bar=True)
    dmrs['p_value'] = dmrs.parallel_apply(calculate_p_value, axis=1)
    # if decrease:
    #     dmrs = dmrs[dmrs.meth_x < dmrs.meth_y]
    # else:
    #     dmrs = dmrs[dmrs.meth_x > dmrs.meth_y]
    subset_df = pd.merge(merged_df, dmrs, on=['chr', 'start'], how='left', indicator=True)
    not_dmrs = subset_df[subset_df['_merge'] == 'left_only'].drop(columns=['_merge']).iloc[:, :6]
    not_dmrs.columns = dmrs.columns[:6]
    #not_dmrs.to_csv('mouse_3bko_notDMRs.txt', sep='\t', index=False, header=False)
    #dmrs.to_csv('mouse_3bko_DMRs.txt', sep='\t', index=False, header=False)
    return dmrs, not_dmrs
