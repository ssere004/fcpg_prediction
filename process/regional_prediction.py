# import preprocess.preprocess as preprocess
# import process.process as process
import preprocess.data_reader as data_reader
import pandas as pd
import torch
from torch.utils.data import DataLoader
import torch.nn as nn
from sklearn.model_selection import train_test_split
import numpy as np
import argparse
from transformers import BertTokenizer, BertModel
import configs
from utils.model_manager import DNASequenceClassifier
from utils.data_manager import DNADataset
from utils import data_manager as dmngr
import utils.model_manager as mdlmngr
import random


kmer = 6
KO = 'TET'
window_size = 512

clf_model = mdlmngr.load_clf_model("/home/ssere004/fCpG_prediction/fcpg_prediction/dump_files/pretrained_models/nodata_leakage/" + "kmer_"+str(kmer) + "_ws_" + str(window_size) + "_KO_" + str(KO) + '_model_BERT_methylnone')


tokenizer = BertTokenizer.from_pretrained("zhihan1996/DNA_bert_"+str(kmer), trust_remote_code=True)

cnfg = configs.hg19
assembly = data_reader.readfasta(cnfg['assembly'])
dmrs, not_dmrs = dmngr.load_df(KO)



def make_input_dataset(assembly, coordinate_df, label, window_size=128):
     res_df = pd.DataFrame()
     res_df['seq'] = coordinate_df.apply(lambda row: str(assembly[row['chr'].lower()][int(row['position']) - int(window_size//2): int(row['position']) + int(window_size//2)]), axis=1)
     res_df['label'] = label
     res_df['meth1'] = coordinate_df['meth1']
     res_df['meth2'] = coordinate_df['meth2']
     res_df['chr'] = coordinate_df['chr']
     res_df['position'] = coordinate_df['position']
     return res_df

chr = 'chr19'
start = 2493377
end = 2497377


b = True

while b:
    chr = random.choice(list(assembly.keys()))
    start = random.randint(0, len(assembly[chr]) - 4000)
    end = start + 4000
    reg_dmrs = dmrs[(dmrs.chr == chr) & (dmrs.position < end) & (dmrs.position > start)]
    reg_not_dmrs = not_dmrs[(not_dmrs.chr == chr) & (not_dmrs.position < end) & (not_dmrs.position > start)]
    if len(reg_dmrs) > 30 and len(reg_not_dmrs) > 30:
        b = False

reg_dmrs = dmrs[(dmrs.chr == chr) & (dmrs.position < end) & (dmrs.position > start)]
reg_not_dmrs = not_dmrs[(not_dmrs.chr == chr) & (not_dmrs.position < end) & (not_dmrs.position > start)]

dataset_ = pd.concat([make_input_dataset(assembly, reg_dmrs, 1, window_size=window_size),
                      make_input_dataset(assembly, reg_not_dmrs, 0, window_size=window_size)])



X = dataset_.drop(columns=['label'])
Y = np.asarray(pd.cut(dataset_['label'], bins=2, labels=[0, 1], right=False))
b = np.zeros((Y.size, Y.max()+1))
b[np.arange(Y.size), Y] = 1
Y = b

batch_size = 2
labels = torch.tensor(Y, dtype=torch.float32)
dataset = DNADataset(X, labels, tokenizer, window_size, kmer)
data_loader_test = DataLoader(dataset, batch_size=batch_size, shuffle=False)



clf_model.to('cuda')
clf_model.eval()

predicted_test, labels_test = np.zeros((len(X), 2)) - 1, np.zeros((len(X), 2)) - 1
idx = 0
with torch.no_grad():
     for batch in data_loader_test:
         input_ids = batch["input_ids"].to('cuda')
         attention_mask = batch["attention_mask"].to('cuda')
         target_labels = batch["label"].to('cuda')
         logits, _ = clf_model(input_ids, attention_mask)
         predicted_labels = logits.argmax(dim=1)
         predicted_test[idx: idx + len(logits)] = logits.cpu().numpy()
         labels_test[idx: idx + len(target_labels)] = target_labels.cpu().numpy()
         idx += len(logits)
dataset_['pred'] = predicted_test[:,1]
dataset_.to_csv('regional_preds_df.csv', sep='\t', index=False)


############################################


predicted_test, labels_test = np.zeros((len(X), 2)) - 1, np.zeros((len(X), 2)) - 1
idx = 0
with torch.no_grad():
     for batch in data_loader_test:
         input_ids = batch["input_ids"]
         attention_mask = batch["attention_mask"]
         target_labels = batch["label"]
         methylations = batch["methylations"]
         logits, _ = clf_model(input_ids, attention_mask, methylations)
         predicted_labels = logits.argmax(dim=1)
         predicted_test[idx: idx + len(logits)] = logits.cpu().numpy()
         labels_test[idx: idx + len(target_labels)] = target_labels.cpu().numpy()
         idx += len(logits)
dataset_['pred'] = predicted_test[:,1]
dataset_.to_csv('regional_preds_df_nodataleakage.csv', sep='\t', index=False)




