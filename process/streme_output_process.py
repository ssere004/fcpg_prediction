import re

import sys
import os
project_root = os.path.dirname("/home/ssere004/fCpG_prediction/fcpg_prediction/")
sys.path.append(project_root)
import argparse

KO = 'DNMT3'
species = 'homo'


parser = argparse.ArgumentParser()
parser.add_argument('-KO', '--knockout', help='TET or DNMT3, QKO, PKO, mouse_3aKO, mouse_3bKO, mouse_SI_TET23_KO', required=True)
parser.add_argument('-sp', '--species', help='species: homo, mouse, mouse_si', required=False, default='BERT')

args = parser.parse_args()

KO = args.knockout
species = args.species

eval_threshold = 0.05


def parse_motif_file(filename):
    motifs = {}
    with open(filename, 'r') as file:
        lines = file.readlines()
    motif_name = None
    e_value = None
    matrix = []
    for line in lines:
        # Check for motif line and extract motif name
        if line.startswith('MOTIF') or line.startswith('***'):
            if motif_name and e_value and matrix:
                motifs[motif_name] = (e_value, matrix)
            if line.startswith('MOTIF'): motif_name = line.split()[1]
            matrix = []
            e_value = None
            print('motif', motif_name)
        elif 'E= ' in line and motif_name and e_value is None:
            e_value_match = re.search(r'E=\s*([0-9.e+-]+)', line)
            if e_value_match:
                e_value = e_value_match.group(1)
            print('eval', e_value)
        elif motif_name and e_value and line.strip() and not line.startswith('MOTIF'):
            matrix_row = [float(x) for x in line.strip().split()]
            matrix.append(matrix_row)
    if motif_name and e_value and matrix:
        motifs[motif_name] = (e_value, matrix)
    return motifs

def write_selected_motifs(out_file, s_mtfs):
    output = ''
    for mtf, (pval, mtrx) in s_mtfs:
        output += mtf + ' e_val = ' + str(pval) + '\n'
        for row in mtrx:
            output += ' '.join(str(item) for item in row) + '\n'
        output += '\n'
    with open(out_file, 'w') as outputfile:
        outputfile.write(output)

root = '/home/ssere004/fCpG_prediction/fcpg_prediction/dump_files/motifs/high_atten_regs'
root_ = '/home/ssere004/fCpG_prediction/fcpg_prediction/dump_files/motifs'

print('START .....')
print('STREME is running .....')
#subprocess.run([root + "/meme_run/streme", "--p", root + "/{}-{}_pos.fasta".format(species, KO), "--n",  root + "/{}-{}_neg.fasta".format(species, KO), "--verbosity", "1", "--minw", "6", "--maxw", "12", "--dna", "--nmotifs", "100", "--oc", root_ + "/streme/{}-{}_STREME/".format(species, KO)])
print('STREME DONE!')

all_motifs = parse_motif_file(root_ + "/streme/{}-{}_STREME/streme.txt".format(species, KO))

selected_motifs = sorted(all_motifs.items(), key=lambda item: float(item[1][0]))[:20]
#output looks like this:

#[('1-SCAGGGGG', ('2.3e-092', [[0.150036, 0.483705, 0.319377, 0.046882], [0.066303, 0.668232, 0.139906, 0.125559], [0.687428, 0.028326, 0.160576, 0.123671], [0.03953, 0.053963, 0.81769, 0.088817], [0.075377, 0.02303, 0.818515, 0.083078], [0.086858, 0.001939, 0.802426, 0.108777], [0.042549, 7e-06, 0.930522, 0.026922], [0.106742, 0.007693, 0.748987, 0.136578]])), ('2-CMWWWKG', ('4.8e-013', [[0.156707, 0.835245, 0.002632, 0.005415], [0.367616, 0.552846, 0.063304, 0.016234], [0.328941, 0.119498, 0.09814, 0.453421], [0.340999, 0.159001, 0.159001, 0.340999], [0.453421, 0.09814, 0.119498, 0.328941], [0.016234, 0.063304, 0.552846, 0.367616], [0.005415, 0.002632, 0.835245, 0.156707]])), ('3-AAAAGGG', ('7.7e-008', [[0.651538, 0.000112, 0.221171, 0.127179], [0.674129, 0.109476, 0.21354, 0.002854], [0.523511, 0.221928, 0.231879, 0.022683], [0.800177, 0.035796, 0.000112, 0.163915], [9.7e-05, 0.001451, 0.881715, 0.116737], [9.7e-05, 0.000112, 0.993738, 0.006053], [9.7e-05, 0.000112, 0.845259, 0.154532]]))]

write_selected_motifs(root_ + "/streme/{}-{}_STREME/streme_selected_20.txt".format(species, KO), selected_motifs)


print('SELECTED MOTIFS WROTE to file')
