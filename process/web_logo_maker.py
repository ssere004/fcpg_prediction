import logomaker
import numpy as np
import re
import pandas as pd
import matplotlib.pyplot as plt

def read_motif_file_to_dict(file_path):
    motifs = {}
    current_key = None
    current_values = []
    with open(file_path, 'r') as file:
        for line in file:
            if line.startswith('>'):
                # Save the previous key and values if they exist
                if current_key is not None:
                    motifs[current_key] = current_values
                    current_values = []
                current_key = line.strip()[1:]
            else:
                # Split the line into numbers and add to current values
                numbers = re.findall(r'\d+', line)
                row = [int(num) for num in numbers]
                current_values.append(row)
        # Don't forget to add the last key and values
        if current_key is not None:
            motifs[current_key] = current_values
    return motifs


arr = read_motif_file_to_dict('../motif_founded/jaspar_motif_pwm.txt')
for key in arr.keys():
    pwm_df = pd.DataFrame({'A': arr[key][0], 'C': arr[key][1], 'G': arr[key][2], 'T': arr[key][3]})
    logo = logomaker.Logo(pwm_df, figsize=(10,3))
    plt.savefig('../motif_founded/jaspar_weblogo/{}.png'.format(key), dpi=300, bbox_inches='tight')
    plt.close()
