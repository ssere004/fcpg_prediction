import sys
import os

project_root = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(project_root)


# import configs
import preprocess.data_reader as data_reader
# import preprocess.preprocess as preprocess
# import process.process as process
import torch.optim as optim
from tqdm import tqdm
import pandas as pd
import torch
from torch.utils.data import DataLoader
import torch.nn as nn
from sklearn.model_selection import train_test_split
import numpy as np
import argparse
from transformers import BertTokenizer, BertModel
import configs
from utils.model_manager import DNASequenceClassifier
from utils.data_manager import DNADataset
from utils import data_manager as dmngr
from transformers import AutoModel, AutoTokenizer, AutoModelForMaskedLM
from preprocess import preprocess
#from other_methods.hyeanaDNA import HyenaDNAPreTrainedModel, CharacterTokenizer

#python es_prediction/DNABERT_finetune.py -kmer 6 -KO TET -ws 512 -bs 16 -m BERT -sp homo -im True

kmer = 6
KO_alias = 'WT_13D-QKO_13D'
window_size = 512
model_type = 'BERT'
batch_size = 32
species = 'mouse'
include_methylation = 'none'
mc_num = 40
d_size = 100000

parser = argparse.ArgumentParser()
parser.add_argument('-dmrs', '--dmrs', help='address of dmrs where the file must have chr,start,meth_x,coverage_x,meth_y,coverage_y,p_value', required=True)
parser.add_argument('-ndmrs', '--ndmrs', help='address of not dmrs where the file must have chr,start,meth_x,coverage_x,meth_y,coverage_y,p_value', required=True)
parser.add_argument('-g', '--genome', help='address of genome assembly in fasta format', required=True)
parser.add_argument('-bs', '--batch_size', help='batch size', required=False, default=32)
parser.add_argument('-ds', '--data_size', help='data size', required=False, default=100000)
parser.add_argument('-s', '--save', help='Save the predicted labels and classifier model?', required=False, default=True)
args = parser.parse_args()


kmer = 6
KO_alias = 'case_sudy_ko'
window_size = 512
model_type = 'BERT'
species = 'species'
include_methylation = 'none'
do_save = True

genome_add = '/data/saleh/Organisms/mouse/assembely/mm9.fa'
dmrs_add = '/data/saleh/Organisms/ES/case_study4/WT_13D-QKO_13D_DMRs.csv'
ndmrs_add = '/data/saleh/Organisms/ES/case_study4/WT_13D-QKO_13D_notDMRs.csv'


batch_size = int(args.batch_size)
d_size = int(args.data_size)
do_save = bool(args.save)

genome_add = args.genome
dmrs_add, ndmrs_add = args.dmrs, args.ndmrs

filename = dmrs_add.split('/')[-1]
KO_alias = filename.split('-')[1].split('_DMRs')[0]
print(f'Now doing for this KO {KO_alias}')

num_classes = 2  # Binary classification


model_name = "zhihan1996/DNA_bert_"+str(kmer)
model_name_alias = 'brt'
model = BertModel.from_pretrained(model_name, num_labels=2, finetuning_task="dnaprom", cache_dir=None)
tokenizer = BertTokenizer.from_pretrained(model_name, trust_remote_code=True)
meth_ws = {'none': 0, 'wt': window_size, 'ko': window_size, 'wt-ko': 2*window_size}
clf_model = DNASequenceClassifier(model, None, num_classes, meth_window_size=meth_ws[include_methylation])

assembly = data_reader.readfasta(genome_add)
dmrs, not_dmrs = pd.read_csv(dmrs_add), pd.read_csv(ndmrs_add)

print(f"DMRS and Not DMRS are loaded {len(dmrs)} and {len(not_dmrs)}")

if 'position' not in dmrs.columns: dmrs.rename(columns={'start': 'position'}, inplace=True)


if 'position' not in not_dmrs.columns: not_dmrs.rename(columns={'start': 'position'}, inplace=True)


print([assembly[dmrs.iloc[i]['chr']][dmrs.iloc[i]['position']] for i in range(200)])

meth_seq_lst = None
print('no methylation included')

print('The data loaded for {}. dmrs size {}, not_dmrs size {}'.format(KO_alias, str(len(dmrs)), str(len(not_dmrs))))

sample_size = min(d_size, len(dmrs)*2, len(not_dmrs)*2)
dataset = pd.concat([dmngr.make_input_dataset(assembly, dmrs.sample(int(sample_size//2)), 1, window_size=window_size, meth_seq_lst=meth_seq_lst, mc_num=20),
                     dmngr.make_input_dataset(assembly, not_dmrs.sample(int(sample_size//2)), 0, window_size=window_size, meth_seq_lst=meth_seq_lst, mc_num=20)])
#dataset_ = pd.read_csv('./dump_files/sample_dataset.csv', header=None, index_col=None, names=['seq', 'meth', 'label'])
dataset = dataset.sample(frac=1).reset_index(drop=True)
dataset = dataset[dataset['seq'].str.len() == window_size]

X = dataset.drop(columns=['label'])
#X is a sequence where the position window_size/2 is 'C' (positioning starts from zero)
Y = np.asarray(pd.cut(dataset['label'], bins=2, labels=[0, 1], right=False))
b = np.zeros((Y.size, Y.max()+1))
b[np.arange(Y.size), Y] = 1
Y = b

x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=0.1, random_state=None)

# Set batch size and max sequence length
labels = torch.tensor(y_train, dtype=torch.float32)
dataset = DNADataset(x_train, labels, tokenizer, window_size, kmer)
data_loader_train = DataLoader(dataset, batch_size=batch_size, shuffle=True)

labels = torch.tensor(y_test, dtype=torch.float32)
dataset = DNADataset(x_test, labels, tokenizer, window_size, kmer)
data_loader_test = DataLoader(dataset, batch_size=batch_size, shuffle=True)

optimizer = optim.Adam(clf_model.parameters(), lr=1e-5)
criterion = nn.BCELoss()
clf_model.to('cuda')
# Set the number of epochs
num_epochs = 5
total_samples = len(data_loader_train.dataset)  # Total number of samples
batch_size = data_loader_train.batch_size  # Batch size
batch_size_test = data_loader_test.batch_size
total_samples_test = len(data_loader_test.dataset)
# Training loop
for epoch in range(num_epochs):
    clf_model.train()
    total_loss = 0
    total_correct = 0
    total_processed = 0  # To keep track of total processed samples
    progress_bar = tqdm(data_loader_train, desc=f"Epoch {epoch+1}/{num_epochs}", leave=False)
    for batch in progress_bar:
        input_ids = batch["input_ids"].to('cuda')
        attention_mask = batch["attention_mask"].to('cuda')
        target_labels = batch["label"].to('cuda').to(torch.float32)
        methylations = batch["methylations"].to('cuda')
        optimizer.zero_grad()
        logits, _ = clf_model(input_ids, attention_mask, methylations=methylations)
        loss = criterion(logits, target_labels)
        loss.backward()
        optimizer.step()
        predictions = logits.argmax(dim=1)
        total_correct += (predictions == target_labels.argmax(dim=1)).sum().item()
        total_processed += batch_size
        if total_processed % (10*batch_size) == 0:
            b_acc = total_correct / total_processed
            progress_percentage = (total_processed / total_samples) * 100
            progress_bar.set_postfix(loss=loss.item(), batch_acc=b_acc, progress=f"{progress_percentage:.2f}%")
        total_loss += loss.item()
    epoch_accuracy = total_correct / total_samples
    average_loss = total_loss / len(data_loader_train)
    print(f"Epoch [{epoch+1}/{num_epochs}] - Average Loss: {average_loss:.4f}, Epoch Accuracy: {epoch_accuracy:.4f}")
    # Evaluate the model on test data
clf_model.eval()
test_correct = 0
#keep track of the predicted and labels to draw the ROC curve
predicted_test, labels_test = np.zeros((total_samples_test, 2)) - 1, np.zeros((total_samples_test, 2)) - 1
idx = 0
with torch.no_grad():
    for batch in data_loader_test:
        input_ids = batch["input_ids"].to('cuda')
        attention_mask = batch["attention_mask"].to('cuda')
        target_labels = batch["label"].to('cuda')
        methylations = batch["methylations"].to('cuda')
        logits, _ = clf_model(input_ids, attention_mask, methylations)
        predicted_labels = logits.argmax(dim=1)
        test_correct += (predicted_labels == target_labels.argmax(dim=1)).sum().item()
        predicted_test[idx: idx + len(logits)] = logits.cpu().numpy()
        labels_test[idx: idx + len(target_labels)] = target_labels.cpu().numpy()
        idx += len(logits)

assert -1 not in predicted_test and -1 not in labels_test
if not os.path.exists("/home/ssere004/fCpG_prediction/fcpg_prediction/dump_files/pred_results/case_sudy/WT_mES"): os.makedirs("/home/ssere004/fCpG_prediction/fcpg_prediction/dump_files/pred_results/case_sudy/WT_mES")
if do_save:
    np.save("/home/ssere004/fCpG_prediction/fcpg_prediction/dump_files/pred_results/case_sudy/WT_mES/predicted_{}_{}_{}_{}_include_meth_{}.npy".format(str(KO_alias), str(kmer), str(window_size), model_name_alias, str(include_methylation)), predicted_test)
    np.save("/home/ssere004/fCpG_prediction/fcpg_prediction/dump_files/pred_results/case_sudy/WT_mES/labels_{}_{}_{}_{}_include_meth_{}.npy".format(str(KO_alias), str(kmer), str(window_size), model_name_alias, str(include_methylation)), labels_test)
test_accuracy = test_correct / total_samples_test
print('%s\t%s\t%s\t%s\t%s\t%s\t%s' %(str(kmer), str(window_size), str(KO_alias), str(test_accuracy), str(model_type), str(include_methylation), str(d_size)))
with open("/home/ssere004/fCpG_prediction/fcpg_prediction/dump_files/pred_results/case_sudy/WT_mES/kmer_test_BERT_classification_results.txt", "a") as file_object:
    file_object.write('%s\t%s\t%s\t%s\t%s\t%s\t%s' %(str(kmer), str(window_size), str(KO_alias), str(test_accuracy), str(model_type), str(include_methylation), str(sample_size)))
    file_object.write("\n")

# if do_save:
#     clf_model.save("./dump_files/pretrained_models/reviewer_feedback/" + "kmer_"+str(kmer) + "_ws_" + str(window_size) + "_KO_" + str(KO_alias) + "_model_" + str(model_type) + '_methyl' + str(include_methylation))
