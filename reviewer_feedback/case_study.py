import process.process as process
import re
import pandas as pd
import sys
import os
project_root = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(project_root)
import preprocess.data_reader as data_reader
from transformers import BertTokenizer, BertModel
import configs
import utils.model_manager as mdlmngr

import sys
import os
project_root = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(project_root)
import preprocess.data_reader as data_reader
import pandas as pd
import torch
from torch.utils.data import Dataset, DataLoader
import numpy as np
import argparse
from transformers import BertTokenizer, BertModel
import configs
import utils.model_manager as mdlmngr
from utils.data_manager import DNADataset
import utils.data_manager as dmngr
import process.process as process


def make_DMCs():
    data_root_add = '/data/saleh/Organisms/ES/case_study3/'
    files = os.listdir(data_root_add)
    WTs = [f for f in files if 'WT' in f and 'GS' in f]
    DKOs = [f for f in files if 'KO' in f and 'GS' in f]
    DKOs, WTs = ['GSE100956%5F3aKO%5FWGBS%5Fmcall%5Fmerge.G.bed', 'GSE100956%5F3bKO%5FWGBS%5Fmcall%5Fmerge.G.bed'], ['GSE100956%5FWT%5FWGBS%5Fmcall%5Fmerge.G.bed']
    DKOs, WTs = ['dnmt3b_ko_b126.bed', 'dnmt3b_ko_b77.bed'], ['wt_rep1.bed', 'wt_rep2.bed']
    aliases = {'GSE100956%5F3aKO%5FWGBS%5Fmcall%5Fmerge.G.bed': '3aKO', 'GSE100956%5F3bKO%5FWGBS%5Fmcall%5Fmerge.G.bed': '3bKO','GSE100956%5FWT%5FWGBS%5Fmcall%5Fmerge.G.bed': 'WT'}
    aliases = {'dnmt3b_ko_b126.bed': '3bKO126', 'dnmt3b_ko_b77.bed': '3bKO77', 'wt_rep1.bed': 'WTrep1', 'wt_rep2.bed': 'WTrep2'}
    for DKO in DKOs:
        for WT in WTs:
            df1, df2 = read_case_study3_dfs(data_root_add+WT), read_case_study3_dfs(data_root_add+DKO)
            #dko_alias, wt_alias= re.search(r'%5(.*?)(?=\.)', DKO).group(1), re.search(r'%5(.*?)(?=\.)', WT).group(1)
            dko_alias, wt_alias = aliases[DKO], aliases[WT]
            dmrs, not_dmrs = process.find_DMRs(df1, df2, 10, 0.6)
            dmrs.to_csv(f'{data_root_add}{wt_alias}-{dko_alias}_DMRs.csv', index=False)
            not_dmrs.to_csv(f'{data_root_add}{wt_alias}-{dko_alias}_notDMRs.csv', index=False)

make_DMCs()


file_list = ["QKO_13D.txt", "QKO_Dnmt3c.txt", "QKO_mES.txt", "RQKO_Dnmt3b_OE.txt", "RQKO_mES.txt", "SKO_Dnmt1.txt", "SKO_Dnmt3c_Dnmt1.txt", "SKO_mES.txt",
    "TKO_13D.txt", "TKO_mES.txt"]

Wts = ["WT_13D.txt", "WT_mES.txt"]



def make_DMCs():
    data_root_add = '/data/saleh/Organisms/ES/case_study4/'
    files = os.listdir(data_root_add)
    WTs = ["WT_13D.txt", "WT_mES.txt"]
    DKOs = ["QKO_13D.txt", "QKO_Dnmt3c.txt", "QKO_mES.txt", "RQKO_Dnmt3b_OE.txt", "RQKO_mES.txt", "SKO_Dnmt1.txt", "SKO_Dnmt3c_Dnmt1.txt", "SKO_mES.txt",
    "TKO_13D.txt", "TKO_mES.txt"]
    for DKO in DKOs:
        for WT in WTs:
            df1, df2 = read_case_study4_dfs(data_root_add+WT), read_case_study4_dfs(data_root_add+DKO)
            #dko_alias, wt_alias= re.search(r'%5(.*?)(?=\.)', DKO).group(1), re.search(r'%5(.*?)(?=\.)', WT).group(1)
            dko_alias, wt_alias = DKO[:-4], WT[:-4]
            dmrs, not_dmrs = process.find_DMRs(df1, df2, 10, 0.6)
            dmrs.to_csv(f'{data_root_add}{wt_alias}-{dko_alias}_DMRs.csv', index=False)
            not_dmrs.to_csv(f'{data_root_add}{wt_alias}-{dko_alias}_notDMRs.csv', index=False)

make_DMCs()






kmer = 6
test_KO = 'DKO-reviewer_feedback'
train_KO = 'mouse_3bKO'
train_KO = 'mouse_SI_TET23_KO'
window_size = 512
model_type = 'BERT'
species = 'mouse'



model_name = "./dump_files/pretrained_models/" + "kmer_"+str(kmer) + "_ws_" + str(window_size) + "_KO_" + str(train_KO) + "_model_" + str(model_type)+ '_methyl' + str('none')
clf_model = mdlmngr.load_clf_model(model_name)
#clf_model = mdlmngr.load_clf_model("./dump_files/pretrained_models_/" + "kmer_"+str(kmer) + "_ws_" + str(window_size) + "_KO_" + str(train_KO))
tokenizer = BertTokenizer.from_pretrained("zhihan1996/DNA_bert_"+str(kmer), trust_remote_code=True)
print('model is loaded....', model_name)
cnfg_dic = {'homo': configs.hg19, 'mouse': configs.mouse, 'mouse_si': configs.mouse_si}
cnfg = cnfg_dic[species]
assembly = data_reader.readfasta(cnfg['assembly'])

df = pd.DataFrame()

wt_aliases = ["WT_13D", "WT_mES"]
dko_aliases = ["QKO_13D", "QKO_Dnmt3c", "QKO_mES", "RQKO_Dnmt3b_OE", "RQKO_mES", "SKO_Dnmt1", "SKO_Dnmt3c_Dnmt1", "SKO_mES","TKO_13D", "TKO_mES"]

for wt_alias in wt_aliases:
    for dko_alias in dko_aliases:
        #wt_alias = wt_aliases[0]
        #dko_alias = dko_aliases[0]
        #dmrs = pd.read_csv(f'/data/saleh/Organisms/ES/reviewer_feedback/FWT%5F{wt_alias}-FDKO%5F{dko_alias}_DMRs.csv')
        dmrs = pd.read_csv(f'/data/saleh/Organisms/ES/case_study4/{wt_alias}-{dko_alias}_DMRs.csv')
        dmrs.columns = ['chr', 'position', 'meth1', 'coverage1', 'meth2', 'coverage2', 'pval']
        #not_dmrs = pd.read_csv(f'/data/saleh/Organisms/ES/reviewer_feedback/FWT%5F{wt_alias}-FDKO%5F{dko_alias}_notDMRs.csv')
        not_dmrs = pd.read_csv(f'/data/saleh/Organisms/ES/case_study4/{wt_alias}-{dko_alias}_notDMRs.csv')
        not_dmrs.columns = ['chr', 'position', 'meth1', 'coverage1', 'meth2', 'coverage2']
        sample_size = min(20000, len(dmrs)*2, len(not_dmrs)*2)
        dataset = pd.concat([dmngr.make_input_dataset(assembly, dmrs.sample(int(sample_size//2)), 1, window_size=window_size),
                             dmngr.make_input_dataset(assembly, not_dmrs.sample(int(sample_size//2)), 0, window_size=window_size)])
        X = dataset.drop(columns=['label'])
        Y = np.asarray(pd.cut(dataset['label'], bins=2, labels=[0, 1], right=False))
        b = np.zeros((Y.size, Y.max()+1))
        b[np.arange(Y.size), Y] = 1
        Y = b
        batch_size = 32
        labels = torch.tensor(Y, dtype=torch.float32)
        dataset = DNADataset(X, labels, tokenizer, window_size, kmer)
        data_loader_test = DataLoader(dataset, batch_size=batch_size, shuffle=True)
        total_samples_test = len(data_loader_test.dataset)
        clf_model.to('cuda')
        clf_model.eval()
        test_correct = 0
        #keep track of the predicted and labels to draw the ROC curve
        predicted_test, labels_test = np.zeros((total_samples_test, 2)) - 1, np.zeros((total_samples_test, 2)) - 1
        idx = 0
        with torch.no_grad():
            for batch in data_loader_test:
                input_ids = batch["input_ids"].to('cuda')
                attention_mask = batch["attention_mask"].to('cuda')
                target_labels = batch["label"].to('cuda')
                methylations = batch["methylations"].to('cuda')
                input_ids.requires_grad = False
                attention_mask.requires_grad = False
                target_labels.requires_grad = False
                logits, _ = clf_model(input_ids, attention_mask, methylations=methylations)
                predicted_labels = logits.argmax(dim=1)
                test_correct += (predicted_labels == target_labels.argmax(dim=1)).sum().item()
                predicted_test[idx: idx + len(logits)] = logits.cpu().numpy()
                labels_test[idx: idx + len(target_labels)] = target_labels.cpu().numpy()
                idx += len(logits)
        assert -1 not in predicted_test and -1 not in labels_test
        test_accuracy = test_correct / total_samples_test
        with open('/data/saleh/Organisms/ES/case_study4/test_accuracy.txt', 'a') as f:
            f.write(str(test_accuracy) + wt_alias + dko_alias + '\n')
