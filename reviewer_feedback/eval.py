import numpy as np
from sklearn.metrics import precision_score, recall_score, f1_score
import pandas as pd
import re
import os

files = os.listdir()

ko_pattern = re.compile(r"labels_(.*?)_6_512_brt_include_meth_none.npy")
kos = [ko_pattern.search(file).group(1) for file in files if ko_pattern.search(file)]


# kos = ['SKO_mES', 'WT_13D-QKO_13D', 'RQKO_Dnmt3b_OE', 'TKO_13D', 'QKO_mES', 'QKO_Dnmt3c', 'RQKO_mES', 'TKO_mES', 'SKO_Dnmt3c_Dnmt1', 'SKO_Dnmt1']

def acc_calc(a, b):
    predicted_labels = np.argmax(b, axis=1)
    true_labels = np.argmax(a, axis=1)
    accuracy = np.mean(predicted_labels == true_labels)
    precision = precision_score(true_labels, predicted_labels)
    recall = recall_score(true_labels, predicted_labels)
    f1 = f1_score(true_labels, predicted_labels)
    return accuracy, precision, recall, f1

res = []
for ko in kos:
    a = np.load(f'labels_{ko}_6_512_brt_include_meth_none.npy')
    b = np.load(f'predicted_{ko}_6_512_brt_include_meth_none.npy')
    accuracy, precision, recall, f1 = acc_calc(a, b)
    res.append([ko, accuracy, precision, recall, f1])

res_df = pd.DataFrame(res)
res_df.columns = ['KO', 'accuracy', 'precision', 'recall', 'f1']
res_df.to_csv('eval_metrics.csv', index=False)
