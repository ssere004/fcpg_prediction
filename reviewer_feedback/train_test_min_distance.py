import pandas as pd
from sklearn.model_selection import train_test_split
import numpy as np

######################################
# get the dmrs and not_dmrs by running the first parts of DNABERT_findtune module, then go to this code

import sys
import os

project_root = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(project_root)


# import configs
import preprocess.data_reader as data_reader
# import preprocess.preprocess as preprocess
# import process.process as process
import torch.optim as optim
from tqdm import tqdm
import pandas as pd
import torch
from torch.utils.data import DataLoader
import torch.nn as nn
from sklearn.model_selection import train_test_split
import numpy as np
import argparse
from transformers import BertTokenizer, BertModel
import configs
from utils.model_manager import DNASequenceClassifier
from utils.data_manager import DNADataset
from utils import data_manager as dmngr
from transformers import AutoModel, AutoTokenizer, AutoModelForMaskedLM
from preprocess import preprocess
#from other_methods.hyeanaDNA import HyenaDNAPreTrainedModel, CharacterTokenizer

#python es_prediction/DNABERT_finetune.py -kmer 6 -KO TET -ws 512 -bs 16 -m BERT -sp homo -im True

KO = 'TET'

parser = argparse.ArgumentParser()
parser.add_argument('-KO', '--knockout', help='TET, DNMT3, QKO, PKO, mouse_3aKO, mouse_3bKO, mouse_SI_TET23_KO', required=True)
args = parser.parse_args()
KO = args.knockout

d_size = 100000

def find_closest_distances(x_train, x_test):
    def closest_distance(test_row):
        matching_train_rows = x_train[x_train['chr'] == test_row['chr']]
        if matching_train_rows.empty:
            return np.nan
        position_diffs = np.abs(matching_train_rows['position'] - test_row['position'])
        return position_diffs.min()
    x_test['closest_distance'] = x_test.apply(closest_distance, axis=1)
    return x_test


for KO in ['TET', 'DNMT3', 'QKO', 'PKO', 'mouse_3aKO', 'mouse_3bKO', 'mouse_SI_TET23_KO']:
    dmrs, not_dmrs = dmngr.load_df(KO)
    sample_size = min(d_size, len(dmrs)*2, len(not_dmrs)*2)
    dmrs = dmrs.sample(int(sample_size//2))
    not_dmrs = not_dmrs.sample(int(sample_size//2))
    dmrs['label'] = 1
    not_dmrs['label'] = 0
    ds = pd.concat([dmrs, not_dmrs], axis=0).sample(frac=1)
    X = ds[['chr', 'position']]
    Y = ds['label']
    x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=0.1, random_state=None)
    np.save(f'/home/ssere004/fCpG_prediction/fcpg_prediction/reviewer_feedback/{KO}_closest_dist.npy', find_closest_distances(x_train, x_test)['closest_distance'].to_numpy())
    #print(f'{KO} {mean_dis}')


#################################################################

#plotting:

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import os

# Directory and filenames
root = "/Users/salehsereshki/Desktop/Data/UCRRA/projects/TET-DNMT/tr_te_closest_dists/"
ko_list = ['TET', 'DNMT3', 'QKO', 'PKO', 'mouse_3aKO', 'mouse_3bKO', 'mouse_SI_TET23_KO']
x_labels = ['TETKO', 'DNMT3KO', 'PKO', 'QKO', 'DNMT3aKO', 'DNMT3bKO', 'TET2/3KO']

# Colors
colors = ['#1f77b4', '#1f77b4', '#1f77b4', '#1f77b4', '#ff7f0e', '#ff7f0e', '#2ca02c']
legend_colors = ['#1f77b4', '#ff7f0e', '#2ca02c']
legend_labels = ['Human ESC', 'Mouse ESC', 'Mouse ISC']

# Load the data from npy files
data = []
for ko in ko_list:
    file_path = os.path.join(root, f"{ko}_closest_dist.npy")
    dist = np.load(file_path)
    data.append(dist)

# Create the violin plot
plt.figure(figsize=(12, 6))
sns.violinplot(data=data, palette=colors)

# Set X-axis labels
plt.xticks(ticks=range(len(x_labels)), labels=x_labels)

# Create a legend
for color, label in zip(legend_colors, legend_labels):
    plt.plot([], [], color=color, label=label, linewidth=8)  # empty plot for legend entry
plt.legend(loc='upper right')

# Label the axes
plt.xlabel('Knockout Type')
plt.ylabel('Closest Distance')
#plt.ylim([0, 10000])

# Show plot
plt.tight_layout()
plt.show()



##############################################################

import sys
sys.path.append(project_root)

import preprocess.data_reader as data_reader
import pandas as pd
from sklearn.model_selection import train_test_split
import numpy as np
import configs
from utils import data_manager as dmngr

kmer = 6
window_size = 512
model_type = 'BERT'
batch_size = 32
include_methylation = 'none'
mc_num = 40
d_size = 100000

def find_closest_distances(x_train, x_test):
    def closest_distance(test_row):
        matching_train_rows = x_train[x_train['chr'] == test_row['chr']]
        if matching_train_rows.empty:
            return np.nan
        position_diffs = np.abs(matching_train_rows['position'] - test_row['position'])
        return position_diffs.min()
    x_test['closest_distance'] = x_test.apply(closest_distance, axis=1)
    return x_test

root_save = '/home/ssere004/fCpG_prediction/fcpg_prediction/reviewer_feedback/closest_dist/'
meth_seq_lst = None
spe_Ko_dic = {'homo': ['DNMT3', 'TET', 'QKO', 'PKO'], 'mouse': ['mouse_3aKO', 'mouse_3bKO'], 'mouse_si': ['mouse_SI_TET23_KO']}
for species in spe_Ko_dic.keys():
    for KO in spe_Ko_dic[species]:
        cnfg_dic = {'homo': configs.hg19, 'mouse': configs.mouse, 'mouse_si': configs.mouse_si}
        cnfg = cnfg_dic[species]
        assembly = data_reader.readfasta(cnfg['assembly'])
        dmrs, not_dmrs = dmngr.load_df(KO)
        sample_size = min(d_size, len(dmrs)*2, len(not_dmrs)*2)
        dataset = pd.concat([dmngr.make_input_dataset(assembly, dmrs.sample(int(sample_size//2)), 1, window_size=window_size, meth_seq_lst=meth_seq_lst, mc_num=20),
                             dmngr.make_input_dataset(assembly, not_dmrs.sample(int(sample_size//2)), 0, window_size=window_size, meth_seq_lst=meth_seq_lst, mc_num=20)])
        #dataset_ = pd.read_csv('./dump_files/sample_dataset.csv', header=None, index_col=None, names=['seq', 'meth', 'label'])
        dataset = dataset.sample(frac=1).reset_index(drop=True)
        dataset = dataset[dataset['seq'].str.len() == window_size]
        X = dataset.drop(columns=['label'])
        Y = np.asarray(pd.cut(dataset['label'], bins=2, labels=[0, 1], right=False))
        b = np.zeros((Y.size, Y.max()+1))
        b[np.arange(Y.size), Y] = 1
        Y = b
        x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=0.1, random_state=None)
        x_test = find_closest_distances(x_train, x_test)
        x_test['label'] = y_test[:, 1]
        x_test = x_test[['chr', 'position', 'closest_distance', 'label']]
        x_test.to_csv(f'{root_save}{KO}_closest_dists.csv', index=False)
        print(f'{KO} saved')


################################################################################

root = "/Users/salehsereshki/Desktop/Data/UCRRA/projects/TET-DNMT/tr_te_closest_dists/"
ko_list = ['TET', 'DNMT3', 'QKO', 'PKO', 'mouse_3aKO', 'mouse_3bKO', 'mouse_SI_TET23_KO']
for KO in ko_list:
    df = pd.read_csv(f'{root}{KO}_closest_dists.csv')
    a = (len(df[(df.label == 1) & (df.closest_distance < 256)]) / len(df[df.label==1]))
    b = (len(df[(df.label == 0) & (df.closest_distance < 256)]) / len(df[df.label==0]))
    print(f'{KO},{a},{b}')


root = "/Users/salehsereshki/Desktop/Data/UCRRA/projects/TET-DNMT/tr_te_closest_dists/"
ko_list = ['TET', 'DNMT3', 'QKO', 'PKO', 'mouse_3aKO', 'mouse_3bKO', 'mouse_SI_TET23_KO']
distances = []
for KO in ko_list:
    df = pd.read_csv(f'{root}{KO}_closest_dists.csv')
    closest_dist = df[(df.label == 1) & (df.closest_distance < 256)]['closest_distance']
    distances.append(closest_dist)

#Make a figure with 7 rows each represent a histogram of a closest_dists list in a. I want the y axis of all of them has the same range.




import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

# Define paths and KO list
root = "/Users/salehsereshki/Desktop/Data/UCRRA/projects/TET-DNMT/tr_te_closest_dists/"
ko_list = ['TET', 'DNMT3', 'QKO', 'PKO', 'mouse_3aKO', 'mouse_3bKO', 'mouse_SI_TET23_KO']
distances = []

# Read data and collect distances
for KO in ko_list:
    df = pd.read_csv(f'{root}{KO}_closest_dists.csv')
    closest_dist = df[(df.label == 1) & (df.closest_distance < 256)]['closest_distance']
    distances.append(closest_dist)

# Create subplots
fig, axes = plt.subplots(nrows=7, ncols=1, figsize=(10, 20), sharey=True)
max_y = 0

# Create bin edges with a size of 10
bin_edges = np.arange(0, 257, 10)  # Creates bins from 0 to 256 with steps of 10

# Plot histograms and determine the max y-axis range
for i, ax in enumerate(axes):
    ax.hist(distances[i], bins=bin_edges, alpha=0.7, color='skyblue')
    ax.set_title(ko_list[i])
    ax.set_xlim(0, 256)
    y_max = ax.get_ylim()[1]
    if y_max > max_y:
        max_y = y_max

# Set the same y-axis range for all subplots
for ax in axes:
    ax.set_ylim(0, max_y)
    ax.set_ylabel('Count')

# Add labels and adjust layout
plt.xlabel('Distance')
plt.tight_layout()
plt.show()



