
kmers = [6]
spe_Ko_dic = {'homo': ['DNMT3', 'TET', 'QKO', 'PKO'], 'mouse': ['mouse_3aKO', 'mouse_3bKO'], 'mouse_si': ['mouse_SI_TET23_KO']}
KOs_homo = ['DNMT3', 'TET', 'QKO', 'PKO']
KOs_mouse = ['mouse_3aKO', 'mouse_3bKO']
KOs_mouse_si = ['mouse_SI_TET23_KO']
KOs = ['TET']
window_sizes = [512]
species = 'mouse'

for species in spe_Ko_dic.keys():
    KOs = spe_Ko_dic[species]
    for ko in KOs:
        print(f'python es_prediction/DNABERT_RF.py -kmer 6 -KO {ko} -ws 512 -bs 32 -m BERT -sp {species}')

for species in spe_Ko_dic.keys():
    KOs = spe_Ko_dic[species]
    for KO in ['DNMT3']:
        for d_size in [10000, 20000, 40000, 80000, 160000, 320000]:
        #print('python es_prediction/DNABERT_finetune.py -kmer 6 -KO {} -ws 512 -bs 32 -m BERT -sp {} -im ko'.format(KO, species))
        #print('python es_prediction/DNABERT_finetune.py -kmer 6 -KO {} -ws 512 -bs 32 -m BERT -sp {} -im wt'.format(KO, species))
        #print('python es_prediction/DNABERT_finetune.py -kmer 6 -KO {} -ws 512 -bs 32 -m BERT -sp {} -im wt-ko'.format(KO, species))
            print('python es_prediction/DNABERT_finetune.py -kmer 6 -KO {} -ws 512 -bs 32 -m BERT -sp {} -im none -ds {}'.format(KO, species, d_size))
        #print('python es_prediction/dmr_prediction_torch.py -KO {} -sp {} -im ko'.format(KO, species))

for species in spe_Ko_dic.keys():
    KOs = spe_Ko_dic[species]
    for KO in KOs:
        #for m_ws in [8, 10, 12]:
            #print('python es_prediction/DNABERT_finetune.py -kmer 6 -KO {} -ws 512 -bs 32 -m BERT -sp {} -im True'.format(KO, species))
            #print('python es_prediction/motif_utils.py -KO {} -sp {} -ws 512 -kmer 6'.format(KO, species))
        #print('python es_prediction/motif_utils.py -KO {} -sp {} -ws 512 -kmer 6 -m_ws {} -m_ml {}'.format(KO, species, 8, 6))
        print('python process/make_weblogo.py -KO {} -sp {}'.format(KO, species))

for species in spe_Ko_dic.keys():
    KOs = spe_Ko_dic[species]
    for KO in KOs:
        print('python process/convert_seq_to_fasta.py -KO {} -sp {}'.format(KO, species))

for species in spe_Ko_dic.keys():
    KOs = spe_Ko_dic[species]
    for KO in KOs:
        print('python es_prediction/motif_finding_feed_gen.py -KO {} -sp {} -ws 512 -kmer 6'.format(KO, species))


for ko in KOs_homo:
    print('python es_prediction/dmr_prediction_torch.py -KO %s -sp %s -im none' % (ko, 'homo'))
for ko in KOs_mouse:
    print('python es_prediction/dmr_prediction_torch.py -KO %s -sp %s -im none' % (ko, 'mouse'))
for ko in KOs_mouse_si:
    print('python es_prediction/dmr_prediction_torch.py -KO %s -sp %s -im none' % (ko, 'mouse_si'))

co_KOs = ['DNMT3', 'mouse_3aKO', 'mouse_3bKO']
co_KOs = ['TET', 'mouse_SI_TET23_KO']
for te_ko in co_KOs:
    for tr_ko in co_KOs:
        if tr_ko !=te_ko:
            sp = 'homo'
            if te_ko in KOs_mouse:
                sp = 'mouse'
            elif te_ko in KOs_mouse_si:
                sp = 'mouse_si'
            print('python process/cross_ko.py --kmer 6 --window_size 512 -test_KO {} -train_KO {} -m BERT -tsp {}'.format(te_ko, tr_ko, sp))

for k in kmers:
    for window_size in window_sizes:
        for ko in KOs:
            print('python es_prediction/DNABERT_finetune.py -kmer %s -KO %s -ws %s -sp %s' % (k, ko, window_size, species))

ds = 100000

KOs = ['homo-DNMT3', 'homo-PKO', 'homo-QKO', 'homo-TET', 'mouse-mouse_3aKO', 'mouse-mouse_3bKO', 'mouse_si-mouse_SI_TET23_KO']
ws_sizes = ['ws_8', 'ws_10', 'ws_12']

for ws in ws_sizes:
    for KO in KOs:
        add = '/home/ssere004/fCpG_prediction/fcpg_prediction/dump_files/motifs/{}/{}/'.format(ws, KO)
        print('cp {}pwms_dic.pkl ./'.format(add))
        print('mv pwms_dic.pkl pwms_{}_{}.pkl'.format(ws, KO))


#CUDA_VISIBLE_DEVICES=1

# source /home/ssere004/anaconda3/bin/activate torch_env
# python es_prediction/DNABERT_finetune.py -kmer 6 -KO TET -ws 512 -bs 8 -m nucleotide-transformer
# source /home/ssere004/anaconda3/bin/activate base
# source /home/ssere004/anaconda3/bin/activate tf_env
# python es_prediction/DNABERT_finetune.py -kmer 6 -KO TET -ws 512 -bs 8 -m BigBird
# source /home/ssere004/anaconda3/bin/activate base
# source /home/ssere004/anaconda3/bin/activate brt_env
# python es_prediction/DNABERT_finetune.py -kmer 6 -KO TET -ws 512 -bs 32 -m BERT2
# source /home/ssere004/anaconda3/bin/activate base


# python es_prediction/dmr_prediction.py -KO DNMT3 -sp homo
# python es_prediction/dmr_prediction.py -KO TET -sp homo
# python es_prediction/dmr_prediction.py -KO QKO -sp homo
# python es_prediction/dmr_prediction.py -KO PKO -sp homo
# python es_prediction/dmr_prediction.py -KO mouse_3aKO -sp mouse
# python es_prediction/dmr_prediction.py -KO mouse_3bKO -sp mouse
# python es_prediction/dmr_prediction.py -KO mouse_SI_TET23_KO -sp mouse

#python es_prediction/dmr_prediction_torch.py
#python es_prediction/dmr_prediction_torch.py -KO mouse_3aKO -sp mouse
#python es_prediction/dmr_prediction_torch.py -KO mouse_3bKO -sp mouse
#python es_prediction/dmr_prediction_torch.py -KO mouse_SI_TET23_KO -sp mouse
#python es_prediction/DNABERT_finetune.py -kmer 6 -KO TET -ws 512 -bs 32 -m BERT2
#python es_prediction/DNABERT_finetune.py -kmer 6 -KO mouse_3aKO -ws 512 -bs 32 -m BERT2 -sp mouse

#python es_prediction/DNABERT_finetune.py -kmer 6 -KO mouse_3aKO -ws 512 -bs 2 -m nucleotide-transformer -sp mouse
#python es_prediction/DNABERT_finetune.py -kmer 6 -KO mouse_3bKO -ws 512 -bs 2 -m nucleotide-transformer -sp mouse
#python es_prediction/DNABERT_finetune.py -kmer 6 -KO mouse_SI_TET23_KO -ws 512 -bs 2 -m nucleotide-transformer -sp mouse

# python es_prediction/DNABERT_finetune.py -kmer 6 -KO QKO -ws 512 -bs 32 -m BERT -sp homo -im ko
# python es_prediction/DNABERT_finetune.py -kmer 6 -KO PKO -ws 512 -bs 32 -m BERT -sp homo -im ko
# python es_prediction/DNABERT_finetune.py -kmer 6 -KO mouse_3aKO -ws 512 -bs 32 -m BERT -sp mouse -im ko
# python es_prediction/DNABERT_finetune.py -kmer 6 -KO mouse_3bKO -ws 512 -bs 32 -m BERT -sp mouse -im ko
# python es_prediction/DNABERT_finetune.py -kmer 6 -KO mouse_SI_TET23_KO -ws 512 -bs 32 -m BERT -sp mouse_si -im ko
# python es_prediction/DNABERT_finetune.py -kmer 6 -KO DNMT3 -ws 512 -bs 32 -m BERT -sp homo -im none
# python es_prediction/DNABERT_finetune.py -kmer 6 -KO TET -ws 512 -bs 32 -m BERT -sp homo -im none
# python es_prediction/DNABERT_finetune.py -kmer 6 -KO QKO -ws 512 -bs 32 -m BERT -sp homo -im none
# python es_prediction/DNABERT_finetune.py -kmer 6 -KO PKO -ws 512 -bs 32 -m BERT -sp homo -im none
# python es_prediction/DNABERT_finetune.py -kmer 6 -KO mouse_3aKO -ws 512 -bs 32 -m BERT -sp mouse -im none
# python es_prediction/DNABERT_finetune.py -kmer 6 -KO mouse_3bKO -ws 512 -bs 32 -m BERT -sp mouse -im none
# python es_prediction/DNABERT_finetune.py -kmer 6 -KO mouse_SI_TET23_KO -ws 512 -bs 32 -m BERT -sp mouse_si -im none



KOs = ['TET', 'DNMT3', 'QKO', 'PKO', 'mouse_3aKO', 'mouse_3bKO', 'mouse_SI_TET23_KO']
for ko in KOs:
    print('python ./reviewer_feedback/train_test_min_distance.py -KO ' + ko)


for species in spe_Ko_dic.keys():
    KOs = spe_Ko_dic[species]
    for KO in KOs:
        print('python /home/ssere004/fCpG_prediction/fcpg_prediction/reviewer_feedback/DNABERT_finetune_epoch_loss.py -kmer 6 -KO {} -ws 512 -bs 32 -m BERT -sp {} -im none'.format(KO, species))


