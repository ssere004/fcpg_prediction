from preprocess import BERT_preprocess as bertprep
from torch.utils.data import Dataset
import pandas as pd
import configs
import preprocess.data_reader as dr
import preprocess.preprocess as preprocess
import torch
import numpy as np

class DNADataset(Dataset):
    def __init__(self, data, labels, tokenizer, max_length, kmer):
        self.data = data
        self.labels = labels
        self.tokenizer = tokenizer
        self.max_length = max_length
        self.include_meth = 'meth' in data.columns
        self.kmer = kmer
    def __len__(self):
        return len(self.data)
    def __getitem__(self, idx):
        sequence = self.data.iloc[idx]['seq']
        methylations = self.data.iloc[idx]['meth'] if self.include_meth else torch.Tensor([])
        label = self.labels[idx]
        # Tokenize sequence
        encoded_sequence = self.tokenizer(
            bertprep.seq2kmer(sequence, int(self.kmer)),
            max_length=self.max_length,
            padding='max_length',
            truncation=True,
            return_tensors='pt'
        )
        input_ids = encoded_sequence["input_ids"].squeeze()
        attention_mask = encoded_sequence["attention_mask"].squeeze()
        return {
            "input_ids": input_ids,
            "attention_mask": attention_mask,
            "label": label,
            "methylations": methylations,
            "sequence": sequence
        }

def make_meth_profile(row, meth_seq_lst, seq_ws, mc_num):
    half_seq_ws = int(seq_ws//2) * 4
    half_mc_num = int(mc_num//2)
    res = np.asarray([])
    for meth_seq in meth_seq_lst:
        m_seq = meth_seq[row['chr'].lower()][int(row['position']) - half_seq_ws: int(row['position'])][:, 0]
        ds = np.concatenate((np.full(max(0, half_mc_num - len(non_zero_elements := m_seq[m_seq != 0])), -1 + configs.unmethylated_c_id), non_zero_elements))[-half_mc_num:] - configs.unmethylated_c_id #In m_seq the methylation levels were added with a tiny number to distinguish with non C nucleotides
        m_seq = meth_seq[row['chr'].lower()][int(row['position']) + 1: int(row['position']) + half_seq_ws][:, 0]
        us = np.concatenate((non_zero_elements := m_seq[m_seq != 0], np.full(max(0, half_mc_num - len(non_zero_elements)), -1 + configs.unmethylated_c_id)))[:half_mc_num] - configs.unmethylated_c_id
        res = np.concatenate((res, ds))
        res = np.concatenate((res, us))
    return res

def make_meth_profile_window_base(row, meth_seq_lst, window_size):
    res = np.asarray([])
    for meth_seq in meth_seq_lst:
        m_seq = meth_seq[row['chr'].lower()][int(row['position']) - int(window_size//2): int(row['position']) + int(window_size//2)][:, 0]
        m_seq[int(window_size//2)] = 0
        m_seq[m_seq == 0] = -1
        m_seq = m_seq - configs.unmethylated_c_id
        res = np.concatenate((res, m_seq))
    return res

def make_input_dataset(assembly, coordinate_df, label, meth_seq_lst=None, window_size=128, mc_num=20):
    res_df = pd.DataFrame()
    res_df['seq'] = coordinate_df.apply(lambda row: str(assembly[row['chr'].lower()][int(row['position']) - int(window_size//2): int(row['position']) + int(window_size//2)]), axis=1)
    #if meth_seq_lst != None: res_df['meth'] = coordinate_df.apply(lambda row: make_meth_profile_window_base(row, meth_seq_lst, window_size, mc_num), axis=1)
    if meth_seq_lst != None: res_df['meth'] = coordinate_df.apply(lambda row: make_meth_profile_window_base(row, meth_seq_lst, window_size), axis=1)
    res_df['label'] = label
    res_df['chr'] = coordinate_df['chr']
    res_df['position'] = coordinate_df['position']
    return res_df

def load_df(KO):
    if KO == 'TET' or KO == 'DNMT3' or KO == 'QKO' or KO == 'PKO':
        if KO == 'TET':
            KO_dmr = configs.TKO_DMRs_address
            KO_notdmr = configs.TKO_notDMRs_address
        elif KO == 'DNMT3':
            KO_dmr = configs.DKO_DMRs_address
            KO_notdmr = configs.DKO_notDMRs_address
        elif KO == 'QKO':
            KO_dmr = configs.QKO_DMRs_address
            KO_notdmr = configs.QKO_notDMRs_address
        elif KO == 'PKO':
            KO_dmr = configs.PKO_DMRs_address
            KO_notdmr = configs.PKO_notDMRs_address
        dmrs = pd.read_csv(KO_dmr, header=None, sep='\t', names=['chr', 'position', 'meth1', 'coverage1', 'meth2', 'coverage2', 'p_value'])
        not_dmrs = pd.read_csv(KO_notdmr, header=None, sep='\t', names=['chr', 'position', 'meth1', 'coverage1', 'meth2', 'coverage2'])
    elif 'mouse' in KO:
        adds = {'mouse_3aKO': (configs.mouse_3aKO_DMRs_address, configs.mouse_3aKO_notDMRs_address),
                'mouse_3bKO': (configs.mouse_3bKO_DMRs_address, configs.mouse_3bKO_notDMRs_address),
                'mouse_SI_TET23_KO': (configs.mouse_SI_TET23_DMRs_address, configs.mouse_SI_TET23_notDMRs_address)}
        dmrs = pd.read_csv(adds[KO][0], header=None, sep='\t', names=['chr', 'position', 'meth1', 'coverage1', 'meth2', 'coverage2', 'p_value'])
        not_dmrs = pd.read_csv(adds[KO][1], header=None, sep='\t', names=['chr', 'position', 'meth1', 'coverage1', 'meth2', 'coverage2'])
    elif KO == 'fCpG':
        fcpgs = dr.read_fcpgs(configs.fCpGs_address1)
        epic2 = dr.read_epic2_conf(configs.epic_file)
        dmrs = preprocess.make_fcpg_loc_df(fcpgs, epic2)
        not_dmrs = epic2[~epic2['Name'].isin(fcpgs['ID'])][['Name', 'chr', 'position']].sample(n=len(dmrs))
    return dmrs, not_dmrs



# class DNADataset(Dataset):
#     def __init__(self, sequences, labels, tokenizer, max_length, kmer):
#         self.sequences = sequences
#         self.labels = labels
#         self.tokenizer = tokenizer
#         self.max_length = max_length
#         self.kmer = kmer
#     def __len__(self):
#         return len(self.sequences)
#     def __getitem__(self, idx):
#         sequence = self.sequences[idx]
#         label = self.labels[idx]
#         # Tokenize sequence
#         input_ids = self.tokenizer(sequence)["input_ids"]
#         attention_mask = None
#         return {
#             "input_ids": input_ids,
#             "attention_mask": attention_mask,
#             "label": label,
#             "sequence": sequence
#         }

def make_train_included_dic(assembly, dataset, window_size):
    hws = int(window_size/2)
    res_dic = {chr: np.zeros(len(assembly[chr])) for chr in assembly.keys()}
    for index, row in dataset.iterrows():
        res_dic[row['chr'].lower()][row['position']-hws: row['position']+hws] = 1
    return res_dic

def make_test_coordinate_df(train_included_dic, coordinate_df):
    coordinate_df['is_included_in_train'] = coordinate_df.apply(lambda row: train_included_dic[row['chr'].lower()][row['position']], axis=1)
    coordinate_df = coordinate_df[coordinate_df['is_included_in_train'] != 1]
    return coordinate_df

def prepare_y(labels):
    Y = np.asarray(pd.cut(labels, bins=2, labels=[0, 1], right=False))
    b = np.zeros((Y.size, Y.max()+1))
    b[np.arange(Y.size), Y] = 1
    return b

